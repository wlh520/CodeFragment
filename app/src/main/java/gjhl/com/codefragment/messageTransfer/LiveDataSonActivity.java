package gjhl.com.codefragment.messageTransfer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class LiveDataSonActivity extends AppCompatActivity {
    private static final String TAG = "LiveDataSonActivity";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, LiveDataSonActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_data_son_activity);
    }

    public void cSend(View view) {
        Log.i(TAG, "cSend: ");
        LiveDataBus.get().getChannel("key_test").setValue("aaaaa");
    }
}
