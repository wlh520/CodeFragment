package gjhl.com.codefragment.messageTransfer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import gjhl.com.codefragment.R;

// https://www.jianshu.com/p/db99b36798cb
public class LiveDataActivity extends AppCompatActivity {
    private static final String TAG = "LiveDataActivity";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, LiveDataActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_data_activity);

        LiveDataBus.get()
                .getChannel("key_test", String.class)
                .observe(this, new Observer<String>() {
                    @Override
                    public void onChanged(String s) {
                        Log.i(TAG, "onChanged: " + s);
                    }
                });
    }

    public void cSend(View view) {
        Log.i(TAG, "cSend: ");
        LiveDataBus.get().getChannel("key_test").setValue("aaaaa");
    }

    public void cSon(View view) {
        LiveDataSonActivity.start(this);
    }
}
