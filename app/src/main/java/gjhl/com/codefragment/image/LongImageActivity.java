package gjhl.com.codefragment.image;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

import gjhl.com.codefragment.R;

// 长图
// https://www.jb51.net/article/126939.htm
// 调用 javascript 方法
public class LongImageActivity extends AppCompatActivity {
    private WebView webView;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, LongImageActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.long_image_activity);
        loadLongImage();
    }

    private void loadLongImage() {
        webView= (WebView) findViewById(R.id.vWebView);
        WebSettings webSettings=webView.getSettings();
        //允许webview对文件的操作
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        //用于js调用Android
        webSettings.setJavaScriptEnabled(true);
        //设置编码方式
        webSettings.setDefaultTextEncodingName("utf-8");
        webView.setWebChromeClient(new chromClient());
        //访问Android assets文件夹内的
        String url="file:///android_asset/test.html";
        //访问网页Html
//    String url="http://192.168.1.121:8080/jsandroid/index.html"；
        runWebView(url);
    }

    private class chromClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if(newProgress==100){
                //页面加载完成执行的操作
                String path= "file://"+ Environment.getExternalStorageDirectory()+ File.separator+"123.jpg";
                String action="javascript:aa('"+path+"')";
                runWebView(action);
            }
            super.onProgressChanged(view, newProgress);
        }
    }
    private void runWebView(final String url){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl(url);
            }
        });
    }
}


