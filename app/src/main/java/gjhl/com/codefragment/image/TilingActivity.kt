package gjhl.com.codefragment.image

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import gjhl.com.codefragment.R

// 平铺
class TilingActivity : AppCompatActivity() {
    companion object {
        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, TilingActivity::class.java)
            activity.startActivity(intent)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tiling)
    }
}