package gjhl.com.codefragment.image;

import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.File;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.recyclerView.nestedScroll.RVHeadFragment;

public class LongImageFragment extends Fragment {
    public static LongImageFragment instance() {
        return new LongImageFragment();
    }

    private WebView webView;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.long_image_activity, container, false);
        loadLongImage();
        return view;
    }

    private void loadLongImage() {
        webView= (WebView) view.findViewById(R.id.vWebView);
        WebSettings webSettings=webView.getSettings();
        //允许webview对文件的操作
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        //用于js调用Android
        webSettings.setJavaScriptEnabled(true);
        //设置编码方式
        webSettings.setDefaultTextEncodingName("utf-8");
        webView.setWebChromeClient(new LongImageFragment.chromClient());
        //访问Android assets文件夹内的
        String url="file:///android_asset/test.html";
        //访问网页Html
//    String url="http://192.168.1.121:8080/jsandroid/index.html"；
        runWebView(url);
    }

    private class chromClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if(newProgress==100){
                //页面加载完成执行的操作
                String path= "file://"+ Environment.getExternalStorageDirectory()+ File.separator+"123.jpg";
                String action="javascript:aa('"+path+"')";
                runWebView(action);
            }
            super.onProgressChanged(view, newProgress);
        }
    }
    private void runWebView(final String url){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl(url);
            }
        });
    }
}
