package gjhl.com.codefragment.image;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import gjhl.com.codefragment.R;

import static java.security.AccessController.getContext;

public class LongImage2Activity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, LongImage2Activity.class);
        activity.startActivity(intent);
    }
    private ImageView iv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.long_image2_activity);
        iv = findViewById(R.id.iv);

        Glide.with(this).asBitmap().override(1080, 38979).load(R.drawable.long_img)
                .into(iv);
    }
}
