package gjhl.com.codefragment.image;

import android.app.Activity;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import gjhl.com.codefragment.R;

public class ImageActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ImageActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        // http://szw.w121.guoji.biz/Uploads/Picture/2019-04-27/5cc3f270349c9.jpg
        Glide.with(this)
                .load("http://szw.w121.guoji.biz/Uploads/Picture/2019-04-27/5cc3f270349c9.jpg")
                .into((ImageView) findViewById(R.id.ivMaxHeight));
        Glide.with(this)
                .load("http://szw.w121.guoji.biz/Uploads/Picture/2019-05-23/5ce5f3ac12f82.jpg")
                .into((ImageView) findViewById(R.id.ivMaxHeight2));
        button();
    }

    private void button() {
        findViewById(R.id.bTiling).setOnClickListener(v -> {
            TilingActivity.Companion.start(this);
        });

        findViewById(R.id.bLong).setOnClickListener(v -> {
            LongImageActivity.start(this);
        });
    }

    public void cLongImage2Activity(View view) {
        LongImage2Activity.start(this);
    }
}
