package gjhl.com.codefragment.activityPassValue;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import gjhl.com.codefragment.R;

public class SonActivity extends AppCompatActivity {
    public static void start(Activity activity, int value) {
        // 1
        Intent intent = new Intent(activity, SonActivity.class);
        intent.putExtra("value", value);
        activity.startActivityForResult(intent, 1);
        // fragment 中用这个
//        fragment.startActivityForResult(new Intent(activity, VerifiedLawyerActivity.class), 1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_son);

        int value = getIntent().getIntExtra("value", 0);
        findViewById(R.id.button).setOnClickListener(v -> {
            cFinishAndReturn("sss");
        });
    }

    // 2
    private void cFinishAndReturn(String data) {
        Intent intent = new Intent();
        intent.putExtra("data_return", data);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        cFinishAndReturn("onBackPressed");
    }
}
