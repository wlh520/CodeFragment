package gjhl.com.codefragment.activityPassValue;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import gjhl.com.codefragment.R;

public class FatherActivity extends AppCompatActivity {
    private static final String TAG = "FatherActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, FatherActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_father);
        findViewById(R.id.button3).setOnClickListener(v -> SonActivity.start(this, 10));
    }

    // registerForActivityResult
    // 3
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
            switch (requestCode) {
                case 1:
                    String returnData = data.getStringExtra("data_return");
                    Log.i(TAG, "onActivityResult: " + returnData);
                    break;
            }
    }
}
