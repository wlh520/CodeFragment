package gjhl.com.codefragment.returnW

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import gjhl.com.codefragment.R
import gjhl.com.codefragment.thread.ThreadActivity

// while true return 直接结束了 while
class ReturnActivity : AppCompatActivity() {
    private val TAG = "ReturnActivity"
    companion object {
        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, ReturnActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.return_activity)
        MyThread().start()
    }

    class MyThread : Thread() {
        override fun run() {
            try {
                while (true) {
                    sleep(1000)
                    r()
                    Log.i("ReturnActivity", "run: ")
                    return // 这个会结束 while true
                }
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }

        fun r() {
            Log.i("ReturnActivity", "run: r()")
            return // 这个不影响 while true
        }
    }
}