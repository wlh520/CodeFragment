package gjhl.com.codefragment.adBroadcast.localFile

import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import gjhl.com.codefragment.R
import gjhl.com.codefragment.databinding.EmptyActivityBinding
import java.io.File

class LocalFileActivity : AppCompatActivity() {
    companion object {
        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, LocalFileActivity::class.java)
            activity.startActivity(intent)
        }
    }

    val binding : EmptyActivityBinding by lazy { DataBindingUtil.setContentView(this, R.layout.empty_activity) as EmptyActivityBinding }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.activity = this

        getFile()
    }

    private fun getFile() {
        val files = FileUtils.getFiles(Environment.getExternalStorageDirectory().toString() + "/测试")
        val map4File = files[0]
        val suffix = FileUtils.getSuffix(map4File)

        Log.i("TAGW", "getFile: ${map4File.absolutePath}")
        Log.i("TAGW", "getFile: ${suffix}")
    }
}