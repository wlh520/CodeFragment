package gjhl.com.codefragment.adBroadcast

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import gjhl.com.codefragment.R
import gjhl.com.codefragment.adBroadcast.localFile.LocalFileActivity
import gjhl.com.codefragment.databinding.AdBroadcastActivityBinding

class AdBroadcastActivity : AppCompatActivity() {
    companion object {
        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, AdBroadcastActivity::class.java)
            activity.startActivity(intent)
        }
    }

    val binding: AdBroadcastActivityBinding by lazy {
        DataBindingUtil.setContentView(this,
                R.layout.ad_broadcast_activity) as AdBroadcastActivityBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.click = this
        secondHide()
    }

    // 隐藏导航栏和状态栏
    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        when (ev.action) {
            MotionEvent.ACTION_DOWN -> {                //有按下动作时取消定时
                Log.i("TAGW", "dispatchTouchEvent: ACTION_DOWN ${ev.y}")
            }
            MotionEvent.ACTION_UP -> {
                Log.i("TAGW", "dispatchTouchEvent: ACTION_UP ${ev.y}")
                secondHide()
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    fun secondHide() {
        val flags = window.decorView.systemUiVisibility
        window.decorView.systemUiVisibility = (flags or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)
    }

  /*  int flags = getWindow().getDecorView().getSystemUiVisibility();
    getWindow().getDecorView().setSystemUiVisibility(flags|View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    |View.SYSTEM_UI_FLAG_FULLSCREEN|View.SYSTEM_UI_FLAG_IMMERSIVE);*/
/*    protected void secondHide() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }*/

    fun cLocalFile() {
        LocalFileActivity.start(this)
    }

}