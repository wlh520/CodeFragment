package gjhl.com.codefragment.adBroadcast.localFile

import java.io.File

object FileUtils {
    /**
     * 获取目录下所有文件
     *
     * @param realPath
     * @param files
     * @returnF
     */
    fun getFiles(realPath: String): List<File> {
        val files = ArrayList<File>()
        val realFile = File(realPath)
        if (realFile.isDirectory) {
            val subFiles = realFile.listFiles()
            for (file in subFiles) {
                if (!file.isDirectory) {
                    files.add(file)
                }
            }
        }
        return files
    }

    /**
     * 获取文件名 不带后缀
     *
     * @param file
     */
    fun getFileName(file: File): String{
        val splits = file.name.split(".") // java split("\\.")
        return splits[0]
    }


    /**
     * 获取文件后缀名 不含点
     *
     * @param file
     */
    fun getSuffix(file: File): String{
        val splits = file.name.split(".")
        return splits[splits.size-1]
    }

    /**
     * 获取文件名加后缀名
     *
     * @param file
     */
    fun getSuffix2(file: File): String{
        val splits = file.name.split("/")
        return splits[splits.size-1]
    }
}