package gjhl.com.codefragment.permission.notification

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import gjhl.com.codefragment.R
import gjhl.com.codefragment.databinding.NotificationActivityBinding
import gjhl.com.codefragment.permission.PermissionUtil
import java.lang.reflect.Method


class NotificationActivity : AppCompatActivity() {
    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, NotificationActivity::class.java)
            activity.startActivity(intent)
        }
    }

    val binding: NotificationActivityBinding by lazy { DataBindingUtil.setContentView(this, R.layout.notification_activity) as NotificationActivityBinding }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.click = this

   /*     val notificationManager = NotificationManagerCompat.from(this);
        val isOpend = notificationManager.areNotificationsEnabled()
        if (!isOpend) {
            val intent = Intent()
//            intent.action = Settings.ACTION_APP_NOTIFICATION_SETTINGS
            intent.action = Settings.ACTION_APP_NOTIFICATION_SETTINGS
            //8.0及以后版本使用这两个extra.  >=API 26
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
            intent.putExtra(Settings.EXTRA_CHANNEL_ID, applicationInfo.uid)
            //5.0-7.1 使用这两个extra.  <= API 25, >=API 21
            intent.putExtra("app_package", packageName)
            intent.putExtra("app_uid", applicationInfo.uid)
            startActivity(intent)
        }*/

        if (!commonROMPermissionCheck(this)) {
            requestAlertWindowPermission()
        }
    }

    //判断权限
    private fun commonROMPermissionCheck(context: Context): Boolean {
        var result = true
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                val clazz: Class<*> = Settings::class.java
                val canDrawOverlays: Method = clazz.getDeclaredMethod("canDrawOverlays", Context::class.java)
                result = canDrawOverlays.invoke(null, context) as Boolean
            } catch (e: Exception) {

            }
        }
        return result
    }

    //申请权限
    private fun requestAlertWindowPermission() {
        val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
        intent.data = Uri.parse("package:$packageName")
        startActivityForResult(intent, 1)
    }
}