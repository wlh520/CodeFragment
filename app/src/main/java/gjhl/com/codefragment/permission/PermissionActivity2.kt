package gjhl.com.codefragment.permission

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

// 权限回调 initAct()
open class PermissionActivity2 : AppCompatActivity() {
    /**
     * 需要进行检测的权限数组,不能多写，多写 onResume 会被调用两次
     * 调用两次: https://www.jianshu.com/p/5764909b9af9
     */
    private var needPermissions = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    private val PERMISSON_REQUESTCODE = 0

    override fun onStart() {
        //不加这个clearFlags好像就没效果
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        )
        //设置导航栏透明(就是虚拟键那一栏)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        //设置状态栏(或者叫通知栏)透明
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        window.setStatusBarColor(Color.TRANSPARENT)
        super.onStart()

        supportActionBar?.hide()
        checkPermissions(needPermissions)
        secondHide()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSON_REQUESTCODE) {
            initAct()
        }
    }

    /**
     *
     * @param permissions
     * @since 2.5.0
     */
    private fun checkPermissions(permissions: Array<String>) {
        val needRequestPermissonList = findDeniedPermissions(permissions)
        if (needRequestPermissonList.size > 0) {
            ActivityCompat.requestPermissions(
                this,
                needRequestPermissonList.toTypedArray(),
                PERMISSON_REQUESTCODE
            )
        } else initAct()
    }

    /**
     * 获取权限集中需要申请权限的列表
     *
     * @param permissions
     * @return
     * @since 2.5.0
     */
    private fun findDeniedPermissions(permissions: Array<String>): List<String> {
        val needRequestPermissionList: MutableList<String> = ArrayList()
        for (perm in permissions) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    perm
                ) !== PackageManager.PERMISSION_GRANTED
                || ActivityCompat.shouldShowRequestPermissionRationale(
                    this, perm
                )
            ) {
                needRequestPermissionList.add(perm)
            }
        }
        return needRequestPermissionList
    }

    fun secondHide() {
        val flags = window.decorView.systemUiVisibility
        window.decorView.systemUiVisibility = (flags or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)
        Handler().postDelayed({ secondHide() }, 500)
    }

    open fun initAct(){}
}