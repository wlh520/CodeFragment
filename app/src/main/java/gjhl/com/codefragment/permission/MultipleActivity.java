package gjhl.com.codefragment.permission;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import gjhl.com.codefragment.R;

public class MultipleActivity extends CheckPermissionsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void havePermission() {
        setContentView(R.layout.activity_multiple);
    }
}
