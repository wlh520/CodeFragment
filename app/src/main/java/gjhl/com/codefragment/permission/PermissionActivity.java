package gjhl.com.codefragment.permission;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivityPermissionBinding;
import gjhl.com.codefragment.permission.notification.NotificationActivity;

public class PermissionActivity extends AppCompatActivity {
    private static final String TAG = "PermissionActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, PermissionActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityPermissionBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_permission);
        binding.setClick(this);
        findViewById(R.id.multiple).setOnClickListener(v -> startActivity(new Intent(PermissionActivity.this, MultipleActivity.class)));

        bMike();
    }

    public void cCall() {
        if (PermissionUtil.INSTANCE.call(this))
            call();
    }

    public void cNotification() {
        NotificationActivity.Companion.start(this);
    }

    private void bMike() {
        findViewById(R.id.bMike).setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(PermissionActivity.this, Manifest.permission.RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PermissionActivity.this,
                        new String[]{Manifest.permission.RECORD_AUDIO}, 2);
            } else {
                Log.i(TAG, "bMike: ");
            }
            ;
        });
    }

    private void call() {
        try {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:10086"));
            startActivity(intent);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionUtil.CALL:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call();
                } else {
                    Toast.makeText(this, "You denied the permission", Toast.LENGTH_SHORT).show();
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "bMike: ");
                } else {
                    Toast.makeText(this, "You denied the permission", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
        }
    }
}
