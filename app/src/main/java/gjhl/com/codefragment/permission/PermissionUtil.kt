package gjhl.com.codefragment.permission

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.util.ArrayList

object PermissionUtil {
    const val CALL = 100
    const val notificationHover = 105

    fun call(activity: Activity): Boolean {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CALL_PHONE), CALL)
            return false
        } else return true
    }

    fun notificationHover(activity: Activity): Boolean {
        return checkPermissions(activity, notificationHover, Manifest.permission.SYSTEM_ALERT_WINDOW)
    }

    // true 有权限
    private fun checkPermissions(activity: Activity, requestCode: Int, vararg permissions: String) : Boolean{
        val needRequestPermissonList: List<String> = findDeniedPermissions(activity, permissions.toList())
        if (needRequestPermissonList.isNotEmpty()) {
            ActivityCompat.requestPermissions(activity,
                    needRequestPermissonList.toTypedArray(),
                    requestCode)
            return false
        } else return true
    }

    private fun findDeniedPermissions(activity: Activity, permissions: List<String>): List<String> {
        val needRequestPermissionList: MutableList<String> = ArrayList()
        for (perm in permissions) {
            if (ContextCompat.checkSelfPermission(activity,
                            perm) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                            activity, perm)) {
                needRequestPermissionList.add(perm)
            }
        }
        return needRequestPermissionList
    }
}