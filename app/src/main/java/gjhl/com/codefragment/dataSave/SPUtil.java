package gjhl.com.codefragment.dataSave;

import android.content.Context;
import android.content.SharedPreferences;

import gjhl.com.codefragment.MyApplication;

public class SPUtil {

    public static void save(String key, String value) {
        //步骤1：创建一个SharedPreferences对象
        SharedPreferences sharedPreferences = getSharedPreferences();
        //步骤2： 实例化SharedPreferences.Editor对象
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //步骤3：将获取过来的值放入文件
        editor.putString(key, value);
        //步骤4：提交
        editor.apply();
    }

    public static String get(String key) {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.getString(key, "");
    }

    private static SharedPreferences getSharedPreferences() {
        return MyApplication.getContext().getSharedPreferences("data", Context.MODE_PRIVATE);
    }
}
