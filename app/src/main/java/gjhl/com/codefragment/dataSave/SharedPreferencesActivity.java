package gjhl.com.codefragment.dataSave;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class SharedPreferencesActivity extends AppCompatActivity {
    private static final String TAG = "SharedPreferencesActivi";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SharedPreferencesActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shared_preferences_activity);
        findViewById(R.id.bSave).setOnClickListener(v -> {
            SPUtil.save("111", "222");
        });

        findViewById(R.id.bGet).setOnClickListener(v -> {
            String value = SPUtil.get("111");
            Log.i(TAG, "onCreate: " + value);
        });
    }


}
