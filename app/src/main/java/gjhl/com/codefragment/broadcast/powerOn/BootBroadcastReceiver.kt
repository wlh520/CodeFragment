package gjhl.com.codefragment.broadcast.powerOn

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import gjhl.com.codefragment.MainActivity

// 1.    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
/*
 2.
 <receiver
            android:name=".adBroadcast.powerOn.BootBroadcastReceiver"
            android:enabled="true"
            android:exported="true">
            <intent-filter>
                <action android:name="android.intent.action.BOOT_COMPLETED" />
            </intent-filter>
        </receiver>
*/
// 3. BootBroadcastReceiver
// 开机启动
class BootBroadcastReceiver : BroadcastReceiver() {
    val action = "android.intent.action.BOOT_COMPLETED"

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action.equals(action)) {
            val mainActivityIntent = Intent(context, MainActivity::class.java)
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context!!.startActivity(mainActivityIntent)
        }
    }

}