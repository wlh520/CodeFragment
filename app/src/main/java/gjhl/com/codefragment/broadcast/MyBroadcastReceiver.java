package gjhl.com.codefragment.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * 发送广播
 Intent intent = new Intent();
 intent.putExtra("key", "value" + i++);
 intent.setAction("test");
 sendBroadcast(intent);
 */


/*
内部广播

private BroadcastReceiver receiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive: ");
    }
};

IntentFilter filter = new IntentFilter(SEND_IMAGE_CAPTURE_SUCCESS);
registerReceiver(receiver, filter);
 */
/**
 外部广播 接收
 <receiver
  android:name=".broadcast.MyBroadcastReceiver"
  android:enabled="true"
  android:exported="true">
 <p>
 <intent-filter>
 <action android:name="test"/>
 </intent-filter>
 </receiver>
 */
// App 间通过广播传递消息
public class MyBroadcastReceiver extends BroadcastReceiver {
    private String action = "test";
    private static final String TAG = "MyBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(action)) {
            String value = intent.getStringExtra("key");
            Log.i(TAG, "onReceive: " + value);
        }
    }
}
