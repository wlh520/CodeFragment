package gjhl.com.codefragment;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ListenSoftKeyboardActivity extends AppCompatActivity {
    private static final String TAG = "ListenSoftKeyboardAct";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ListenSoftKeyboardActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listen_soft_keyboard);

        SoftKeyBoardListener.setListener(this, new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
            @Override
            public void keyBoardShow(int height) {
                findViewById(R.id.tv).setVisibility(View.INVISIBLE);
//                Toast.makeText(ListenSoftKeyboardActivity.this, "键盘显示 高度" + height, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void keyBoardHide(int height) {
                findViewById(R.id.tv).setVisibility(View.VISIBLE);
//                Toast.makeText(ListenSoftKeyboardActivity.this, "键盘隐藏 高度" + height, Toast.LENGTH_SHORT).show();
            }
        });


    }
}
