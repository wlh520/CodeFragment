package gjhl.com.codefragment.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gjhl.com.codefragment.R;

public class SonFragment extends Fragment {
    public static SonFragment newInstance() {
        return new SonFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_son, container, false);
    }

}
