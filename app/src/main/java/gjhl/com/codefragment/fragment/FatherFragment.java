package gjhl.com.codefragment.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gjhl.com.codefragment.R;

public class FatherFragment extends Fragment {
    private View rootView;

    public static FatherFragment newInstance() {
        return new FatherFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_father, container, false);
        flFather();
        return rootView;
    }

    private void flFather() {
        getChildFragmentManager().beginTransaction().replace(R.id.flFather, SonFragment.newInstance()).commit(); // fragment添加子fragment
    }

}
