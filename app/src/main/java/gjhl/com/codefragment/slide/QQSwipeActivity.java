package gjhl.com.codefragment.slide;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import gjhl.com.codefragment.R;

public class QQSwipeActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, QQSwipeActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qqswipe);
        findViewById(R.id.tvDelete).setOnClickListener(v -> Toast.makeText(this, "delete", Toast.LENGTH_SHORT).show());
    }
}
