package gjhl.com.codefragment.text.horizontalScroll;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class MarqueeActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, MarqueeActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.marquee_activity);

    }
}
