package gjhl.com.codefragment.text.big;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

// 尽可能大
public class BigActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, BigActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.big_activity);
        TextView tv1 = findViewById(R.id.tv1);
        TextView tv2 = findViewById(R.id.tv2);
        autoMatchFont(tv1);
        autoMatchFont(tv2);
    }

    public static void autoMatchFont(final TextView view){
        view.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            float vWidth = v.getWidth();
            TextPaint paint = view.getPaint();
            String text = view.getText().toString();
            float textLen = paint.measureText(text);
            float oldSize = view.getTextSize();

            if (textLen != vWidth){
                float size = vWidth * oldSize / textLen;
                view.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
            }
        });
    }
}
