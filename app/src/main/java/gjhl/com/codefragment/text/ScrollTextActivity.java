package gjhl.com.codefragment.text;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewFlipper;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.text.big.BigActivity;
import gjhl.com.codefragment.text.horizontalScroll.MarqueeActivity;

// 文字滚动
public class ScrollTextActivity extends AppCompatActivity {
    private static final String TAG = "ScrollTextActivity";
    private String isNull;
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ScrollTextActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_text);

        ViewFlipper viewFlipper= (ViewFlipper) findViewById(R.id.flipper);
        addDataToViewFlipper(viewFlipper);

        TextView tvEmpty = findViewById(R.id.tvEmpty);
        Log.i(TAG, "onCreate: " + tvEmpty.getText() + "--" + isNull);
    }

    private void addDataToViewFlipper(ViewFlipper viewFlipper) {
        for (int i = 0; i < 5; i++) {
            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.layout_custom, null);
            viewFlipper.addView(view);
        }
    }

    public void cBigActivity(View v) {
        BigActivity.start(this);
    }

    public void cMarqueeActivity(View v) {
        MarqueeActivity.start(this);
    }
}
