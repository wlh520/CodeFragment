package gjhl.com.codefragment;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Created by Administrator on 2018/4/3.
 */

public class Home_Scan_Util {
    public static String getSignal(long times) {
        String timeTemp = String.valueOf(times);
        String appToken = "NEWS";
        String temp = timeTemp + appToken;
        char[] temps = temp.toCharArray();
        Arrays.sort(temps);
        String newTemp = String.valueOf(temps);
        return SHA1(newTemp);
    }

    /**
     * SHA1加密
     */
    public static String SHA1(String decrypt) {
        try {
            MessageDigest digest = MessageDigest
                    .getInstance("SHA-1");
            digest.update(decrypt.getBytes());
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            // 字节数组转换为 十六进制 数
            for (byte aMessageDigest : messageDigest) {
                String shaHex = Integer.toHexString(aMessageDigest & 0xFF);
                if (shaHex.length() < 2) {
                    hexString.append(0);
                }
                hexString.append(shaHex);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
