package gjhl.com.codefragment.verticalText;

/**
 * 作者：Abeing
 * <p>
 * 时间： 2019/3/18 18:35
 */
public class HtmlString {
    private String tagStart;
    private String tagEnd;
    private char content;

    public String getTagStart() {
        return tagStart;
    }

    public void setTagStart(String tagStart) {
        this.tagStart = tagStart;
    }

    public char getContent() {
        return content;
    }

    public void setContent(char content) {
        this.content = content;
    }

    public String getTagEnd() {
        return tagEnd;
    }

    public void setTagEnd(String tagEnd) {
        this.tagEnd = tagEnd;
    }
}
