package gjhl.com.codefragment.verticalText;

import android.app.Activity;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import gjhl.com.codefragment.R;

// 垂直的文字显示
public class VerticalTextActivity extends AppCompatActivity {
    private static final String TAG = "VerticalTextActivity";
    private String string = "<span style=\"color:#222222;font-family:Consolas, \" font-size:16px;background-color:#ffffff;\"=\"\">员们分析了龙湾区工贸经济的现状。 </span><div><span style=\"color:#222222;font-family:Consolas, \" font-size:16px;background-color:#ffffff;\"=\"\"><br /></span></div>";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, VerticalTextActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vertival_text);
        htmlText();
        margin();
    }

    private void htmlText() {
        StringBuilder needString = VerticalText.getVerticalHtml(3, string);

        TextView textView = findViewById(R.id.tv);
        textView.setText(Html.fromHtml(needString.toString()));
        Log.i(TAG, "onCreate: " + needString);
    }

    // 代码设置 margin
    private void margin() {
        TextView textView2 = findViewById(R.id.textView2);
        textView2.setText("fasdfldsaf");
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView2.getLayoutParams();
        params.setMargins(300, 500, 0, 0);
        textView2.setLayoutParams(params);
        textView2.requestLayout();
    }
}
