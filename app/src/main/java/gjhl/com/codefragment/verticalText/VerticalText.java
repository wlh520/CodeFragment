package gjhl.com.codefragment.verticalText;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import gjhl.com.codefragment.verticalText.HtmlString;

/**
 * 作者：Abeing
 * <p>
 * 时间： 2019/3/19 09:04
 */
public class VerticalText {
    public static StringBuilder getVerticalHtml(int columnText, String htmlStr) {
        Stack<String> stack = new Stack<>();
        List<HtmlString> htmlStrings = new ArrayList<>(200);
        StringBuilder tag = new StringBuilder();
        for (int i = 0; i < htmlStr.length() - 1; i++) {
            if (htmlStr.charAt(i) == '<' && htmlStr.charAt(i+1) == '/') {
                while (htmlStr.charAt(i) != '>') i++;
                stack.pop();
            } else if (htmlStr.charAt(i) == '<') {
                tag.append(htmlStr.charAt(i));
                while (htmlStr.charAt(i) != '>') {
                    i++;
                    tag.append(htmlStr.charAt(i));
                }
                stack.push(tag.toString());
                tag.delete(0, tag.length());
            } else {
                HtmlString htmlString = new HtmlString();
                htmlString.setTagStart(stack.peek());
                htmlString.setContent(htmlStr.charAt(i));

                int j = 1;
                StringBuilder endTag = new StringBuilder("</");
                while (htmlString.getTagStart().charAt(j) != ' ') {
                    endTag.append(htmlString.getTagStart().charAt(j));
                    j++;
                }
                endTag.append(">");
                htmlString.setTagEnd(endTag.toString());
                htmlStrings.add(htmlString);
            }
        }

        int m = htmlStrings.size();
        HtmlString[][] chars = new HtmlString[m / columnText + 1][columnText + 1];

        if (m % columnText != 0) {
            int i;
            for (i = 0; i < columnText - (m % columnText); i++) {
                HtmlString htmlString = new HtmlString();
                htmlString.setTagEnd("");
                htmlString.setTagStart("");
                htmlString.setContent(' ');
                htmlStrings.add(htmlString);
            }
            m += i;
        }

        int i = 0, j = 0;
        for (int k = 0; k <= m - 1; k++) {
            chars[i][j] = htmlStrings.get(k);
            if (j == columnText - 1) {
                j = 0;
                i++;
            } else
                j++;
        }

        int ki;
        List<HtmlString> needSb = new ArrayList<>();
        for (int kj = 0; kj < columnText; kj++) {
            for (ki = i - 1; ki >= 0; ki--) {
                needSb.add(chars[ki][kj]);
            }
            HtmlString htmlString = new HtmlString();
            htmlString.setTagEnd("<br/>");
            htmlString.setTagStart("");
            htmlString.setContent(' ');
            needSb.add(htmlString);
        }

        StringBuilder needString = new StringBuilder();
        for (HtmlString htmlString : needSb) {
            needString.append(htmlString.getTagStart());
            needString.append(htmlString.getContent());
            if (htmlString.getContent() == ' ')
                needString.append("&nbsp&nbsp&nbsp&nbsp");
            needString.append(htmlString.getTagEnd());
        }
        return needString;
    }

}
