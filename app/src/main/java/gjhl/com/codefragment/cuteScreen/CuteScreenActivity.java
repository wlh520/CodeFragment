package gjhl.com.codefragment.cuteScreen;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.hardware.display.DisplayManager;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.nio.ByteBuffer;

import gjhl.com.codefragment.R;

// 截屏保存到相册 获取uri 上传到网络
public class CuteScreenActivity extends AppCompatActivity {
    private static final String TAG = "CuteScreenActivity";
    private final int SCREEN_SHOT = 1;
    private MediaProjectionManager projectionManager;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, CuteScreenActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cute_screen);
    }

    public void cCutScreenNew(View view) {
        projectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);
        startActivityForResult(projectionManager.createScreenCaptureIntent(),
                SCREEN_SHOT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SCREEN_SHOT) {
            if (resultCode == RESULT_OK) {
                //获取 MediaProjection
                MediaProjection mediaProjection = projectionManager.getMediaProjection(requestCode, data);

                ImageReader imageReader = ImageReader.newInstance(1080, 1080,  PixelFormat.RGBA_8888, 1);
                mediaProjection.createVirtualDisplay("ScreenShout",
                        1080,1080,1,
                        DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                        imageReader.getSurface(),null,null);

                SystemClock.sleep(1000);
                Image image = imageReader.acquireNextImage();

                int width = image.getWidth();
                int height = image.getHeight();
                final Image.Plane[] planes = image.getPlanes();
                final ByteBuffer buffer = planes[0].getBuffer();
                int pixelStride = planes[0].getPixelStride();
                int rowStride = planes[0].getRowStride();
                int rowPadding = rowStride - pixelStride * width;
                Bitmap bitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height, Bitmap.Config.ARGB_8888);
                bitmap.copyPixelsFromBuffer(buffer);
                image.close();

                ImageView iv = findViewById(R.id.iv);
                iv.setImageBitmap(bitmap);
            }
        }
    }

    public void bCutScreen(View view) {
        Bitmap bitmap = CuteScreenUtil.screenShot(this);
        File dirFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "dj.jpg");
        try {
            if (!dirFile.exists())
                dirFile.createNewFile();

            boolean ret = CuteScreenUtil.save(bitmap, dirFile, Bitmap.CompressFormat.JPEG, true);
            if (ret) {
                Uri uriPath = getImageContentUri(getApplicationContext(), dirFile);
                Toast.makeText(getApplicationContext(), "截图已保持至 " + dirFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                Log.i(TAG, "onClick: " + dirFile.getAbsolutePath() + " uriPath:" + uriPath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Images.Media._ID }, MediaStore.Images.Media.DATA + "=? ",
                new String[] { filePath }, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }
}
