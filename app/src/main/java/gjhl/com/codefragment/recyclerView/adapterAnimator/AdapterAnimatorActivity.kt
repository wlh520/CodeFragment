package gjhl.com.codefragment.recyclerView.adapterAnimator

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import gjhl.com.codefragment.R

// Rv 上下左右移动动画
class AdapterAnimatorActivity : AppCompatActivity() {
    companion object {
        fun start(a: Activity) {
            val i = Intent(a, AdapterAnimatorActivity::class.java)
            a.startActivity(i)
        }
    }

    private val TAG = "AdapterAnimatorActivity"
    private var i = 0
    private val list = ArrayList<String>()
    private val rv by lazy { findViewById<RecyclerView>(R.id.rv) }
    private val tvNum by lazy { findViewById<TextView>(R.id.tvNum) }
    private val adapter = Main1366AdapterKt(list)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_1366_activity)

        bottomToTop()
        rv.adapter = adapter

        tvNum.setOnClickListener {
            i++
            list.add(0, "乐山大佛是否${i}")
            adapter.notifyItemInserted(0)
//            rv.smoothScrollToPosition(0)
            scroll()
        }
    }

    private fun bottomToTop() {
        // 从下往上
        rv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, true)
    }

    private fun topToBottom() {
        // 从上往下
        rv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    }

    private fun leftToRight() {
        // 从左往右
        rv.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, true)
    }

    private fun rightToLeft() {
        // 从右往左
        rv.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
    }

    private fun scroll() {
        val milliSecondsPerInch = 0.8f // 1 对应 3 秒
        val linearSmoothScroller: LinearSmoothScroller =
            object : LinearSmoothScroller(this) {
                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
                    val s = milliSecondsPerInch
                    Log.i(TAG, "calculateSpeedPerPixel: $s ${displayMetrics.densityDpi}")
                    return s
                }
            }
        linearSmoothScroller.targetPosition = 0 //the last position of the item in the list
        rv.layoutManager?.startSmoothScroll(linearSmoothScroller)
    }
}