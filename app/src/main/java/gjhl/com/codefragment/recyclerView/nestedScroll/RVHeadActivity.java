package gjhl.com.codefragment.recyclerView.nestedScroll;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.R;

// rv 添加头部
public class RVHeadActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, RVHeadActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rv_head_activity);

        ArrayList<String> strings = new ArrayList<>();
        for (int i=0; i<100; i++) {
            strings.add("" + i);
        }
        MyAdapter adapter = new MyAdapter(strings);
        RecyclerView rv = findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(adapter);

    }

    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<String> messages;

        public MyAdapter(ArrayList<String> messages) {
            this.messages = messages;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder holder = null;

            View view = null;
            view = LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.holder_my_view, parent, false);
            holder = new MyViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            myViewHolder.tv.setText(messages.get(position));
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            View itemView;
            TextView tv;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                this.itemView = itemView;
                this.tv = itemView.findViewById(R.id.tv);
            }
        }
    }

}
