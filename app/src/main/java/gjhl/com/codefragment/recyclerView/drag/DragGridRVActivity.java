package gjhl.com.codefragment.recyclerView.drag;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.recyclerView.Bean;
import gjhl.com.codefragment.recyclerView.MyAdapter;

// 上下滑动
// 拖拽
public class DragGridRVActivity extends AppCompatActivity {
    private MyDragAdapter adapter;
    private ArrayList<Bean> strings;
    private int i;

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, DragGridRVActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drag_grid_rv_activity);

        strings = new ArrayList<>();
        for (i = 0; i < 10; i++) {
            Bean bean = new Bean();
            bean.string = "" + i;
            strings.add(bean);
        }
        adapter = new MyDragAdapter(strings);
        RecyclerView rv = findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(this,2);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                int dragFlag = ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;

                return makeMovementFlags(dragFlag, 0);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

                adapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                //item移动后  也要移动数据
                Collections.swap(strings, viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                //暂不处理  这个主要是做左右拖动的回调
            }

            @Override
            public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
                // We only want the active item to change
                if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                    viewHolder.itemView.setAlpha(0.7f);
                }
            }

            @Override
            public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);
                viewHolder.itemView.setAlpha(1);
            }

            @Override
            public boolean canDropOver(RecyclerView recyclerView, RecyclerView.ViewHolder current, RecyclerView.ViewHolder target) {
                //需要手动重写的方法,返回true
                //当前item可以被拖动到目标位置后,直接落到target上,后面的item也接着落
                return true;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                //是否开启长按拖动
                //返回true  可以实现长按拖动排序和拖动动画
                return true;
            }

        };

        //创建item helper
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);

        //绑定到recyclerView上面
        itemTouchHelper.attachToRecyclerView(rv);

    }
}
