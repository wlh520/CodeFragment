package gjhl.com.codefragment.recyclerView.adapterAnimator

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import gjhl.com.codefragment.MyApplication
import gjhl.com.codefragment.R
import java.util.*

class Main1366AdapterKt(private val beans: ArrayList<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view = LayoutInflater.from(MyApplication.getContext())
            .inflate(R.layout.main_1366_adapter, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myViewHolder = holder as MyViewHolder
        myViewHolder.tv.text = beans[position]
    }

    override fun getItemCount(): Int {
        return beans.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tv: TextView = itemView.findViewById(R.id.tv)

    }
}
