package gjhl.com.codefragment.recyclerView.drag;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.recyclerView.Bean;

public class MyDragAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Bean> beans;

    public MyDragAdapter(ArrayList<Bean> beans) {
        this.beans = beans;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;

        View view = null;
        view = LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.holder_my_view, parent, false);
        holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.tv.setText(beans.get(position).string);
    }

    @Override
    public int getItemCount() {
        return beans.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public View itemView;
        TextView tv;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.tv = itemView.findViewById(R.id.tv);
        }
    }
}
