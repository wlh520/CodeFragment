package gjhl.com.codefragment.recyclerView.upLoad;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ItemDiscountBinding;

public class DemoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<String> list;
    public DemoAdapter(List<String> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discount, parent, false);
        ItemDiscountBinding binding = ItemDiscountBinding.bind(view);
        MyHolder holder = new MyHolder(view);
        holder.setBinding(binding);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ItemDiscountBinding binding = ((MyHolder) viewHolder).binding;
        String bean = list.get(position);
//        binding.setBean(bean);
        binding.tName.setText(bean);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        private ItemDiscountBinding binding;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
        }

        public ItemDiscountBinding getBinding() {
            return binding;
        }

        public void setBinding(ItemDiscountBinding binding) {
            this.binding = binding;
        }
    }
}
