package gjhl.com.codefragment.recyclerView.upLoad;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by jing on 2015/7/20.
 * current_page = 1, 分页开始
 */
public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
    public String TAG = EndlessRecyclerOnScrollListener.class.getSimpleName();

    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = true; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = 1; // The minimum amount of items to have below your current scroll position before loading more.
    int firstVisibleItem, visibleItemCount, totalItemCount,last;

    private int current_page = 1;

    private LinearLayoutManager linearLayoutManager;

    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public boolean isLoading() {
        return loading;
    }

    public int getPreviousTotal() {
        return previousTotal;
    }

    public void setPreviousTotal(int previousTotal) {
        this.previousTotal = previousTotal;
    }

    public void reload() {
        previousTotal -= 2;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);


        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = linearLayoutManager.getItemCount();
        firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();


        last = linearLayoutManager.findLastVisibleItemPosition();

        Log.i(TAG, "onScrolled: visibleItemCount=" + visibleItemCount
                + " totalItemCount=" + totalItemCount + " firstVisibleItem=" + firstVisibleItem + " last=" + last);
        if (loading) {
            if (totalItemCount > previousTotal + 1) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (dy > 0 && last >= 10) {
            onTopChange(true);
        } else if (dy < 0 && last < 10) {
            onTopChange(false);
        }
        if (!loading && last + 1 >= totalItemCount) {
            // End has been reached
            // Do something
            current_page++;
            new Handler(Looper.getMainLooper()).post(() -> onLoadMore(current_page));
            loading = true;
        }
    }

    public abstract void onLoadMore(int current_page);
    public abstract void onTopChange(boolean isShow);
}
