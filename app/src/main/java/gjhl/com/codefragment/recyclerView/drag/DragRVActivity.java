package gjhl.com.codefragment.recyclerView.drag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.recyclerView.Bean;
import gjhl.com.codefragment.recyclerView.MyAdapter;
import gjhl.com.codefragment.recyclerView.RVActivity;

// 上下滑动
// 拖拽
public class DragRVActivity extends AppCompatActivity {
    private MyAdapter adapter;
    private ArrayList<Bean> strings;
    private int i;

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, DragRVActivity.class));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drag_rv_activity);

        strings = new ArrayList<>();
        for (i=0; i<10; i++) {
            Bean bean = new Bean();
            bean.string = "" + i;
            strings.add(bean);
        }
        adapter = new MyAdapter(strings);
        RecyclerView rv = findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                //监控上下左右
//                int swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;

                return makeMovementFlags(dragFlags,0);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

                adapter.notifyItemMoved(viewHolder.getAdapterPosition(),target.getAdapterPosition());
                //item移动后  也要移动数据
                Collections.swap(strings,viewHolder.getAdapterPosition(),target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                //暂不处理  这个主要是做左右拖动的回调

            }

            @Override
            public boolean canDropOver(RecyclerView recyclerView, RecyclerView.ViewHolder current, RecyclerView.ViewHolder target) {
                //需要手动重写的方法,返回true
                //当前item可以被拖动到目标位置后,直接落到target上,后面的item也接着落
                return true;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                //是否开启长按拖动
                //返回true  可以实现长按拖动排序和拖动动画
                return true;
            }

        };

        //创建item helper
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);

        //绑定到recyclerView上面
        itemTouchHelper.attachToRecyclerView(rv);

    }
}
