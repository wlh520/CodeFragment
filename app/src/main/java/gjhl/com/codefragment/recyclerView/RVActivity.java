package gjhl.com.codefragment.recyclerView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.recyclerView.adapterAnimator.AdapterAnimatorActivity;
import gjhl.com.codefragment.recyclerView.drag.DragGridRVActivity;
import gjhl.com.codefragment.recyclerView.drag.DragRVActivity;
import gjhl.com.codefragment.recyclerView.nestedScroll.RVHeadActivity;
import gjhl.com.codefragment.recyclerView.nestedScroll.RVHeadFragmentActivity;
import gjhl.com.codefragment.recyclerView.upLoad.RecyclerUpLoadActivity;
import gjhl.com.codefragment.recyclerView.upLoad.grid.GridActivity;

// 倒序 recycler view
public class RVActivity extends AppCompatActivity {

    private MyAdapter adapter;
    private ArrayList<Bean> strings;

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, RVActivity.class));
    }

    private int i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rv);

        strings = new ArrayList<>();
        for (i=0; i<10; i++) {
            Bean bean = new Bean();
            bean.string = "" + i;
            strings.add(bean);
        }
        adapter = new MyAdapter(strings);
        RecyclerView rv = findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(adapter);

        findViewById(R.id.bItemInserted).setOnClickListener(v->{
            Bean bean = new Bean();
            bean.string = "" + ++i;
            strings.add(0, bean);
            adapter.notifyItemInserted(0);
        });
    }

    public void update(View view) {
        Bean s = strings.get(1);
        s.string = "进来撒Klan立刻就橄榄绿你快来解放拉萨啦个回笼觉发散开来了就爱上了更好啦发生了开发咖喱哈里发";
        adapter.notifyDataSetChanged();
    }

    public void cHeadRV(View view) {
        RVHeadActivity.start(this);
    }

    public void cUpLoadRV(View view) {
        RecyclerUpLoadActivity.start(this);
    }

    public void cUpLoadGridRV(View view) {
        GridActivity.start(this);
    }

    public void cHeadFragmentRV(View view) {
        RVHeadFragmentActivity.start(this);
    }

    public void cDragRV(View view) {
        DragRVActivity.start(this);
    }

    public void cDragGridRV(View view) {
        DragGridRVActivity.start(this);
    }

    public void cAdapterAnimatorRV(View view) {
        AdapterAnimatorActivity.Companion.start(this);
    }
}
