package gjhl.com.codefragment.recyclerView.upLoad.grid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.RecyclerUpLoadActivityBinding;
import gjhl.com.codefragment.recyclerView.upLoad.DemoAdapter;
import gjhl.com.codefragment.recyclerView.upLoad.EndlessRecyclerOnScrollListener;
import gjhl.com.codefragment.recyclerView.upLoad.RecyclerUpLoadActivity;

public class GridActivity extends AppCompatActivity {
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    private List<String> list;
    private boolean isLoading = false;
    private DemoAdapter adapter;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, GridActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RecyclerUpLoadActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.recycler_up_load_activity);
        GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 2);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.list.setLayoutManager(linearLayoutManager);

        data();
        adapter = new DemoAdapter(list);
        binding.list.setAdapter(adapter);

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.i(TAG, "onLoadMore: " + current_page);
                if (list != null && !isLoading) {
                    isLoading = true;
                    http(current_page);
                }
            }

            @Override
            public void onTopChange(boolean isShow) {

            }

        };
        binding.list.addOnScrollListener(endlessRecyclerOnScrollListener);
    }

    private void data() {
        list = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            list.add(i + "");
        }
    }

    private void http(int current_page) {
        int start = list.size();
        for (int i = 0; i < 10; i++) {
            list.add(i + "");
        }
        adapter.notifyItemRangeInserted(start, 10);
        isLoading = false;
    }

    //    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(String event) {
        if (list.size() <= 0) {
            if (endlessRecyclerOnScrollListener != null) {
                endlessRecyclerOnScrollListener.setLoading(true);
                endlessRecyclerOnScrollListener.setPreviousTotal(0);
            }
        }
    }
}
