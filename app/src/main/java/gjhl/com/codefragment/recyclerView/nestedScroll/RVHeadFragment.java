package gjhl.com.codefragment.recyclerView.nestedScroll;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.R;

public class RVHeadFragment extends Fragment {
    public static RVHeadFragment instance() {
        return new RVHeadFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.rv_head_fragment, container, false);

        ArrayList<String> strings = new ArrayList<>();
        for (int i=0; i<100; i++) {
            strings.add("" + i);
        }
        MyAdapter adapter = new MyAdapter(strings);
        RecyclerView rv = rootView.findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(adapter);
        return rootView;
    }

    public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<String> messages;

        public MyAdapter(ArrayList<String> messages) {
            this.messages = messages;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder holder = null;

            View view = null;
            view = LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.holder_my_view, parent, false);
            holder = new MyAdapter.MyViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            RVHeadActivity.MyAdapter.MyViewHolder myViewHolder = (RVHeadActivity.MyAdapter.MyViewHolder) holder;
            myViewHolder.tv.setText(messages.get(position));
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            View itemView;
            TextView tv;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                this.itemView = itemView;
                this.tv = itemView.findViewById(R.id.tv);
            }
        }
    }
}
