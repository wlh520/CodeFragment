package gjhl.com.codefragment.recyclerView.nestedScroll;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class RVHeadFragmentActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, RVHeadFragmentActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rv_head_fragment_activity);
        getSupportFragmentManager().beginTransaction().replace(R.id.fl, RVHeadFragment.instance()).commitAllowingStateLoss();
    }
}
