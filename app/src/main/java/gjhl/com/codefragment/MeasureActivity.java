package gjhl.com.codefragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

public class MeasureActivity extends AppCompatActivity {
    private static final String TAG = "MeasureActivity";

    public static void start(Activity a) {
        Intent intent = new Intent(a, MeasureActivity.class);
        a.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measure);
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;         // 屏幕宽度（像素）
        int height = dm.heightPixels;       // 屏幕高度（像素） 包含了状态栏
        float density = dm.density;         // 屏幕密度（0.75 / 1.0 / 1.5）
        int densityDpi = dm.densityDpi;     // 屏幕密度dpi（120 / 160 / 240）
        // 屏幕宽度算法:屏幕宽度（像素）/屏幕密度
        int screenWidth = (int) (width / density);  // 屏幕宽度(dp)
        int screenHeight = (int) (height / density);// 屏幕高度(dp)

        Log.i(TAG, "onCreate: width/" + width + " height/" + height + " density/" + density + " densityDpi/" + densityDpi);
        TextView textView3 = findViewById(R.id.textView3);
        textView3.setText("width/" + width + " height/" + height + " density/" + density + " densityDpi/" + densityDpi + " screenWidth/" + screenWidth + " screenHeight/" + screenHeight);
    }
}
