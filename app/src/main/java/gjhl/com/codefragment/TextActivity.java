package gjhl.com.codefragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import gjhl.com.codefragment.verticalText.VerticalTextActivity;

public class TextActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, TextActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        // 文字下划线
        TextView tvUnderscore = findViewById(R.id.tvUnderscore);
        underscore(tvUnderscore, tvUnderscore.getText().toString());

        slash();

        // 文字省略号
        TextView tv = findViewById(R.id.tv);
        tv.setSingleLine(false);
        tv.setMaxLines(1);
        tv.setMaxEms(3);
        tv.setEllipsize(android.text.TextUtils.TruncateAt.valueOf("END"));
        tv.setText("三等奖法律");

        // 部分字体颜色改变
        partColorChange();
        partColorChange2();

        TextView tvHtmlString = findViewById(R.id.tvHtmlString);
        String html = "<h2>请使</h2><p></p>用<font color=\"#ff0000\"><b>qqqqq</b></font>牛兑换<font color=\"#00ff00\"><b>dfsafa</b></font>上网时长";
        htmlString(tvHtmlString, html);
        ttf();
    }

    private void ttf() {
        TextView tvTtf = findViewById(R.id.tvTtf);
        tvTtf.setTypeface(getTypeface(this, "LCDNOV_9.TTF"));
    }

    private Typeface getTypeface(Context context, String name) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), name);
        return typeface;
    }

    // 垂直的文字显示
    public void bVerticalText(View view) {
        VerticalTextActivity.start(this);
    }

    private void slash() {
        TextView tvSlash = findViewById(R.id.tvSlash);
        TextView tvSlash2 = findViewById(R.id.tvSlash2);
        tvSlash.setRotation(30);
        tvSlash2.setRotation(30);
    }

    public static void underscore(TextView textView, String text) {
        textView.setText(Html.fromHtml("<u>" + text + "</u>"));
    }

    public static void htmlString(TextView textView, String html) {
        textView.setText(Html.fromHtml(html));
    }

    // https://www.jianshu.com/p/1664dbcc73ba
    // 部分字体颜色改变
    private void partColorChange() {
        TextView tvPartColorChange = findViewById(R.id.tvPartColorChange);
        String exchange = getResources().getString(R.string.exchange_txt_hint);
        String info = String.format(exchange, "123", "14", "123");
        tvPartColorChange.setText(Html.fromHtml(info));
    }

    private void partColorChange2() {
        TextView tvPartColorChange = findViewById(R.id.tvPartColorChange2);
        String exchange = getResources().getString(R.string.exchange_txt_hint2);
        String info = String.format(exchange, "#婚姻家庭#", "我和王某有借款纠纷，我不想还款了想发律师函给王某，让他不要讨债刚到感...");
        tvPartColorChange.setText(Html.fromHtml(info));
    }
}
