package gjhl.com.codefragment.html;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.image.LongImageActivity;

// html 内容展示
public class HtmlActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, HtmlActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.html_activity);
        WebView webView = findViewById(R.id.webview);
        webView.loadData("<p>dslfajljsdkalf</p>","text/html", "UTF-8");
    }
}
