package gjhl.com.codefragment.view.anim.wave;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * Created by AIRY on 1/8/2016.
 * https://blog.csdn.net/androidmsky/article/details/52103432
 * https://www.2cto.com/kf/201701/591316.html
 */

public class CPMikeView extends View {
    private static final String TAG = "WavaBezierProgress";
    private final int WAVE_WIDTH = 100; // 波浪长度
    private final int WAVE_HEIGHT = 10; // 波浪高度

    private Paint mPaint;
    private Path mPath;
    private int mOffset;
    private int mscreenHeight;
    private int mscreenWidth;
    /**
     *
     */
    private int mWaveCount;
    private int mCenterY;


    public CPMikeView(Context context) {
        super(context);
    }

    public CPMikeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CPMikeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mPath = new Path();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        /*
         * 用动画控制布局往右跑
         * */
        ValueAnimator animator = ValueAnimator.ofInt(0, WAVE_WIDTH);
        animator.setDuration(5000);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(animation -> {
            mOffset = (int) animation.getAnimatedValue();
            postInvalidate();
        });
        animator.start();
    }
    /*
    *
    * 测量初始化精度条
    * */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mscreenWidth = MeasureSpec.getSize(widthMeasureSpec);
        mscreenHeight = MeasureSpec.getSize(heightMeasureSpec);
        mWaveCount = (int) Math.round(mscreenWidth/WAVE_WIDTH + 1.5);
        mCenterY = mscreenHeight*50/100;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPath.reset();
        mPath.moveTo(-WAVE_WIDTH + mOffset, mCenterY); // 控制起始点
        for (int i = 0; i < mWaveCount; i++) { // 一个屏幕画几个波浪
            mPath.quadTo(-WAVE_WIDTH*3/4 + i*WAVE_WIDTH + mOffset, mCenterY + WAVE_HEIGHT, -WAVE_WIDTH/2 + i*WAVE_WIDTH + mOffset, mCenterY); //WAVE_HEIGHT 是波纹的高度
            mPath.quadTo(-WAVE_WIDTH/4 + i*WAVE_WIDTH + mOffset, mCenterY - WAVE_HEIGHT, i*WAVE_WIDTH + mOffset, mCenterY); //WAVE_HEIGHT 是波纹的高度
        }
        mPath.lineTo(mscreenWidth, mscreenHeight);
        mPath.lineTo(0, mscreenHeight);
        mPath.close();
        canvas.drawPath(mPath, mPaint);
    }
}
