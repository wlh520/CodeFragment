package gjhl.com.codefragment.view.anim;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.view.AddViewActivity;

public class RotateActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, RotateActivity.class);
        activity.startActivity(intent);
    }

    private boolean isRotation = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rotate_activity);
        View vAll = findViewById(R.id.vAll);
        findViewById(R.id.bButton).setOnClickListener(v -> {
            isRotation = !isRotation;

            if (isRotation)
                vAll.setRotation(180);
            else vAll.setRotation(0);
        });
    }
}
