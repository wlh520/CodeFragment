package gjhl.com.codefragment.view.anim.hideAndShow;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class VerticalHideAndShowActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, VerticalHideAndShowActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vertical_hide_and_show_activity);

        TextView tv = findViewById(R.id.tv);
        AnimatorUtil animator = new AnimatorUtil(tv);
        findViewById(R.id.button).setOnClickListener(v->{
            animator.hide();
        });

        findViewById(R.id.button2).setOnClickListener(v -> {
            animator.show();
        });

        TextView tvShowAndHide = findViewById(R.id.tvShowAndHide);
        findViewById(R.id.bShowAndHide).setOnClickListener(v->{
            AnimatorUtil.hideAndShow(tvShowAndHide);
        });
    }
}
