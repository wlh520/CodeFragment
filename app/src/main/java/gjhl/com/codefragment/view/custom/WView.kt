package gjhl.com.codefragment.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import gjhl.com.codefragment.R

// 自定义view kotlin
// view 复用的时候，id 重复会只更新第一个找到的id
// 解决办法 用 getChildAt
//   private val fl by lazy { getChildAt(0) as  FrameLayout}
//    private val cv by lazy { fl.getChildAt(0) as ContentView }
class WView : ConstraintLayout {

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private val cl by lazy { getChildAt(0) as ConstraintLayout }
    private val b by lazy { cl.getChildAt(1) as Button }
    private fun init(context: Context) {
        LayoutInflater.from(context).inflate(R.layout.info_voice_view, this)
        b.setOnClickListener { v: View? -> Toast.makeText(context, "tvButton", Toast.LENGTH_SHORT).show() }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
    }
}