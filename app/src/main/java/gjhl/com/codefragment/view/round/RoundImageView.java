package gjhl.com.codefragment.view.round;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;

import gjhl.com.codefragment.R;

/**
 * Created by Administrator on 2018/11/28 0028.
 * 自定义圆角ImageView
 */

public class RoundImageView extends AppCompatImageView {
    private static final String TAG = "RoundImageView";
    public RoundImageView(Context context) {
        super(context);
    }

    public RoundImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RoundImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private Paint paint;
    private Path path;
    private int viewWidth, viewHeight, diameter = 24;

    {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        path = new Path();
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.MyView);
        diameter = array.getDimensionPixelSize(R.styleable.MyView_roundSize, 24); // xxhdpi px 3:1
        array.recycle();  //释放资源
//        Log.i(TAG, "size:"+diameter);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        viewWidth = getWidth();
        viewHeight = getHeight();
        Bitmap rawBitmap = getBitmap(getDrawable());
        if (rawBitmap != null) {
            setPaintShader(rawBitmap);
            setRoundPath();
            canvas.drawPath(path, paint);
        } else super.onDraw(canvas);
    }

    private void setRoundPath() {
        arcTo(0, 0, -180, 90);
        arcTo(viewWidth - diameter, 0, -90, 90);
        arcTo(viewWidth - diameter, viewHeight - diameter, 0, 90);
        arcTo(0, viewHeight - diameter, 90, 90);
    }

    private void arcTo(float leftDp, float topDp, float startAngle,
                  float sweepAngle) {
        path.arcTo(leftDp, topDp, leftDp + diameter, topDp + diameter, startAngle, sweepAngle, false);
    }

    /*
    左上角 和 右上角 圆角
    private void setRoundPath() {
        arcTo(0, 0, -180, 90);
        arcTo(viewWidth - diameter, 0, -90, 90);
        arcTo(viewWidth, viewHeight, 0, 0);
        arcTo2(0, viewHeight, 90, 0);
    }

    private void arcTo(float leftDp, float topDp, float startAngle,
                       float sweepAngle) {
        path.arcTo(leftDp, topDp, leftDp + diameter, topDp + diameter, startAngle, sweepAngle, false);
    }

    private void arcTo2(float leftDp, float topDp, float startAngle,
                        float sweepAngle) {
        path.arcTo(leftDp, topDp, leftDp, topDp + diameter, startAngle, sweepAngle, false);
    }*/

    private void setPaintShader(Bitmap rawBitmap) {
        Shader shader = new BitmapShader(rawBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        paint.setShader(shader);
    }

    private Bitmap getBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof ColorDrawable) {
            Rect rect = drawable.getBounds();
            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;
            int color = ((ColorDrawable) drawable).getColor();
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawARGB(Color.alpha(color), Color.red(color), Color.green(color), Color.blue(color));
            return bitmap;
        } else {
            return null;
        }
    }
}
