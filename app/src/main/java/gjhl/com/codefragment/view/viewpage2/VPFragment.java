package gjhl.com.codefragment.view.viewpage2;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import gjhl.com.codefragment.R;

public class VPFragment extends Fragment {
    private static final String TAG = "VPFragment";
    public static VPFragment instance() {
        return new VPFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: " + this);
        return inflater.inflate(R.layout.vp_fragment, container, false);
    }
}
