package gjhl.com.codefragment.view.rz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.view.rz.viewpage.RZViewpage;

import androidx.appcompat.app.AppCompatActivity;

public class RZActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, RZActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rz_activity);
        RZViewpage rzViewpage = findViewById(R.id.rzViewpage);
        String COMMON_PATH = Environment.getExternalStorageDirectory() + "/a/";
        rzViewpage.initView(COMMON_PATH, 5);
    }
}
