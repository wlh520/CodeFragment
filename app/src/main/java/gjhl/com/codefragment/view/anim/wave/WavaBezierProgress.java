package gjhl.com.codefragment.view.anim.wave;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * Created by AIRY on 1/8/2016.
 * https://blog.csdn.net/androidmsky/article/details/52103432
 * https://www.2cto.com/kf/201701/591316.html
 */

public class WavaBezierProgress extends View {
    private static final String TAG = "WavaBezierProgress";
    private Paint mPaint;

    private Path mPath;
    private int mOffset;
    private int mscreenHeight;
    private int mscreenWidth;
    /**
     *
     */
    private int mWaveCount;
    private int mCenterY;


    public WavaBezierProgress(Context context) {
        super(context);
    }

    public WavaBezierProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public WavaBezierProgress(Context context, AttributeSet attrs) {
        super(context, attrs);

        mPath = new Path();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        /*
         * 用动画控制布局往右跑
         * */
        ValueAnimator animator = ValueAnimator.ofInt(0, 1000);
        animator.setDuration(5000);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(animation -> {
            mOffset = (int) animation.getAnimatedValue();
            postInvalidate();
        });
        animator.start();
    }
    /*
    *
    * 测量初始化精度条
    * */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mscreenWidth = MeasureSpec.getSize(widthMeasureSpec);
        mscreenHeight = MeasureSpec.getSize(heightMeasureSpec);
        mWaveCount = (int) Math.round(mscreenWidth/1000 + 1.5);
        mCenterY = (mscreenHeight / 100) * 10;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPath.reset();
        mPath.moveTo(-1000 + mOffset, mCenterY); // 控制起始点
        for (int i = 0; i < mWaveCount; i++) { // 一个屏幕画几个波浪
            mPath.quadTo(-750 + i*1000 + mOffset, mCenterY + 100, -500 + i*1000 + mOffset, mCenterY); //100是波纹的高度
            mPath.quadTo(-250 + i*1000 + mOffset, mCenterY - 100, i*1000 + mOffset, mCenterY); //100是波纹的高度
        }
        mPath.lineTo(mscreenWidth, mscreenHeight);
        mPath.lineTo(0, mscreenHeight);
        mPath.close();
        canvas.drawPath(mPath, mPaint);
    }
}
