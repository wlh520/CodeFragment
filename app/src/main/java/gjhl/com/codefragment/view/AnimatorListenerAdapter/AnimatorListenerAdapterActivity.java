package gjhl.com.codefragment.view.AnimatorListenerAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivityAnimatorListenerAdapterBinding;
import gjhl.com.codefragment.view.BaseViewActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

public class AnimatorListenerAdapterActivity extends AppCompatActivity {

    private ActivityAnimatorListenerAdapterBinding mBinding;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, AnimatorListenerAdapterActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_animator_listener_adapter);
        mBinding.bAnimator.setOnClickListener(v->tipAnimator());
        mBinding.bAnimator2.setOnClickListener(v->tipAnimator2());
        mBinding.bAnimator3.setOnClickListener(v->startRotationAnim());
    }

    public void tipAnimator() {
        mBinding.bAnimator.setClickable(false);
        Log.i("WWH", "tipAnimator: " + mBinding.bAnimator.isClickable());
        // 闪烁
        ObjectAnimator dismissAnimator = ObjectAnimator.ofFloat(mBinding.vTip, "alpha", 1.0f, 0f);
        dismissAnimator.setDuration(500);
        dismissAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                mBinding.vTip.setAlpha(1.0f);
                mBinding.vTip.setVisibility(View.GONE);
                mBinding.bAnimator.setClickable(true);
                Log.i("WWH", "onAnimationEnd: " + mBinding.bAnimator.isClickable());
            }
        });
        dismissAnimator.start();
    }

    public void tipAnimator2() {
        mBinding.bAnimator.setClickable(false);
        Log.i("WWH", "tipAnimator: " + mBinding.bAnimator.isClickable());
        ObjectAnimator dismissAnimator = ObjectAnimator.ofFloat(mBinding.vTip, "alpha", 1.0f, 0f);
        dismissAnimator.setDuration(500);
        dismissAnimator.setRepeatCount(1);
        dismissAnimator.setInterpolator(new LinearInterpolator());
        dismissAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                Log.i("WWH", "onAnimationEnd: " + mBinding.bAnimator.isClickable());
                mBinding.vTip.setAlpha(1.0f);
                mBinding.vTip.setVisibility(View.GONE);
                mBinding.bAnimator.setClickable(true);
            }
        });
        dismissAnimator.start();
    }

    private void startRotationAnim() {
        Log.d("heatgiftrl", "startRotationAnim()1");
        ObjectAnimator animator = null;
        //免费礼物抖动动画
        if (mBinding.vTip != null) {
            if (animator == null) {
                animator = ObjectAnimator.ofFloat(mBinding.vTip, "rotation", 0, 15, 0, -15, 0);
                animator.setRepeatCount(1);
                animator.setInterpolator(new LinearInterpolator());
                animator.setDuration(200);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Log.d("heatgiftrl", "startRotationAnim()2");
                        super.onAnimationEnd(animation);
                    }
                });
            }
            animator.start();
        }
    }
}
