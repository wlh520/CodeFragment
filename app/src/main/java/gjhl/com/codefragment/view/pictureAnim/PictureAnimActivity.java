package gjhl.com.codefragment.view.pictureAnim;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.view.pictureAnim.anim.Anim;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimBox;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimChessboard;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimLadder;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimShanXingZhanKai;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimShiZiXingKuoZhan;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimSuiJiXianTiao;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimWheel;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimWindowShadow;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimXiangNeiRongJie;
import gjhl.com.codefragment.view.pictureAnim.anim.AnimYuanXingKuoZhan;
import gjhl.com.codefragment.view.pictureAnim.anim.EnterAnimLayout;

// https://blog.csdn.net/eyishion/article/details/53728913
public class PictureAnimActivity extends Activity {
    private EnterAnimLayout enterAnimLayout;
    private ImageView ivBackground;
    private ImageView ivOutShow;
    private boolean isWaterfall = true;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, PictureAnimActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picture_anim_activity);
        enterAnimLayout = findViewById(R.id.eal);
        ivBackground = findViewById(R.id.ivBackground);
        ivOutShow = findViewById(R.id.ivOutShow);
        ivOutShow.setImageResource(R.drawable.waterfall);
    }

    // 布局未完成时 h 为0 会没效果 :用 invisible
    public void cBaiYeChuang(View view) {
        changeImage();
        Anim anim = new AnimWindowShadow(enterAnimLayout);
        anim.startAnimation();
    }

    public void cXiangneirongjie(View view) {
        changeImage();
        Anim anim = new AnimXiangNeiRongJie(enterAnimLayout);
        anim.startAnimation();
    }

    public void cQipan(View view) {
        changeImage();
        Anim anim = new AnimChessboard(enterAnimLayout);
        anim.startAnimation();
    }

    public void cYuanxingkuozhan(View view) {
        changeImage();
        Anim anim = new AnimYuanXingKuoZhan(enterAnimLayout);
        anim.startAnimation();
    }

    public void cHezhuang(View view) {
        changeImage();
        Anim anim = new AnimBox(enterAnimLayout);
        anim.startAnimation();
    }

    public void cJieTi(View v) {
        changeImage();
        Anim anim = new AnimLadder(enterAnimLayout);
        anim.startAnimation();
    }

    public void cLunzi(View v) {
        changeImage();
        Anim anim = new AnimWheel(enterAnimLayout);
        anim.startAnimation();
    }

    public void cShanxingzhankai(View v) {
        changeImage();
        Anim anim = new AnimShanXingZhanKai(enterAnimLayout);
        anim.startAnimation();
    }

    public void cSuijixiantiao(View v) {
        changeImage();
        Anim anim = new AnimSuiJiXianTiao(enterAnimLayout);
        anim.startAnimation();
    }

    public void cShizixingkuozhan(View v) {
        changeImage();
        Anim anim = new AnimShiZiXingKuoZhan(enterAnimLayout);
        anim.startAnimation();
    }

    private void changeImage() {
        isWaterfall = !isWaterfall;
        ivBackground.setImageResource(isWaterfall ? R.drawable.map : R.drawable.waterfall);
        ivOutShow.setImageResource(isWaterfall ? R.drawable.waterfall : R.drawable.map);
    }


}
