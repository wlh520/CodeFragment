package gjhl.com.codefragment.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import gjhl.com.codefragment.R;

// 自定义view
// view 复用的时候，id 重复会只更新第一个找到的id
// 解决办法 用 getChildAt
//   private val fl by lazy { getChildAt(0) as  FrameLayout}
//    private val cv by lazy { fl.getChildAt(0) as ContentView }
public class InfoVoiceView extends ConstraintLayout {

    private View rootView;

    public InfoVoiceView(Context context) {
        super(context);
        init(context);
    }

    public InfoVoiceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public InfoVoiceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        rootView = LayoutInflater.from(context).inflate(R.layout.info_voice_view, this);
      /*  rootView.findViewById(R.id.tvButton).setOnClickListener(v->{
            Toast.makeText(context, "tvButton", Toast.LENGTH_SHORT).show();
        });*/
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
}
