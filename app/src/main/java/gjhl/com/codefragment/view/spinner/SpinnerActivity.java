package gjhl.com.codefragment.view.spinner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import gjhl.com.codefragment.R;

// Spinner 下拉框
public class SpinnerActivity extends AppCompatActivity {
    private static final String TAG = "SpinnerActivity";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SpinnerActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spinner_activity);
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 2010; i < 2060; i++)
            arrayList.add(i+"");

        Spinner s = findViewById(R.id.s);
        initializeSpinner(s, arrayList);

        s.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {//选择item的选择点击监听事件
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                Log.i(TAG, "onItemSelected: " + arg2);
            }
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        findViewById(R.id.bPerformClick).setOnClickListener(v->{
            s.performClick();
        });


        initializeSpinner2(arrayList);
    }

    private void initializeSpinner2(ArrayList array) {
        Spinner spinner2 = findViewById(R.id.spinner2);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner2.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,array));
        int selectedItemPosition = spinner2.getSelectedItemPosition(); // 获取当前选中的元素位置
    }

    private Spinner initializeSpinner(final Spinner spinner, ArrayList array) {
        spinner.setSelection(0, true);
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, array));
        return spinner;
    }
}
