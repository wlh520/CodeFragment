package gjhl.com.codefragment.view.spinner;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MySpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<MyObject> mObjects;

    public MySpinnerAdapter(Context context, ArrayList<MyObject> objects) {
        mContext = context;
        mObjects = objects;
    }

    @Override
    public int getCount() {
        return mObjects.size();
    }

    @Override
    public MyObject getItem(int position) {
        return mObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = new TextView(mContext);
        textView.setText(mObjects.get(position).getName());
        return textView;
    }
}
