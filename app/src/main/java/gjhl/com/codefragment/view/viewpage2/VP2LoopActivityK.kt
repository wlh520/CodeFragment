package gjhl.com.codefragment.view.viewpage2

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import gjhl.com.codefragment.R
import java.util.*

class VP2LoopActivityK : AppCompatActivity() {
    private lateinit var fragments: ArrayList<Fragment>
    private lateinit var viewPager2: ViewPager2
    private val mHandler = Handler(Looper.getMainLooper())
    private val loopTime = 5
    private var i = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.viewpage2_activity)
        viewPager2 = findViewById(R.id.vp)
        initVP()
        loopVP()
    }

    private fun loopVP() {
        i++
        i %= fragments.size
        if (i == 0) viewPager2.setCurrentItem(i, false) else viewPager2.setCurrentItem(i, true)
        mHandler.removeCallbacksAndMessages(null)
        mHandler.postDelayed({ loopVP() }, (loopTime * 1000).toLong())
    }

    private fun initVP() {
        fragments = ArrayList()
        fragments.add(VPFragment())
        fragments.add(VPFragment())
        viewPager2.adapter = object : FragmentStateAdapter(this) {
            override fun createFragment(position: Int): Fragment {
                return fragments[position]
            }

            override fun getItemCount(): Int {
                return fragments.size
            }
        }
    }

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, VP2LoopActivity::class.java)
            activity.startActivity(intent)
        }

        private const val TAG = "Viewpage2Activity"
    }
}
