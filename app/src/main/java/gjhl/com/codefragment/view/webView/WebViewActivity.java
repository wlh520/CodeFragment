package gjhl.com.codefragment.view.webView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class WebViewActivity extends AppCompatActivity {
    public static void start(Activity activity, String url) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra("url", url);
        activity.startActivity(intent);
    }

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view_activity);
        bindViews();
    }

    private void bindViews() {
        String url = getIntent().getStringExtra("url");
        webView = (WebView) findViewById(R.id.wv);
      /*  val webSettings: WebSettings = web.getSettings()
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.useWideViewPort = true //关键点
        webSettings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
        webSettings.displayZoomControls = false
        webSettings.databaseEnabled = true
        webSettings.javaScriptEnabled = true // 设置支持javascript脚本
        webSettings.allowFileAccess = true // 允许访问文件
        webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        webSettings.builtInZoomControls = true // 设置显示缩放按钮
        webSettings.setSupportZoom(false) // 支持缩放
        webSettings.loadWithOverviewMode = true
        webSettings.mediaPlaybackRequiresUserGesture = false
        webSettings.allowUniversalAccessFromFileURLs = true*/
//        setCache(webSettings)

        webView.loadUrl(url);
        //系统默认会通过手机浏览器打开网页，为了能够直接通过WebView显示网页，则必须设置
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //使用WebView加载显示url
                view.loadUrl(url);
                //返回true
                return true;
            }
        });
    }

/*    private fun setCache(webSettings: WebSettings) {
        //设置缓存模式
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK)
        // 开启DOM storage API 功能
        webSettings.setDomStorageEnabled(true)
        // 开启database storage API功能
        webSettings.setDatabaseEnabled(true)
        val cacheDirPath = filesDir.absolutePath + "/webcache"
        //        Log.i("cachePath", cacheDirPath)
        // 设置数据库缓存路径
        webSettings.setAppCachePath(cacheDirPath)
        webSettings.setAppCacheEnabled(true)
        //        Log.i("databasepath", settings.getDatabasePath())
    }*/

}