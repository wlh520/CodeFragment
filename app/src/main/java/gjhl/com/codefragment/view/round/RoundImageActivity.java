package gjhl.com.codefragment.view.round;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.glide.ImageLoad;
import gjhl.com.codefragment.view.AddViewActivity;

public class RoundImageActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, RoundImageActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.round_image_activity);
        loadCircularCorner();
    }

    private void loadCircularCorner() {
        ImageLoad.loadCenterCrop(this, (ImageView) findViewById(R.id.riv), "http://imgsrc.baidu.com/forum/pic/item/4bed2e738bd4b31c2958c52c87d6277f9f2ff8c5.jpg");
        ImageLoad.loadCenterCrop(this, (ImageView) findViewById(R.id.riv2), "http://imgsrc.baidu.com/forum/pic/item/4bed2e738bd4b31c2958c52c87d6277f9f2ff8c5.jpg");
    }
}
