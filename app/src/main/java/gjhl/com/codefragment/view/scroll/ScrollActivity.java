package gjhl.com.codefragment.view.scroll;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.view.BaseViewActivity;

public class ScrollActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ScrollActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroll_activity);
    }

}
