package gjhl.com.codefragment.view.touch;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import gjhl.com.codefragment.R;

/**
 * Created by liwen on 2016/11/2.
 */
public class DragView extends View {

    private int lastY;
    private int lastX;
    private LinearLayout.LayoutParams mParams;
    private int offsetX;
    private int mWidth;
    private Context mContext;
    private int offsetY;
    private int mHeight;

    public DragView(Context context) {
        this(context, null);
    }

    public DragView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DragView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    private void initView() {
        setBackgroundResource(R.mipmap.drag);
        getWindowValues();
    }

    private void getWindowValues() {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        mWidth = wm.getDefaultDisplay().getWidth();
        mHeight = wm.getDefaultDisplay().getHeight();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int x = (int) event.getX();
        int y = (int) event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = (int) event.getX();
                lastY = (int) event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                mParams = (LinearLayout.LayoutParams) getLayoutParams();

                offsetX = getLeft() + x - lastX;
                offsetY = getTop() + y - lastY;

                if (offsetX <= 0) {
                    offsetX = 0;
                }

                if (offsetX + DensityUtil.dip2px(mContext, getDpValue()) >= mWidth) {
                    offsetX = mWidth - DensityUtil.dip2px(mContext, getDpValue());
                }

                if (offsetY <= 0) {
                    offsetY = 0;
                }

                if (offsetY + DensityUtil.dip2px(mContext, getDpValue() + 25) >= mHeight) { //25是状态栏的高度
                    offsetY = mHeight - DensityUtil.dip2px(mContext, getDpValue() + 25);
                }

                mParams.leftMargin = offsetX;
                mParams.topMargin = offsetY;
                setLayoutParams(mParams);
                break;
            case MotionEvent.ACTION_UP:

                if (offsetX > mWidth / 2) {
                    mParams.leftMargin = mWidth - DensityUtil.dip2px(mContext, getDpValue());
                } else {
                    mParams.leftMargin = 0;
                }
                mParams.topMargin = offsetY;
                setLayoutParams(mParams);
                break;
        }

        return true;

    }

    private int getDpValue() {
        return 50; //布局的宽高大小
    }
}
