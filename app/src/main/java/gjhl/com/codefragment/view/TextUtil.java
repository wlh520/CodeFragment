package gjhl.com.codefragment.view;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.util.List;

import gjhl.com.codefragment.R;

/**
 * 作者：Abeing
 * <p>
 * ： 2019/2/27 13:46
 */
public class TextUtil {
    public static void underscore(TextView textView, String text) {
        textView.setText(Html.fromHtml("<u>"+ text +"</u>"));
    }

    public static void flipper(ViewFlipper viewFlipper, List<String> flipperData, Context context) {
        for (int i = 0; i < 5; i++) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_custom, null);
            TextView textView = view.findViewById(R.id.textView);
            textView.setText(flipperData.get(i));
            viewFlipper.addView(view);
        }
    }

    public static void underscoreFlipper(ViewFlipper viewFlipper, List<String> flipperData, Context context) {
        for (int i = 0; i < 5; i++) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_custom, null);
            TextView textView = view.findViewById(R.id.textView);
            underscore(textView, flipperData.get(i));
            viewFlipper.addView(view);
        }
    }
}
