package gjhl.com.codefragment.view.anim.wx;

import android.animation.ObjectAnimator;
import android.view.View;

public class WXAnimatorUtil {
    private ObjectAnimator hide1;
    private ObjectAnimator hide2;

    private ObjectAnimator show1;
    private ObjectAnimator show2;

    public WXAnimatorUtil(View v) {
        int duration = 200;
        hide1 = ObjectAnimator.ofFloat(v, "scaleX", 1f, 0f);
        hide1.setDuration(duration);

        v.post(new Runnable() {
            @Override
            public void run() {
                // v.getWidth() 只有在 post 的时候才有值
                hide2 = ObjectAnimator.ofFloat(v, "translationX", 0f, v.getWidth() / 2);
                hide2.setDuration(duration);
            }
        });

        show1 = ObjectAnimator.ofFloat(v, "scaleX", 0f, 1f);
        show1.setDuration(duration);

        v.post(new Runnable() {
            @Override
            public void run() {
                show2 = ObjectAnimator.ofFloat(v, "translationX", v.getWidth() / 2, 0f);
                show2.setDuration(duration);
            }
        });
    }

    boolean isShow;
    public void startAnimator() {
        isShow = !isShow;
        if (isShow)
            hide();
        else
            show();
    }

    private void hide() {
        hide1.start();
        hide2.start();
    }

    private void show() {
        show1.start();
        show2.start();
    }
}
