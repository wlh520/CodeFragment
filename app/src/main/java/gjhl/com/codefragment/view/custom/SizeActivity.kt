package gjhl.com.codefragment.view.custom

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import gjhl.com.codefragment.R
import gjhl.com.codefragment.databinding.SizeActivityBinding
import gjhl.com.codefragment.exit.Util


// 代码设置布局宽高
// ScrollView嵌套RecyclerView https://www.jianshu.com/p/5490d14a0a45
class SizeActivity : AppCompatActivity() {
    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, SizeActivity::class.java)
            activity.startActivity(intent)
        }
    }

    lateinit var binding: SizeActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.size_activity)

        val strings: ArrayList<String> = ArrayList<String>()
        var i = 0
        while (i < 10) {
            strings.add("" + i)
            i++
        }

        val adapter = SizeAdapter(strings)
        val rv = findViewById<RecyclerView>(R.id.rv)
        val lp = rv.getLayoutParams() as LinearLayout.LayoutParams
        lp.height = Util.dip2px(200.0f)*10
        rv.setLayoutParams(lp)
        val linearLayoutManager = object : LinearLayoutManager(this) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        linearLayoutManager.canScrollHorizontally()
        rv.layoutManager = linearLayoutManager
        rv.adapter = adapter
    }
}
