package gjhl.com.codefragment.view.anim.frame;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.widget.ImageView;

import java.lang.ref.SoftReference;

import gjhl.com.codefragment.MyApplication;


/**
 * Created by adolph on 2017/9/27.
 */
public class AnimationsContainer {
    public int delayTime = 100;
    public int firstDelayTime = 0;
    private int resId;
    private Context mContext = MyApplication.getContext();
    private int[] mAnimFrames;
    private boolean isOne = false;

    public AnimationsContainer() {
    }

    public AnimationsContainer setResId(int resId) {
        this.resId = resId;
        mAnimFrames = getData(resId);
        return this;
    }

    public AnimationsContainer setAnimFrames(int[] mAnimFrames) {
        this.mAnimFrames = mAnimFrames;
        return this;
    }

    // 一帧图片的时间
    public AnimationsContainer setDelayTime(int times) {
        this.delayTime = times;
        return this;
    }

    public AnimationsContainer setFirstDelayTime(int firstTimes) {
        this.firstDelayTime = firstTimes;
        return this;
    }

    public AnimationsContainer setIsOne(boolean isOne) {
        this.isOne = isOne;
        return this;
    }

    public FramesSequenceAnimation createProgressDialogAnim(ImageView imageView) {
        if (firstDelayTime == 0) {
            firstDelayTime = delayTime;
        }
        return new FramesSequenceAnimation(imageView, mAnimFrames, delayTime, firstDelayTime);
    }

    public class FramesSequenceAnimation {
        private int[] mFrames;
        private int mIndex;
        private boolean mShouldRun;
        private boolean mIsRunning;
        private SoftReference<ImageView> mSoftReferenceImageView;
        private Handler mHandler;
        private int mDelayMillis, mfirstDelayMillis;
        private OnAnimationStoppedListener mOnAnimationStoppedListener;
        private boolean isLast;
        private Bitmap mBitmap = null;
        private BitmapFactory.Options mBitmapOptions;

        public FramesSequenceAnimation(ImageView imageView, int[] frames, int delayTimes, int firstDelayTimes) {
            mHandler = new Handler();
            mFrames = frames;
            mIndex = -1;
            mSoftReferenceImageView = new SoftReference<ImageView>(imageView);
            mShouldRun = false;
            mIsRunning = false;
            mDelayMillis = delayTimes;
            mfirstDelayMillis = firstDelayTimes;

            imageView.setImageResource(mFrames[0]);

            Bitmap bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            int width = bmp.getWidth();
            int height = bmp.getHeight();
            Bitmap.Config config = bmp.getConfig();
            mBitmap = Bitmap.createBitmap(width, height, config);
            mBitmapOptions = new BitmapFactory.Options();
            mBitmapOptions.inBitmap = mBitmap;
            mBitmapOptions.inMutable = true;
            mBitmapOptions.inSampleSize = 1;
        }

        private int getNext() {
            mIndex++;
            if (mIndex >= mFrames.length)
                mIndex = 0;
            return mFrames[mIndex];
        }

        public synchronized void start() {
            mShouldRun = true;
            if (mIsRunning)
                return;

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    ImageView imageView = mSoftReferenceImageView.get();
                    if (!mShouldRun || imageView == null) {
                        mIsRunning = false;
                        if (mOnAnimationStoppedListener != null) {
                            mOnAnimationStoppedListener.AnimationStopped();
                        }
                        return;
                    }

                    if (isOne && mIndex == mFrames.length - 2) {
                        stop();
                    }

                    mIsRunning = true;
                    if (imageView.isShown()) {
                        int imageRes = getNext();
                        if (mBitmap != null) {
                            Bitmap bitmap = null;
                            try {
                                bitmap = BitmapFactory.decodeResource(imageView.getResources(), imageRes, mBitmapOptions);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (bitmap != null) {
                                imageView.setImageBitmap(bitmap);
                            } else {
                                imageView.setImageResource(imageRes);
                                mBitmap.recycle();
                                mBitmap = null;
                            }
                        } else {
                            imageView.setImageResource(imageRes);
                        }
                    }
                    if (0 == mIndex) {
                        mHandler.postDelayed(this, mfirstDelayMillis);
                    } else {
                        mHandler.postDelayed(this, mDelayMillis);
                    }

                }
            };

            mHandler.post(runnable);
        }

        public synchronized void stop() {
            mShouldRun = false;
        }

        public synchronized void stopAndFirstFrame() {
            mShouldRun = false;
            if (mSoftReferenceImageView != null && mSoftReferenceImageView.get() != null && mFrames != null && mFrames.length > 0) {
                mIndex = 0;
                mSoftReferenceImageView.get().setImageResource(mFrames[0]);
            }
        }

        public synchronized void stopAndClear() {
            mShouldRun = false;
            if (mSoftReferenceImageView != null) {
                mIndex = 0;
                mSoftReferenceImageView.get().setImageDrawable(null);
            }
        }

        public synchronized void recycle() {
            mShouldRun = false;
            if (mBitmap != null) {
                mBitmap.recycle();
                mBitmap = null;
            }
            if (mSoftReferenceImageView != null) {
                mSoftReferenceImageView.clear();
            }
        }

        public void setOnAnimStopListener(OnAnimationStoppedListener listener) {
            this.mOnAnimationStoppedListener = listener;
        }
    }

    private int[] getData(int resId) {
        TypedArray array = mContext.getResources().obtainTypedArray(resId);

        int len = array.length();
        int[] intArray = new int[array.length()];

        for (int i = 0; i < len; i++) {
            intArray[i] = array.getResourceId(i, 0);
        }
        array.recycle();
        return intArray;
    }

    public interface OnAnimationStoppedListener {
        void AnimationStopped();
    }
}