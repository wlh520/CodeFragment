package gjhl.com.codefragment.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivityBaseViewBinding;
import gjhl.com.codefragment.view.AnimatorListenerAdapter.AnimatorListenerAdapterActivity;
import gjhl.com.codefragment.view.anim.AnimatorActivity;
import gjhl.com.codefragment.view.anim.RotateActivity;
import gjhl.com.codefragment.view.anim.move.MoveAnimActivity;
import gjhl.com.codefragment.view.cardView.CardViewActivity;
import gjhl.com.codefragment.view.custom.CustomViewActivity;
import gjhl.com.codefragment.view.custom.SizeActivity;
import gjhl.com.codefragment.view.cute.CuteViewActivity;
import gjhl.com.codefragment.view.extensions.ViewExtensionsKt;
import gjhl.com.codefragment.view.floating.FloatingActivity;
import gjhl.com.codefragment.view.material.MaterialButtonActivity;
import gjhl.com.codefragment.view.pictureAnim.PictureAnimActivity;
import gjhl.com.codefragment.view.round.RoundImageActivity;
import gjhl.com.codefragment.recyclerView.adapterAnimator.AdapterAnimatorActivity;
import gjhl.com.codefragment.view.rz.RZActivity;
import gjhl.com.codefragment.view.scroll.ScrollActivity;
import gjhl.com.codefragment.view.spinner.ObjectSActivity;
import gjhl.com.codefragment.view.spinner.SpinnerActivity;
import gjhl.com.codefragment.view.svga.SvgaActivity;
import gjhl.com.codefragment.view.touch.TouchActivity;
import gjhl.com.codefragment.view.viewpage2.VP2LoopActivity;
import gjhl.com.codefragment.view.viewpage2.Viewpage2Activity;
import gjhl.com.codefragment.view.webView.WebViewActivity;
import gjhl.com.codefragment.view.webView.WebViewMethodActivity;

public class BaseViewActivity extends AppCompatActivity {
    private static final String TAG = "BaseViewActivity";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, BaseViewActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBaseViewBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_base_view);
        binding.setActivity(this);

        ViewExtensionsKt.onSingleClick(binding.b2s1, 2000, true, v->{
            Log.i(TAG, "onSingleClick: binding.b2s1");
            return null;
        });
        ViewExtensionsKt.onSingleClick(binding.b2s2, 2000, true, v->{
            Log.i(TAG, "onSingleClick: binding.b2s2");
            return null;
        });
    }

    public void cMove(View view) {
        MoveAnimActivity.start(this);
    }

    public void cRv(View view) {
        AdapterAnimatorActivity.Companion.start(this);
    }

    public void cFloating(View view) {
        FloatingActivity.start(this);
    }

    public void cRz(View view) {
        RZActivity.start(this);
    }

    public void cCardView(View view) {
        CardViewActivity.start(this);
    }

    public void cVP2Loop(View view) {
        VP2LoopActivity.start(this);
    }

    public void cViewpage2(View v) {
        Viewpage2Activity.start(this);
    }
    public void cCuteView() {
        CuteViewActivity.start(this);
    }

    public void cPictureAnim(View v) {
        PictureAnimActivity.start(this);
    }

    public void cRound(View view) {
        RoundImageActivity.start(this);
    }

    public void cAnimatorListener() {
        AnimatorListenerAdapterActivity.start(this);
    }

    public void cShowSvga() {
        SvgaActivity.start(this);
    }

    public void cShowFrame() {
        AnimatorActivity.start(this);
    }

    public void cCustomView() {
        CustomViewActivity.start(this);
    }

    public void cSizeActivity() {
        SizeActivity.Companion.start(this);
    }

    public void cRotateActivity() {
        RotateActivity.start(this);
    }

    public void cMaterialButtonActivity() {
        MaterialButtonActivity.Companion.start(this);
    }

    public void cWebView (View view) {
        WebViewActivity.start(this, "http://www.baidu.com");
    }

    public void cWebViewMethod (View view) {
        WebViewMethodActivity.start(this, "file:///android_asset/androidMethod.html");
    }

    public void cSpinner(View view) {
        SpinnerActivity.start(this);
    }

    public void cSpinnerObject(View view) {
        ObjectSActivity.Companion.start(this);
    }

    public void cTouchAct(View view) {
        TouchActivity.start(this);
    }

    // 滚动条
    public void cScroll(View view) {
        ScrollActivity.start(this);
    }
}
