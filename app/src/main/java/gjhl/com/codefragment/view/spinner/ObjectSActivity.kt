package gjhl.com.codefragment.view.spinner

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import gjhl.com.codefragment.R

class ObjectSActivity:AppCompatActivity() {
    companion object {
        fun start(a: Activity) {
            val i = Intent(a, ObjectSActivity::class.java)
            a.startActivity(i)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.object_s_activity)
        val s = findViewById<Spinner>(R.id.spinner2)
        val list = arrayListOf(MyObject("option 1"), MyObject("option 3"), MyObject("option 2"))
        val adapter = MySpinnerAdapter(this, list)
        s.adapter = adapter
    }
}