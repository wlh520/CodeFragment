package gjhl.com.codefragment.view.cute;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivityCuteViewBinding;

// view裁剪 https://hencoder.com/ui-1-4/
public class CuteViewActivity extends AppCompatActivity {

    private ActivityCuteViewBinding binding;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, CuteViewActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cute_view);
        binding.setActivity(this);
    }

    public void cUpdateVoiceView1() {
        binding.vVoice1.decreaseWidth();
        binding.vVoice1.invalidate();
    }
}
