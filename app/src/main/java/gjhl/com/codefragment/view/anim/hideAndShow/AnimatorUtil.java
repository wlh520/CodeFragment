package gjhl.com.codefragment.view.anim.hideAndShow;

import android.animation.ObjectAnimator;
import android.view.View;

public class AnimatorUtil {
    private ObjectAnimator hide1;
    private ObjectAnimator hide2;

    private ObjectAnimator show1;
    private ObjectAnimator show2;

    public AnimatorUtil(View v) {
        int duration = 500;
        hide1 = ObjectAnimator.ofFloat(v, "scaleY", 1f, 0f);
        hide1.setDuration(duration);

        v.post(() -> {
            // v.getWidth() 只有在 post 的时候才有值
            hide2 = ObjectAnimator.ofFloat(v, "translationY", 0f, -v.getWidth() / 2);
            hide2.setDuration(duration);
        });

        show1 = ObjectAnimator.ofFloat(v, "scaleY", 0f, 1f);
        show1.setDuration(duration);

        v.post(new Runnable() {
            @Override
            public void run() {
                show2 = ObjectAnimator.ofFloat(v, "translationY", -v.getWidth() / 2, 0f);
                show2.setDuration(duration);
            }
        });
    }

    public static void hideAndShow(View v) {
        int length = 10;
        float[] floats = new float[length*2];
        for (int i = 0; i < length*2; i+=2) {
            floats[i] = 0;
            floats[i+1] = 1;
        }
        ObjectAnimator animator = ObjectAnimator.ofFloat(v, "alpha", floats);
        animator.setDuration(length * 1500);
        animator.start();
    }

    public void hide() {
        hide1.start();
        hide2.start();
    }

    public void show() {
        show1.start();
        show2.start();
    }
}
