package gjhl.com.codefragment.view.touch;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;

import gjhl.com.codefragment.R;

/**
 * Created by liwen on 2016/11/2.
 */
public class DragSimpleView extends AppCompatTextView {

    private static final String TAG = "DragSimpleView";
    private int lastY;
    private int lastX;
    private LinearLayout.LayoutParams mParams;
    private int offsetX;
    private int offsetY;
    private int preOffsetX;
    private int preOffsetY;

    public DragSimpleView(Context context) {
        this(context, null);
    }

    public DragSimpleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DragSimpleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int x = (int) event.getRawX();
        int y = (int) event.getRawY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = (int) event.getRawX();
                lastY = (int) event.getRawY();
                Log.i(TAG, "onTouchEvent ACTION_DOWN: lastX=" + lastX + " lastY=" + lastY);
                break;
            case MotionEvent.ACTION_MOVE:
                mParams = (LinearLayout.LayoutParams) getLayoutParams();

                offsetX = x - lastX;
                offsetY = y - lastY;

                Log.i(TAG, "onTouchEvent ACTION_MOVE: offsetX=" + offsetX + " offsetY=" + offsetY);
                mParams.leftMargin = offsetX + preOffsetX;
                mParams.topMargin = offsetY + preOffsetY;
                setLayoutParams(mParams);
                break;
            case MotionEvent.ACTION_UP:
                preOffsetX = x - lastX + preOffsetX;
                preOffsetY = y - lastY + preOffsetY; // getTop() view relative to its parent.
                break;
        }

        return true;
    }
}
