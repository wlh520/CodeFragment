package gjhl.com.codefragment.view.material

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import gjhl.com.codefragment.R
import gjhl.com.codefragment.databinding.MaterialButtonActivityBinding

// Theme.MaterialComponents.Light.NoActionBar
class MaterialButtonActivity : AppCompatActivity() {
    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, MaterialButtonActivity::class.java)
            activity.startActivity(intent)
        }
    }


    lateinit var binding: MaterialButtonActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.material_button_activity)
    }
}