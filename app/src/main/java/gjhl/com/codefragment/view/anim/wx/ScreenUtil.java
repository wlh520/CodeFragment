package gjhl.com.codefragment.view.anim.wx;

import android.content.Context;
import android.view.WindowManager;

/**
 * 作者：Abeing
 * <p>
 * 时间： 2019/4/29 14:43
 */
public class ScreenUtil {
    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay().getWidth();
    }

    public static int dp2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }
}
