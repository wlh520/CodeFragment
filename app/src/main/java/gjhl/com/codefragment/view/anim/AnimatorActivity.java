package gjhl.com.codefragment.view.anim;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.view.anim.frame.FrameAnimationActivity;
import gjhl.com.codefragment.view.anim.hideAndShow.VerticalHideAndShowActivity;
import gjhl.com.codefragment.text.horizontalScroll.MarqueeActivity;
import gjhl.com.codefragment.view.anim.move.MoveAnimActivity;
import gjhl.com.codefragment.view.anim.wave.WaveActivity;
import gjhl.com.codefragment.view.anim.wx.HorizontalAnimActivity;

public class AnimatorActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, AnimatorActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animator_activity);
        findViewById(R.id.bFrame).setOnClickListener(v -> {
            FrameAnimationActivity.start(this);
        });
        findViewById(R.id.bMarquee).setOnClickListener(v -> {
            MarqueeActivity.start(this);
        });
    }

    public void cWave(View view) {
        WaveActivity.start(this);
    }

    // 左右淡入淡出
    public void cHorizontalHideAndShow(View view) {
        HorizontalAnimActivity.start(this);
    }

    // 上下淡入淡出
    public void cVerticalHideAndShow(View view) {
        VerticalHideAndShowActivity.start(this);
    }

}
