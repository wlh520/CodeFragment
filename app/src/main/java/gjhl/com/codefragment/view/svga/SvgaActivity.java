package gjhl.com.codefragment.view.svga;

import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;

import org.jetbrains.annotations.NotNull;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivitySvgaBinding;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

// http://svga.io/index.html
// https://github.com/yyued/SVGAPlayer-Android
public class SvgaActivity extends AppCompatActivity {
    private static final String TAG = "SvgaActivity";

    private ActivitySvgaBinding mBinding;
    private SVGAParser mSVGAParser;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SvgaActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_svga);
        mSVGAParser = new SVGAParser(this);

        Handler handler = new Handler();
        handler.postDelayed(()->{
            Log.i(TAG, "onCreate: ");
            startSVGAAnim(mBinding.svgview, "temp1.svga");
        }, 1000);
    }

    private void startSVGAAnim(SVGAImageView svgaImageView, String assetsName) {
        mSVGAParser.parse(assetsName, new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity svgvaVideoEntity) {
                svgaImageView.setVisibility(View.VISIBLE);
                svgaImageView.setLoops(1);
                svgaImageView.setClearsAfterStop(false);
                svgaImageView.setFillMode(SVGAImageView.FillMode.Forward);
                SVGADrawable drawable = new SVGADrawable(svgvaVideoEntity);
                svgaImageView.setImageDrawable(drawable);
                svgaImageView.startAnimation();
            }

            @Override
            public void onError() {
                Log.i(TAG, "onError: svga");
            }
        });
    }
}
