package gjhl.com.codefragment.view.touch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

// view 跟随手指滑动
public class TouchActivity extends AppCompatActivity {

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, TouchActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.touch_activity);
    }


}
