package gjhl.com.codefragment.view.floating;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.FullScreenActivity;
import gjhl.com.codefragment.R;

// 添加权限
//   <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" />
//    <uses-permission android:name="android.permission.SYSTEM_OVERLAY_WINDOW" />
public class FloatingActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, FloatingActivity.class);
        activity.startActivity(intent);
    }

    private Button mFloatingButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.floating_activity);
        //初始化view
        initView();
    }
    //初始化view
    private void initView() {
        mFloatingButton=(Button) findViewById(R.id.floating_btn);
        mFloatingButton.setOnClickListener(this::startFloatingButtonService);
    }

    public void startFloatingButtonService(View view) {
        if (FloatingService_Button.isStarted) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {//判断系统版本
            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "当前无权限，请授权", Toast.LENGTH_SHORT).show();
                startActivityForResult(new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName())), 0);
            } else {
                startService(new Intent(this, FloatingService_Button.class));
            }
        } else {
            startService(new Intent(this, FloatingService_Button.class));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "授权失败", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "授权成功", Toast.LENGTH_SHORT).show();
                startService(new Intent(this, FloatingService_Button.class));
            }
        }
    }
}
