package gjhl.com.codefragment.view.anim.move;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class MoveAnimActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, MoveAnimActivity.class);
        activity.startActivity(intent);
    }

    private static final String TAG = "MoveAnimActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.move_anim_activity);
        vDrawer = findViewById(R.id.vDrawer);
    }

    private View vDrawer;
    private boolean isExpand = false;//是否展开
    int endMarginLeft = 2000;
    int startMarginLeft = 600;

    /**
     * 展开收起
     */
    public void cToggle(View view) {
        if (isExpand) {
            startValAnim(startMarginLeft, endMarginLeft, new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = (float) animation.getAnimatedValue();
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) vDrawer.getLayoutParams();
                    params.leftMargin = (int) value;
                    vDrawer.setLayoutParams(params);
                }
            });
        } else {

            startValAnim(endMarginLeft, startMarginLeft, new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = (float) animation.getAnimatedValue();
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) vDrawer.getLayoutParams();
                    params.leftMargin = (int) value;
                    vDrawer.setLayoutParams(params);
                }
            });
        }
        isExpand = !isExpand;
    }

    private void startValAnim(float from, float to, ValueAnimator.AnimatorUpdateListener listener) {
        ValueAnimator animator = ValueAnimator.ofFloat(from, to);
        //设置动画时长
        animator.setDuration(200);
        //设置插值器，当然你可以不用
//        animator.setInterpolator(new DecelerateInterpolator());
        //回调监听
        animator.addUpdateListener(listener);
        //启动动画
        animator.start();
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Log.i(TAG, "onAnimationEnd: ");
            }
        });
    }


}
