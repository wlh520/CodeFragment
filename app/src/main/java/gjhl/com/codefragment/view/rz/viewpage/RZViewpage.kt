package gjhl.com.codefragment.view.rz.viewpage

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import gjhl.com.codefragment.R
import gjhl.com.codefragment.util.KUtil
import kotlinx.android.synthetic.main.rz_viewpage.view.*

class RZViewpage : FrameLayout {
    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private val TAG = "RZViewpage"
    private val mHandler = Handler(Looper.myLooper()!!)
    private var i = 0
    private val files = ArrayList<Bean>()
    private var path = ""
    private var time = 15

    private fun init(context: Context) {
        LayoutInflater.from(context).inflate(R.layout.rz_viewpage, this)
    }

    fun initView(path: String, secondTime: Int) {
        this.path = path
        this.time = secondTime
        getData()
        initBanner()
        loop()
    }

    private fun getData() {
        files.clear()
        val fs = KUtil.listFileSortByName(path)
        for (f1 in fs) {
            if (KUtil.isPicture(f1)) {
                val bean = Bean()
                bean.file = f1
                bean.isPicture = true
                files.add(bean)
            } else if (KUtil.isVideo(f1)) {
                val bean = Bean()
                bean.file = f1
                bean.isPicture = false
                files.add(bean)
            }
        }
    }

    private fun loop() {
        if (files.size == 0)
            return
        nextPage()
        if (files.get(i).isPicture)
            mHandler.postDelayed({
                mHandler.removeCallbacksAndMessages(null)
                loop()
            }, time * 1000L)
    }

    private fun nextPage() {
        i++
        i %= files.size
        Log.i(TAG, "nextPage: ${files.size}")
        if (i == 0)
            viewPager.setCurrentItem(i, false)
        else
            viewPager.setCurrentItem(i, true)
    }

    var picture : RZImageFragment?=null
    private fun initBanner() {
        viewPager.adapter = object : FragmentStateAdapter(context as FragmentActivity) {
            override fun createFragment(position: Int): Fragment {
                val file = files.get(position)
                if (file.isPicture) {
                    Log.i(TAG, "createFragment: ")
                    picture = RZImageFragment.create(file.file.absolutePath)
                    return picture!!
                }
                else
                    if (files.size == 1)
                        return RZFullNoProgressVideoFragment.newInstance(
                            file.file.absolutePath,
                            true
                        ) {}
                    else
                        return RZFullNoProgressVideoFragment.newInstance(file.file.absolutePath) {
                            loop()
                        }
            }

            override fun getItemCount(): Int {
                return files.size
            }
        }
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                Log.i(TAG, "onPageSelected: $position")
               /* if (position==1)
                    picture?.initImage()*/
            }
        })
    }

    // view 生命周期
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        handler.removeCallbacksAndMessages(null)
    }
}