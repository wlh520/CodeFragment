package gjhl.com.codefragment.view.viewpage2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import java.util.ArrayList;

import gjhl.com.codefragment.R;

public class VP2LoopActivity extends AppCompatActivity {

    private ArrayList<Fragment> fragments;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, VP2LoopActivity.class);
        activity.startActivity(intent);
    }

    private static final String TAG = "Viewpage2Activity";
    private ViewPager2 viewPager2;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private int loopTime = 5;
    private int i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpage2_activity);
        viewPager2 = findViewById(R.id.vp);
        initVP();
        loopVP();
    }

    private void loopVP() {
        i++;
        i = i % fragments.size();
        if (i == 0)
            viewPager2.setCurrentItem(i, false);
        else
            viewPager2.setCurrentItem(i, true);
        mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(this::loopVP, loopTime*1000);
    }

    private void initVP() {
        fragments = new ArrayList<>();
        fragments.add(new VPFragment());
        fragments.add(new VPFragment());
        viewPager2.setAdapter(new FragmentStateAdapter(this) {
            @Override
            public Fragment createFragment(int position) {
                return fragments.get(position);
            }

            @Override
            public int getItemCount() {
                return fragments.size();
            }
        });
    }
}
