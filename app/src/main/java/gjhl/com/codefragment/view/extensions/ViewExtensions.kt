package gjhl.com.codefragment.view.extensions

import android.app.Activity
import android.content.ContextWrapper
import android.os.SystemClock
import android.view.View
import gjhl.com.codefragment.R

// https://www.jianshu.com/p/04ed8d18c335
// interval 延迟时间
// isShareSingleClick true 所有按钮共享延迟点击时间
fun View.onSingleClick(
    interval: Int=2000,
    isShareSingleClick: Boolean = true,
    listener: (View) -> Unit
) {
    setOnClickListener {
        val target = if (isShareSingleClick) getActivity(this)?.window?.decorView ?: this else this
        val millis = target.getTag(R.id.single_click_tag_last_single_click_millis) as? Long ?: 0
        if (SystemClock.uptimeMillis() - millis >= interval) {
            target.setTag(R.id.single_click_tag_last_single_click_millis, SystemClock.uptimeMillis())
            listener.invoke(this)
        }
    }
}

private fun getActivity(view: View): Activity? {
    var context = view.context
    while (context is ContextWrapper) {
        if (context is Activity) {
            return context
        }
        context = context.baseContext
    }
    return null
}