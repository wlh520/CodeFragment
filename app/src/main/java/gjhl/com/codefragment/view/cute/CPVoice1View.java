package gjhl.com.codefragment.view.cute;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import gjhl.com.codefragment.R;

public class CPVoice1View extends View {
    private static final String TAG = "CPVoice1View";
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    Bitmap bitmap;
    private int width;

    public CPVoice1View(Context context) {
        super(context);
    }

    public CPVoice1View(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CPVoice1View(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    {
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.maps);
    }

    public void decreaseWidth() {
        width+=10;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        canvas.clipRect(width, 0, getWidth(), getHeight()); // 留下的范围
        canvas.drawBitmap(bitmap, 0, 0, paint);
        canvas.restore();
    }
}