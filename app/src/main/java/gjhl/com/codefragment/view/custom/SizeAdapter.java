package gjhl.com.codefragment.view.custom;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.R;

public class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.MyViewHolder> {
    private ArrayList<String> messages;

    public SizeAdapter(ArrayList<String> messages) {
        this.messages = messages;
    }

    @NonNull
    @Override
    public SizeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.size_adapter, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SizeAdapter.MyViewHolder holder, int position) {
        holder.tv.setText(position+"");
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        TextView tv;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.tv = itemView.findViewById(R.id.tv);
        }
    }
}