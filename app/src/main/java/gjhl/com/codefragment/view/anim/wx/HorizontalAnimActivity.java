package gjhl.com.codefragment.view.anim.wx;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class HorizontalAnimActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, HorizontalAnimActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wx_anim_activity);

        WXAnimatorUtil animator = new WXAnimatorUtil(findViewById(R.id.ll));
        findViewById(R.id.button).setOnClickListener(v->{
            animator.startAnimator();
        });
    }
}
