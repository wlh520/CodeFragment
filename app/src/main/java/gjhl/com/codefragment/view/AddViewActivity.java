package gjhl.com.codefragment.view;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.view.custom.InfoVoiceView;

public class AddViewActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, AddViewActivity.class);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_view);
//        addViewNormal();

//        codeSetLayoutGravity(); // Android:layout_gravity属性
        codeSetLayoutGravity2(); // customView Android:layout_gravity属性
    }

    private void codeSetLayoutGravity2() {
        FrameLayout groupView = findViewById(R.id.ll);
        InfoVoiceView customView = new InfoVoiceView(this);
        groupView.addView(customView);
    }

    private void codeSetLayoutGravity() {
        FrameLayout groupView = findViewById(R.id.ll);
        View view = LayoutInflater.from(this).inflate(R.layout.custom, null);
        TextView textView = view.findViewById(R.id.textView);
        textView.setText("123");
        groupView.addView(view);
        groupView.post(()->{
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.BOTTOM;
            view.setLayoutParams(lp);
        });
    }

    private void addViewNormal() {
        ViewGroup groupView = findViewById(R.id.ll);
        View view = LayoutInflater.from(this).inflate(R.layout.custom, null);
        TextView textView = view.findViewById(R.id.textView);
        textView.setText("123");
        groupView.addView(view);
    }
}
