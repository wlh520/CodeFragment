package gjhl.com.codefragment.view.viewpage2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import java.util.ArrayList;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.video.VideoActivity;

public class Viewpage2Activity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, Viewpage2Activity.class);
        activity.startActivity(intent);
    }

    private static final String TAG = "Viewpage2Activity";
    private ViewPager2 viewPager2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpage2_activity);
        viewPager2 = findViewById(R.id.vp);
        initVpPicture();
    }

    private void initVpPicture() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new VPFragment());
        fragments.add(new VPFragment());
        viewPager2.setAdapter(new FragmentStateAdapter(this) {
            @Override
            public Fragment createFragment(int position) {
                return fragments.get(position);
            }

            @Override
            public int getItemCount() {
                return fragments.size();
            }
        });
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                Log.i(TAG, "onPageSelected: " + position);
            }
        });
    }
}
