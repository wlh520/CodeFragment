package gjhl.com.codefragment.view.rz.viewpage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import gjhl.com.codefragment.R
import gjhl.com.codefragment.util.WUtil

class RZImageFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val wImageView = WImageView(layoutInflater, container)
        val image = requireArguments().getString("image")?:""
        wImageView.bind(image)
        return wImageView.view
    }

    companion object {

        /** Creates a Fragment for a given [Card]  */
        fun create(image: String): RZImageFragment {
            val fragment = RZImageFragment()
            fragment.arguments = Bundle().apply {
                putString("image", image)
            }
            return fragment
        }
    }

    class WImageView(layoutInflater: LayoutInflater, container: ViewGroup?) {
        val view: View = layoutInflater.inflate(R.layout.rz_image_fragment, container, false)
        private val iv: ImageView

        init {
            iv = view.findViewById(R.id.iv)
        }

        fun bind(url: String) {
            WUtil.loadFileImage(url, iv)
        }
    }
}