package gjhl.com.codefragment.view.anim.frame;

import androidx.appcompat.app.AppCompatActivity;
import gjhl.com.codefragment.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

// 帧动画
public class FrameAnimationActivity extends AppCompatActivity {

    private AnimationsContainer.FramesSequenceAnimation mAnimation;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, FrameAnimationActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_animation);
        mAnimation = new AnimationsContainer().setResId(R.array.voice_anim)
                .setDelayTime(400).setIsOne(true)
                .createProgressDialogAnim(findViewById(R.id.ivFrame));
        mAnimation.setOnAnimStopListener(mAnimation::start);
        mAnimation.start();

        findViewById(R.id.bStop).setOnClickListener(view->{
            mAnimation.setOnAnimStopListener(null);
        });
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mAnimation.setOnAnimStopListener(null);
    }
}
