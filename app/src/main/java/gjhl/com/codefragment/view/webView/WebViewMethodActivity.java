package gjhl.com.codefragment.view.webView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

// 建立assets 目 File-> New -> Folder -> Assets Folder
// webView 调用Android 方法
public class WebViewMethodActivity extends AppCompatActivity {
    public static void start(Activity activity, String url) {
        Intent intent = new Intent(activity, WebViewMethodActivity.class);
        intent.putExtra("url", url);
        activity.startActivity(intent);
    }

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view_activity);
        bindViews();
    }

    @JavascriptInterface
    public void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public static class Config {
        public String serverIP;
    }

    @JavascriptInterface
    public String getConfig() {
        return "1.1.1.1";
    }

    private void bindViews() {
        String url = getIntent().getStringExtra("url");
        webView = (WebView) findViewById(R.id.wv);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.addJavascriptInterface(this, "Android"); // 设置给 web 使用
        webView.loadUrl(url);
        //系统默认会通过手机浏览器打开网页，为了能够直接通过WebView显示网页，则必须设置
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //使用WebView加载显示url
                view.loadUrl(url);
                //返回true
                return true;
            }
        });
    }

}