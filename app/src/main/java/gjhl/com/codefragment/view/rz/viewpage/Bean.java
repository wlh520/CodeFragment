package gjhl.com.codefragment.view.rz.viewpage;

import java.io.File;

public class Bean {
    private File file;
    private boolean isPicture;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public boolean isPicture() {
        return isPicture;
    }

    public void setPicture(boolean picture) {
        isPicture = picture;
    }
}
