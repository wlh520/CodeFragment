package gjhl.com.codefragment.view.custom;

import androidx.appcompat.app.AppCompatActivity;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.view.BaseViewActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

// 自定义view 带布局的
public class CustomViewActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, CustomViewActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_view);
    }
}
