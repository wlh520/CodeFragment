package gjhl.com.codefragment.view.anim.wave;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import gjhl.com.codefragment.R;

// 自定义安卓波浪进度条展示 波浪动画
public class WaveActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, WaveActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wave);
    }
}
