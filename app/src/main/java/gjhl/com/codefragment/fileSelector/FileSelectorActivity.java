package gjhl.com.codefragment.fileSelector;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import gjhl.com.codefragment.R;

// 文件选择器 资源管理器 文件管理器
public class FileSelectorActivity extends AppCompatActivity {
    private static final String TAG = "FileSelectorActivity";
    private final String DES = Environment.getExternalStorageDirectory() + "/source.mp4";
    private final String SOURCE = Environment.getExternalStorageDirectory() + "/video.mp4";;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, FileSelectorActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.file_selector_activity);
    }

    public void cCopy(View view) {
        Log.i(TAG, "cCopy: " + SOURCE + " des:" + DES);
        FileOperation.copyNio(SOURCE, DES);
    }

    public void cCopy2(View view) {
        Log.i(TAG, "cCopy2: " + SOURCE + " des:" + DES);
        FileOperation.copy(SOURCE, DES);
    }

    public void cCopy3(View view) {
        Log.i(TAG, "cCopy3: " + SOURCE + " des:" + DES);
        try {
            FileOperation.copyNio2(new FileInputStream(new File(SOURCE)), DES);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void cCopy4(View view) {
        Log.i(TAG, "cCopy4: " + SOURCE + " des:" + DES);
        try {
            FileOperation.copy2(new FileInputStream(new File(SOURCE)), DES);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void cSelector(View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");//设置类型，我这里是任意类型，任意后缀的可以这样写。
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                if (uri != null) {
                    FileSelectorUtils.context = this;
                    String path = FileSelectorUtils.getPath(uri);
                    if (path != null) {
                        File file = new File(path);
                        if (file.exists()) {
                            String upLoadFilePath = file.toString();
                            String upLoadFileName = file.getName();
                        }
                    }
                    Log.i(TAG, "onActivityResult: " + path + " " + uri);
                }
            }
        }
    }
}
