package gjhl.com.codefragment.fileSelector;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

import gjhl.com.codefragment.config.FileUtils;

public class FileOperation {
    private static final String TAG = "FileOperation";
    // 复制文件
    public static void copyNio(String source, String dest) {
        FileChannel input = null;
        FileChannel output = null;

        try {
            input = new FileInputStream(new File(source)).getChannel();
            output = new FileOutputStream(new File(dest)).getChannel();
            output.transferFrom(input, 0, input.size());
        } catch (Exception e) {
            Log.e(TAG, "copyNio: ", e);
        } finally {
            try {
                input.close();
                output.close();
            } catch (Exception e) {

            }
        }
    }

    // 复制文件
    public static void copyNio2(FileInputStream source, String dest) {
        FileChannel input = null;
        FileChannel output = null;

        try {
            input = source.getChannel();
            output = new FileOutputStream(new File(dest)).getChannel();
            output.transferFrom(input, 0, input.size());
        } catch (Exception e) {
            Log.e(TAG, "copyNio: ", e);
        } finally {
            try {
                input.close();
                output.close();
            } catch (Exception e) {

            }
        }
    }

    // 复制文件
    public static void copy(String source, String dest) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(new File(source));
            out = new FileOutputStream(new File(dest));

            byte[] buffer = new byte[1024];
            int len;

            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
        } catch (Exception e) {
            Log.w(TAG + ":copy", "error occur while copy", e);
        } finally {
            try {
                in.close();
                out.close();
            } catch (Exception e) {

            }
        }
    }

    // 复制文件2
    public static void copy2(InputStream in, String dest) {
        FileUtils.createFile(dest);
        OutputStream out = null;
        try {
            out = new FileOutputStream(new File(dest));

            byte[] buffer = new byte[1024*8];
            int len;

            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
        } catch (Exception e) {
            Log.w(TAG + ":copy", "error occur while copy", e);
        } finally {
            try {
                in.close();
                out.close();
            } catch (Exception e) {

            }
        }
    }
}
