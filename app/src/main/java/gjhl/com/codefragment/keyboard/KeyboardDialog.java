package gjhl.com.codefragment.keyboard;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.DialogKeyboardBinding;

public class KeyboardDialog extends DialogFragment {
    private static final String TAG = "KeyboardDialog";
    public static void showFragment(FragmentActivity activity) {
        KeyboardDialog fragment = (KeyboardDialog) activity.getSupportFragmentManager().findFragmentByTag("KeyboardDialog");
        if (fragment != null) {
            activity.getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }

        fragment = new KeyboardDialog();
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        if (!activity.isFinishing()) {
            ft.add(fragment, "KeyboardDialog").commitAllowingStateLoss();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        DialogKeyboardBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_keyboard, container, false);
        binding.bCloseKeyboard.setOnClickListener(v->{
            Log.i(TAG, "onCreateView: ");
            cCloseSoft(v);
//            cCloseSoft(getActivity());
        });
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void cCloseSoft(View v) {
        v.requestFocus();
        InputMethodManager mInputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mInputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

    }

    // 在activity 中有效果，DialogFragment 中无效
    private void cCloseSoft(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusView = activity.getCurrentFocus();
        if (focusView != null) {
            imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
        }
    }
}
