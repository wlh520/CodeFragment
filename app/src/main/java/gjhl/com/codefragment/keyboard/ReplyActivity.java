package gjhl.com.codefragment.keyboard;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import gjhl.com.codefragment.R;

// 弹出软键盘 布局上移
public class ReplyActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ReplyActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow);
    }
}
