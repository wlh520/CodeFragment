package gjhl.com.codefragment.keyboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivityHideKeyboardBinding;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

// https://www.jianshu.com/p/57856a944bdc
// manifests 中设置 android:windowSoftInputMode="stateHidden" // 隐藏软键盘
public class HideKeyboardActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, HideKeyboardActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityHideKeyboardBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_hide_keyboard);
        binding.setActivity(this);

        KeyboardDialog.showFragment(this);
    }

    public void cHideKeyboard() {
        cCloseSoft(this);
    }

    public void cFollowKeyboard() {
        ReplyActivity.start(this);
    }

    public void cFollowKeyboardLogin() {
        LoginActivity.start(this);
    }

    // 在activity 中有效果，DialogFragment 中无效
    private void cCloseSoft(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusView = activity.getCurrentFocus();
        if (focusView != null) {
            imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
        }
    }
}
