package gjhl.com.codefragment.glide

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import gjhl.com.codefragment.R
import gjhl.com.codefragment.adBroadcast.AdBroadcastActivity
import gjhl.com.codefragment.databinding.AdBroadcastActivityBinding
import gjhl.com.codefragment.databinding.GlideActivityBinding

class GlideActivity : AppCompatActivity() {
    companion object {
        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, GlideActivity::class.java)
            activity.startActivity(intent)
        }
    }

    val binding: GlideActivityBinding by lazy {
        DataBindingUtil.setContentView(this,
                R.layout.glide_activity) as GlideActivityBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.click = this
        val requestOptions = RequestOptions().transform(RoundedCorners(10))
        Glide.with(this).load(R.drawable.maps).into(binding.ivOrigin)
        Glide.with(this).load(R.drawable.maps).into(binding.ivCenterCrop)
        // 圆弧
        Glide.with(this).load(R.drawable.map).apply(requestOptions).into(binding.ivFitXY)
        // bitmap
    }
}