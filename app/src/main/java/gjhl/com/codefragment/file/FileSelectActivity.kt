package gjhl.com.codefragment.file

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import gjhl.com.codefragment.R
import gjhl.com.codefragment.databinding.FileActivityBinding
import gjhl.com.codefragment.fileSelector.FileOperation


const val FileActivity_Code = 1

class FileActivity : AppCompatActivity() {

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, FileActivity::class.java)
            activity.startActivity(intent)
        }
    }

    private val TAG = "FileActivity"
    val binding: FileActivityBinding by lazy {
        DataBindingUtil.setContentView(
            this,
            R.layout.file_activity
        ) as FileActivityBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.click = this
    }

    // 打开系统的文件选择器
    fun cPickFile() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
//        intent.type = "application/*" 文件类型
        intent.type = "*/*" //设置类型
        val mimetypes = arrayOf("image/*", "video/*") // 只能选择图片和视频
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)
        startActivityForResult(intent, FileActivity_Code)
    }

    fun cImageFile(v: View) {
        ImageFileActivity.start(this)
    }
    /*
        private lateinit var registerForActivityResult: ActivityResultLauncher<Intent>

    onCreate 中
    val registerForActivityResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            // 扫描二维码/条码回传
            if (it.resultCode == RESULT_OK) {
                if (it.data != null) {
                    val mac = it.data!!.getStringExtra(Constant.CODED_CONTENT)
                }
            }
        }

    val intent = Intent(this@MainActivity, CaptureActivity::class.java)
    registerForActivityResult.launch(intent)

    // 3
    val intent = Intent()
        intent.putExtra("data_return", data)
    setResult(RESULT_OK)
     */


    // 获取文件的真实路径
    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data == null) {
            // 用户未选择任何文件，直接返回
            return
        }
        val path = data.data.toString() // 获取用户选择文件的URI
        Log.i(TAG, "onActivityResult: ${data.data?.path.toString()}")
        if (isFile(path)) {
            Log.i(TAG, "onActivityResult: isFile")
        }
        copy(data)
    }

    // usb 拷贝
    private fun copy(data: Intent) {
        val uri = data.data
        val input = applicationContext.contentResolver.openInputStream(uri!!)
        FileOperation.copy2(input, Environment.getExternalStorageDirectory().toString() + "/b.png")
    }

    private fun isFile(path: String): Boolean {
        val suffix = path.subSequence(path.length - 4, path.length)
        val fileSuffixes = arrayOf("docx", ".pdf", ".txt", ".doc")
        for (fileSuffix in fileSuffixes) {
            if (fileSuffix == suffix)
                return true
        }
        return false
    }
}