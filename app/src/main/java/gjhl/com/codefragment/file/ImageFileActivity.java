package gjhl.com.codefragment.file;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileInputStream;

import gjhl.com.codefragment.R;

public class ImageFileActivity extends AppCompatActivity {
    private static final String TAG = "ImageFileActivity";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ImageFileActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_file_activity);

        getDropboxIMGSize(new File(Environment.getExternalStorageDirectory()+"/茶几2/理财超市/微信图片_2.jpg"));
    }

    // 获取图片大小 （长宽）
    private void getDropboxIMGSize(File file){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        Log.i(TAG, "getDropboxIMGSize: imageHeight="+imageHeight+" imageWidth="+imageWidth);
    }
}
