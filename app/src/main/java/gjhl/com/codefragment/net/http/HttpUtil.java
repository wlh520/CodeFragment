package gjhl.com.codefragment.net.http;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.reactivex.Single;

public class HttpUtil {

    public static Single<String> loginUser() {
        ArrayList<Bean> beans = new ArrayList<>();
        Bean bean = new Bean();
        bean.visitUser = "王依依";
        bean.staffCode = "1";
        bean.createTime = "2022-08-09 17:13:52";
        bean.state = "1";
        bean.ip = "192.168.1.158";
        beans.add(bean);
        return Single.fromCallable(() -> {
            String http = WURLConnection.sendPost("api/receiveLog/createReceiveLog", new Gson().toJson(beans));
            return http;
        });
    }

    public static class Bean {
        public String visitUser;
        public String staffCode;
        public String createTime;
        public String state;
        public String ip;
    }
}
