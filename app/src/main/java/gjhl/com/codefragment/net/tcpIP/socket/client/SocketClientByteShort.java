package gjhl.com.codefragment.net.tcpIP.socket.client;

import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
/*
val s = SocketClientByteShort("192.168.0.171", 2000)
val byte = arrayListOf<Byte>(0x55.toByte(),0xAA.toByte(),0x03.toByte(),0x01.toByte(),
    0x01.toByte(),0x01.toByte(),0x06.toByte())
s.setSocketData(byte.toByteArray())
s.start()
 */
public class SocketClientByteShort extends Thread {
    private static final String TAG = "SocketClientByteShort";
    private DataOutputStream bufferedWriter;
    private final String ip;
    private final int port;
    private Socket socket;
    private byte[] sendData;

    public SocketClientByteShort(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void setSocketData(byte[] sendData) {
        this.sendData = sendData;
    }

    @Override
    public void run() {
        super.run();
        connect();
    }

    public void send() {
        if (bufferedWriter == null) {
            start();
            return;
        }
        write(sendData);
    }

    private void connect() {
        try {
            socket = new Socket(ip, port);
            bufferedWriter = new DataOutputStream(socket.getOutputStream());

            write(sendData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            if (socket != null)
                socket.close();
            if (bufferedWriter != null)
                bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(byte[] bytes) {
        Log.i(TAG, "write: " + bytes);
//        WUtil.writeLogFile(s);

        try {
            if (bufferedWriter == null) {
             /*   connect();
                Log.i(TAG, "write: 服务器掉线了");*/
                return;
            }
            SocketUtil.bwByte(bufferedWriter, bytes);
            sleep(1000);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
