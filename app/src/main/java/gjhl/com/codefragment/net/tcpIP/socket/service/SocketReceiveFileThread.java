package gjhl.com.codefragment.net.tcpIP.socket.service;

import android.os.Environment;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class SocketReceiveFileThread extends Thread {
    private Socket socket;
    private static final String TAG = "SocketThread";

    public SocketReceiveFileThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            InputStream is = socket.getInputStream();
            FileOutputStream fos = new FileOutputStream( Environment.getExternalStorageDirectory() + "/a.png");
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                fos.write(buffer, 0, bytesRead);
            }
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}