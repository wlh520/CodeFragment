package gjhl.com.codefragment.net.tcpIP.socket.service;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class SocketServiceActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SocketServiceActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.socket_service_activity);
        startService(new Intent(this, SocketServiceService.class));
    }
}
