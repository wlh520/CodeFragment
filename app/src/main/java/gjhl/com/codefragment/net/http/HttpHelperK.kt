package gjhl.com.codefragment.net.http

import com.google.gson.Gson
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object HttpHelperK {
    fun updateCabinetVersion(version: String, back: (String)->Unit) {
        Single.fromCallable {
            var params = "${getCommonParams()}&version=${version}"
            WURLConnection.get("api/cabinet/updateCabinetVersion?$params")
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe { s: String ->
                back(s)
            }
    }

    fun findByRfId(list: ArrayList<String>, back: (User) -> Unit) {
        val bean = User("", "")
        Single.fromCallable {
            WURLConnection.sendPost("api/drawer/findByRfId", Gson().toJson(bean))
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe { s: String ->
            }
    }
    private fun getCommonParams(): String {
//        return "ip=${WUtil.getIp()}&deptCode=${SPUtil.get(SPUtil.NET_CODE)}"
        return "ip"
    }
}