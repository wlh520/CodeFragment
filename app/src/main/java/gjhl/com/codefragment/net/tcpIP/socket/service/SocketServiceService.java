package gjhl.com.codefragment.net.tcpIP.socket.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServiceService extends Service {
    private static final String TAG = "SocketServiceService";
    private final static int GRAY_SERVICE_ID = 1001;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //初始化websocket
        initSocketService();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (server != null) {
            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public SocketServiceService() {
    }

    private ServerSocket server;

    Runnable connRunnable = new Runnable() {
        public void run() {
            try {
                server = new ServerSocket(8888);

                while (true) {
                    // 没 client 连进来时，会卡在这行等待连接进入
                    Socket socket = server.accept();
                    SocketReceiveFileThread socketReceiveThread = new SocketReceiveFileThread(socket);
                    Log.i(TAG, "run: ");
                    socketReceiveThread.start();
                }
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    };

    /**
     */
    private void initSocketService() {
        new Thread(connRunnable).start();
    }
}
