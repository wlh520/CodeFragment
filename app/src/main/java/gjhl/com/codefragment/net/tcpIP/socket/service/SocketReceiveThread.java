package gjhl.com.codefragment.net.tcpIP.socket.service;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class SocketReceiveThread extends Thread {
    private Socket socket;
    private static final String TAG = "SocketThread";

    public SocketReceiveThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            byte[] bytes = new byte[1024];
            int len;
            StringBuilder sb = new StringBuilder();
            Log.i(TAG, "start");
            // 接收
            while ((len = inputStream.read(bytes)) != -1) {// 连接完，断开的时候为-1
                sb.append(new String(bytes, 0, len, "gbk"));
                Log.i(TAG, "run: " + sb.toString());
                sb = new StringBuilder();
            }
            Log.i(TAG, "close");
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}