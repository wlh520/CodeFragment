package gjhl.com.codefragment.net.tcpIP.socket.client.b;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class SocketReceiveThreadByte extends Thread {
    private static final String TAG = "SocketReceiveThread";
    private Socket socket;
    private InputStream inputStream;
    private SocketDataBackByte socketDataBack;

    public SocketReceiveThreadByte(Socket socket, InputStream inputStream, SocketDataBackByte socketDataBack) {
        this.socket = socket;
        this.inputStream = inputStream;
        this.socketDataBack = socketDataBack;
    }

    public void run() {
        try {
            while (true) {
                if (socket.isClosed() || inputStream == null)
                    return;
                String[] str = getInputStringHEX(inputStream);
                Log.d(TAG, "run: " + new Gson().toJson(str));
                socketDataBack.back(str);
            }
        } catch (Exception e) {
            Log.e(TAG, "run: ", e);
        }
    }

    public static String[] getInputStringHEX(InputStream inputStream) throws IOException {
        byte[] b = new byte[1024];
        int r = inputStream.read(b);
        byte[] realB = new byte[r];
        for (int i = 0; i < r; i++) {
            realB[i] = b[i];
        }
        return bytes2hex(realB);
    }


    // 用16进制的方式打印byte数组
    public static String[] bytes2hex(byte[] bytes) {
        String[] hex = new String[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            hex[i] = Integer.toHexString(0xFF & bytes[i]);
            if (hex[i].length() == 1)
                hex[i] = "0" + hex[i];
        }
        return hex;
    }
}
