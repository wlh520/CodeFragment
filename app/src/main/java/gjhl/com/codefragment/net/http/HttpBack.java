package gjhl.com.codefragment.net.http;

public interface HttpBack {
    void success(String s) throws Exception;
    void fail(String s) throws Exception;
}
