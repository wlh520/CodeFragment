package gjhl.com.codefragment.net.tcpIP.socket.client;

import android.util.Log;

import java.io.InputStream;
import java.net.Socket;

public class SocketReceiveThread extends Thread {
    private static final String TAG = "SocketReceiveThread";
    private Socket socket;
    private InputStream inputStream;

    public SocketReceiveThread(Socket socket, InputStream inputStream) {
        this.socket = socket;
        this.inputStream = inputStream;
    }

    public void run() {
        try {
            while (true) {
                if (isServerClose(socket))
                    return;
//                String str = SocketUtil.getInputStringHEX(inputStream);
                String str = SocketUtil.getInputString(inputStream);
                Log.i(TAG, Thread.currentThread().getName() + " run: " + str);
            }
        } catch (Exception e) {
            Log.e(TAG, "run: ", e);
        }
    }

    /**
     * 判断是否断开连接，断开返回true,没有返回false
     * @param socket
     * @return
     */
    public boolean isServerClose(Socket socket){
        try{
            socket.sendUrgentData(0);//发送1个字节的紧急数据，默认情况下，服务器端没有开启紧急数据处理，不影响正常通信
            return false;
        }catch(Exception se){
            return true;
        }
    }
}
