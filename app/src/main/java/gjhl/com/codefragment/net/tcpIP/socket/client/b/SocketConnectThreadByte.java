package gjhl.com.codefragment.net.tcpIP.socket.client.b;

import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.net.tcpIP.socket.client.SocketUtil;

/*
sensorConnectThread.setSocketDataBack(bytes) {
}
sensorConnectThread.send()
 */
public class SocketConnectThreadByte extends Thread {
    private BufferedWriter bufferedWriter;
    private DataOutputStream dOut;
    private final String ip;
    private final int port;
    private SocketReceiveThreadByte receiveThread;
    private Socket socket;
    private InputStream inputStream;
    private SocketDataBackByte socketDataBack;
    private byte[] sendData;

    public SocketConnectThreadByte(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void setSocketDataBack(byte[] sendData, SocketDataBackByte socketDataBack) {
        this.socketDataBack = socketDataBack;
        this.sendData = sendData;
    }

    @Override
    public void run() {
        super.run();
        connect();
    }

    public void send() {
        if (receiveThread == null) {
            start();
            return;
        }
        writeByte(sendData);
    }

    private void connect() {
        try {
            socket = new Socket(ip, port);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            dOut = new DataOutputStream(socket.getOutputStream());
            inputStream = socket.getInputStream();

            receiveThread = new SocketReceiveThreadByte(socket, inputStream, socketDataBack);
            receiveThread.start();
            sleep(100);
            writeByte(sendData);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            socket.close();
            bufferedWriter.close();
            inputStream.close();
            dOut.close();
            receiveThread=null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeByte(byte[] bytes) {
        if (dOut == null) {
            connect();
            Toast.makeText(MyApplication.getContext(), "服务器掉线了", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            SocketUtil.bwByte(dOut, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
