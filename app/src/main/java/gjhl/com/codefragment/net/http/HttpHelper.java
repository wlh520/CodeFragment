package gjhl.com.codefragment.net.http;

import org.json.JSONObject;

public class HttpHelper {
    private static final String TAG = "HttpHelper";
    private static final String DOMAIN="http://46643x21m0.goho.co:46545";
//    private static final String DOMAIN="http://192.168.1.146:8000";

    public static void login(String account, String password, HttpBack back) {
        String url = DOMAIN + "/auth/login1";
        new Thread() {
            @Override
            public void run() {
                String s = WURLConnection.sendPostParam(url, "password=" + password + "&username=" + account);
                dealResponse(s, back);
            }
        }.start();
    }

    private static void dealResponse(String s, HttpBack back) {
        try {
            JSONObject json = new JSONObject(s);
            int status = json.optInt("status");
            if (status == 200) back.success(s);
            else {
                String message = json.optString("message");
//                ToastUtils.toast(MyApplication.getContext(), message);
                back.fail(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void cabinet(HttpBack back) {
//        String url = DOMAIN + "/api/cabinet/findByCabinet?userId="+MyApplication.userBean.id;
        String url = DOMAIN + "/api/cabinet/findByCabinet?userId=";
        new Thread() {
            @Override
            public void run() {
                super.run();
                String s = WURLConnection.get(url);
                dealResponse(s,back);
            }
        }.start();
    }
}
