package gjhl.com.codefragment.net.http;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.util.WUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HttpActivity extends AppCompatActivity {
    private static final String TAG = "HttpActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, HttpActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.http_activity);
        http();

        TextView tvIp = findViewById(R.id.tvIp);
        tvIp.setText(WUtil.getIp(getApplication()));
        EditText etPingIp = findViewById(R.id.etPingIp);
        Button bPingIp = findViewById(R.id.bPingIp);
        bPingIp.setOnClickListener(v -> {
            String ip = etPingIp.getText().toString();
            Log.i(TAG, "onCreate: " + WUtil.ping(ip));

        });
    }

    private void http() {
        findViewById(R.id.bHttp).setOnClickListener(v -> {

            HttpUtil.loginUser().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(it -> {
                        Log.i(TAG, "onCreate: " + it);
                    });
        });
    }
}
