package gjhl.com.codefragment.net.tcpIP.socket.client;

import android.util.Log;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.util.WUtil;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class SocketUtil {
    private static final String TAG = "SocketUtil";
    public static String getInputString(InputStream inputStream) throws IOException {
        byte[] b = new byte[1024];
        // 会卡这， inputStream.read 等待内容输入
        int r = inputStream.read(b);
        String str="";
        if (r > -1) {
            str = new String(b,0,r);

        }
        return str;
    }

    public static String getInputStringHEX(InputStream inputStream) throws IOException {
        byte[] b = new byte[1024];
        int r = inputStream.read(b);
        byte[] realB = new byte[r];
        for (int i = 0; i < r; i++) {
            realB[i] = b[i];
        }
        return WUtil.bytes2hex(realB);
    }


    public static void bwString(BufferedWriter bw, String s) throws IOException {
        Single.fromCallable(() -> {
            Log.i(TAG, "bwString: ");
            try {
                bw.write(s);
                bw.flush();
            } catch (Exception e) {
                Toast.makeText(MyApplication.getContext(), "连接已断开", Toast.LENGTH_SHORT).show();
            }

            return s;
        }).subscribeOn(Schedulers.io()).subscribe();
    }

    public static void bwByte(DataOutputStream dataOutputStream, byte[] bytes) throws IOException {
        Single.fromCallable(() -> {
            Log.i(TAG, "bwByte: ");
            try {
                dataOutputStream.write(bytes);
                dataOutputStream.flush();
            } catch (Exception e) {
                Toast.makeText(MyApplication.getContext(), "连接已断开", Toast.LENGTH_SHORT).show();
            }

            return "";
        }).subscribeOn(Schedulers.io()).subscribe();
    }
}
