package gjhl.com.codefragment.net.tcpIP;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.net.tcpIP.socket.SocketActivity;
import gjhl.com.codefragment.net.tcpIP.udp.ReceiverUdpActivity;

public class TCPipActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, TCPipActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tcp_ip_activity);
    }

    public void cSocket(View view) {
        SocketActivity.start(this);
    }

    public void cUdp(View view) {
        ReceiverUdpActivity.start(this);
    }
}
