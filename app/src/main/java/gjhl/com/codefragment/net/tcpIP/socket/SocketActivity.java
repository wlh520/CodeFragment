package gjhl.com.codefragment.net.tcpIP.socket;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.net.tcpIP.socket.client.SocketClientActivity;
import gjhl.com.codefragment.net.tcpIP.socket.service.SocketServiceActivity;

public class SocketActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SocketActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.socket_activity);
    }

    public void cSocketClient(View view) {
        SocketClientActivity.start(this);
    }

    public void cSocketService(View view) {
        SocketServiceActivity.start(this);
    }

}
