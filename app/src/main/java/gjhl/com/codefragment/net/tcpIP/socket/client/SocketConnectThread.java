package gjhl.com.codefragment.net.tcpIP.socket.client;

import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

import gjhl.com.codefragment.MyApplication;

// SocketReceiveThread 要是做掉线重连，要用单例子
public class SocketConnectThread extends Thread {
    private BufferedWriter bufferedWriter;
    private DataOutputStream dOut;
    private final String ip;
    private final int port;
    private SocketReceiveThread receiveThread;
    private Socket socket;
    private InputStream inputStream;

    public SocketConnectThread(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    @Override
    public void run() {
        super.run();
        connect();
    }

    private void connect() {
        try {
            socket = new Socket(ip, port);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            dOut = new DataOutputStream(socket.getOutputStream());
            inputStream = socket.getInputStream();

            receiveThread = new SocketReceiveThread(socket, inputStream);
            receiveThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            socket.close();
            bufferedWriter.close();
            inputStream.close();
            dOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(String s) {
        try {
            if (bufferedWriter == null) {
                connect();
                Toast.makeText(MyApplication.getContext(), "服务器掉线了", Toast.LENGTH_SHORT).show();
                return;
            }
            SocketUtil.bwString(bufferedWriter, s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeByte(byte[] bytes) {
        try {
            if (dOut == null) {
                connect();
                Toast.makeText(MyApplication.getContext(), "服务器掉线了", Toast.LENGTH_SHORT).show();
                return;
            }
            SocketUtil.bwByte(dOut, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
