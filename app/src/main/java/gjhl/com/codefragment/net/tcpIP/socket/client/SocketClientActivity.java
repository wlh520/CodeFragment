package gjhl.com.codefragment.net.tcpIP.socket.client;

import static gjhl.com.codefragment.util.WUtil.findAllThreads;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

/**
 * ping 不通自己电脑
 * 方法1：关闭防火墙
 * https://blog.csdn.net/wudinaniya/article/details/80956158
 * 1.“Windows Defender 防火墙”中的“高级设置”位置：
 * 2 点击“入站规则”，在右侧“入站规则”找到“文件和打印机共享（回显请求）”注意是ICMPv4。然后启用规则
 * 之后重启电脑
 * 连接socket server
 * telnet 127.0.0.1 7000
 * ctrl + ]
 * send 100
 * https://blog.csdn.net/Brave_heart4pzj/article/details/107434944
 * 安装 telnet 搜索 启动或关闭 Windows 功能
 * https://jingyan.baidu.com/article/3ea51489ba79e252e61bba97.html
 *
 * 0 段 不能给 9 段 发送消息
 */
public class SocketClientActivity extends AppCompatActivity {
    private static final String TAG = "SocketClientActivity";
    private SocketConnectThread socketConnectThread;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SocketClientActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.socket_client_activity);
    }

    public void cSend(View view) {
        byte[] bytes = {(byte) 0xBB, (byte) 0x00, (byte) 0x27, (byte) 0x00, (byte) 0x03, (byte) 0x22, (byte) 0xFF, (byte) 0xFF, (byte) 0x4A, (byte) 0x7E};
        socketConnectThread.writeByte(bytes);

    }

    public void cSendString(View view) {
        socketConnectThread.write("123456");
    }

    public void cClose(View view) {
        socketConnectThread.close();
    }

    public void cConnect(View view) {
        EditText etIp = findViewById(R.id.etIp);
        EditText etPort = findViewById(R.id.etPort);
        String ip = etIp.getText().toString();
        int port = Integer.parseInt(etPort.getText().toString());
        socketConnectThread = new SocketConnectThread(ip, port);
        socketConnectThread.start();
    }

    public void cThread(View view) {
        Thread[] threads = findAllThreads();
        for (int i = 0; i < threads.length; i++) {
            Thread thread = threads[i];
            Log.i(TAG, "cThread: " + thread.getName() + " " + thread.isAlive());
        }
    }
}
