package gjhl.com.codefragment.net.tcpIP.socket.client;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
/**
 val s = SocketClientShort(ip[0], 2000)
 s.setSocketData("${SPUtil.get(SPUtil.NET_CODE)}_${WUtil.getIp()}")
 s.start()
 */
public class SocketClientShort extends Thread {
    private static final String TAG = "SocketConnectThread";
    private BufferedWriter bufferedWriter;
    private final String ip;
    private final int port;
    private Socket socket;
    private String sendData;

    public SocketClientShort(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void setSocketData(String sendData) {
        this.sendData = sendData;
    }

    @Override
    public void run() {
        super.run();
        connect();
    }

    public void send() {
        if (bufferedWriter == null) {
            start();
            return;
        }
        write(sendData);
    }

    private void connect() {
        try {
            socket = new Socket(ip, port);
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            write(sendData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            if (socket != null)
                socket.close();
            if (bufferedWriter != null)
                bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(String s) {
        Log.i(TAG, "write: " + s);
//        WUtil.writeLogFile(s);

        try {
            if (bufferedWriter == null) {
             /*   connect();
                Log.i(TAG, "write: 服务器掉线了");*/
                return;
            }
            SocketUtil.bwString(bufferedWriter, s);
            sleep(1000);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
