package gjhl.com.codefragment.net.tcpIP.udp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import gjhl.com.codefragment.R;

// 端口地址要改
public class ReceiverUdpActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ReceiverUdpActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.receiver_upd_activity);
        new Thread(new Receiver()).start();
    }

    class Receiver implements Runnable {
        DatagramSocket ds;
        DatagramPacket dp;

        @Override
        public void run() {
            try {
                ds = new DatagramSocket(8001);
                dp = new DatagramPacket(new byte[1024], 1024);
                while (true) {
                    ds.receive(dp);
                    System.out.println(dp.getAddress());
                    System.out.println(new String(dp.getData(), 0, dp.getLength()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
