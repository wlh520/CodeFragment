package gjhl.com.codefragment.video.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.video.VideoBaseActivity;
import gjhl.com.codefragment.video.multi.FullNoProgressVideoFragment;

public class VideoFragmentActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, VideoFragmentActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_fragment_activity);
        loop();
    }

    private void loop() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fl, FullNoProgressVideoFragment.Companion.newInstance(VideoBaseActivity.getVideoUrl(), ()->{
            loop();
            return null;
        })).commitAllowingStateLoss();
    }

    public void cEmptyActivity(View view) {
        EmptyActivity.start(this);
    }
}
