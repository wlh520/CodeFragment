package gjhl.com.codefragment.video.multi.main;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import android.widget.VideoView;

public class WLHVideoView extends VideoView {
    public WLHVideoView(Context context) {
        super(context);
        init();
    }

    public WLHVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WLHVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return true;
    }
}
