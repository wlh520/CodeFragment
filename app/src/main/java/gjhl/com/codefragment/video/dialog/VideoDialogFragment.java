package gjhl.com.codefragment.video.dialog;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.video.fragment.VideoFragment;

public class VideoDialogFragment extends DialogFragment {

    private View rootView;
    private VideoFragment multiVideoFragment;

    public static void showFragment(FragmentActivity activity) {
        VideoDialogFragment fragment = (VideoDialogFragment) activity.getSupportFragmentManager().findFragmentByTag("VideoDialogFragment");
        if (fragment != null) {
            activity.getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }

        fragment = new VideoDialogFragment();
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        if (!activity.isFinishing()) {
            ft.setCustomAnimations(
                    R.anim.dialog_from_bottom_anim_in,
                    R.anim.dialog_from_bottom_anim_out
            ).add(fragment, "VideoDialogFragment").commitAllowingStateLoss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() == null) {
            return;
        }
//        getDialog().setCanceledOnTouchOutside(false);

        Window window = getDialog().getWindow();
        if (window == null) {
            return;
        }
        View decorView = window.getDecorView();
        decorView.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getDialog() != null) {
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        }

        rootView = inflater.inflate(R.layout.video_dialog_fragment, container, false);

        multiVideoFragment = VideoFragment.newInstance();
        getChildFragmentManager().beginTransaction().replace(R.id.flVideo, multiVideoFragment)
                .commitAllowingStateLoss();

        return rootView;
    }
}
