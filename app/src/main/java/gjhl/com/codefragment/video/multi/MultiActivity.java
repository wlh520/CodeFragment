package gjhl.com.codefragment.video.multi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.video.VideoBaseActivity;

public class MultiActivity extends AppCompatActivity {
    private static final String TAG = "MultiActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, MultiActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi);
        startVideo(findViewById(R.id.videoView));
        VideoView videoView2 = findViewById(R.id.videoView2);
        startVideo(videoView2);

        //对于全屏拉伸操作，主要就是如下代码
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        videoView2.setLayoutParams(layoutParams);

        getSupportFragmentManager().beginTransaction().replace(R.id.fl, MultiVideoFragment.Companion.newInstance(VideoBaseActivity.getVideoUrl(), () -> {
            Log.i(TAG, "onCreate: complete");
            return null;
        })).commitAllowingStateLoss();
        getSupportFragmentManager().beginTransaction().replace(R.id.fl1, MultiVideoFragment.Companion.newInstance(VideoBaseActivity.getVideoUrl(), () -> {
            Log.i(TAG, "onCreate: complete");
            return null;
        }), "DefExam").commitAllowingStateLoss();
    }

    private void startVideo(VideoView videoView) {
        //加载指定的视频文件
        videoView.setVideoPath(VideoBaseActivity.getVideoUrl());
        //创建MediaController对象
        MediaController mediaController = new MediaController(this);
        //VideoView与MediaController建立关联
        videoView.setMediaController(mediaController);
        //让VideoView获取焦点
        videoView.requestFocus();
        videoView.start();
        videoView.setOnCompletionListener(mp -> {
            Log.i(TAG, "startVideo: 播放结束");
        });
    }
}
