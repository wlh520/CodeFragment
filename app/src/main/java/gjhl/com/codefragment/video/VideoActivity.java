package gjhl.com.codefragment.video;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class VideoActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, VideoActivity.class);
        activity.startActivity(intent);
    }

    private static final String TAG = "VideoActivity";

    // 视频声音
    private float volume = 0.1f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_activity);
        video();
    }

    private void video() {
        VideoView videoView = (VideoView)findViewById(R.id.videoView);

        //加载指定的视频文件
        videoView.setVideoPath(getVideoUrl());

        //创建MediaController对象
        MediaController mediaController = new MediaController(this);

        //VideoView与MediaController建立关联
        videoView.setMediaController(mediaController);

        //让VideoView获取焦点
        videoView.requestFocus();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setVolume(volume, volume);
            }
        });
        videoView.start();
    }

    public void cAddVolume(View view) {
        volume+=0.1;
        if (volume>1)
            volume = 1;
        video();
    }

    public void cDecrease(View view) {
        volume-=0.1;
        if (volume<0)
            volume = 0;
        video();
    }

    private String getVideoUrl() {
        String path = Environment.getExternalStorageDirectory().toString() + "/a/宣传视频/30秒动画 惠支付.mp4";
        Log.i(TAG, "getVideoUrl: " + path);
        return path;
    }

    // rawId
    //         videoView.setVideoURI(getVideoUri());
   /* private Uri getVideoUri() {
       return Uri.parse("android.resource://"+getPackageName()+"/"+resId);
    }*/
}
