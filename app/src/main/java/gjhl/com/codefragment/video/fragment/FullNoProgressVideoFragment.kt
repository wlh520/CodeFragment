package gjhl.com.codefragment.video.multi

import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import android.widget.RelativeLayout
import android.widget.VideoView
import androidx.fragment.app.Fragment
import gjhl.com.codefragment.R

class FullNoProgressVideoFragment : Fragment() {
    private val TAG = "MultiVideoFragment"
    companion object {
        // 传入网络地址或者本地地址
        fun newInstance(path: String, completeBack : ()->Unit):FullNoProgressVideoFragment {
            return FullNoProgressVideoFragment().apply {
                this.completeBack = completeBack
                arguments = Bundle().apply {
                    putString("path", path)
                }
            }
        }
    }

    var seekTime = 0
    var completeBack : ()->Unit = {}
    val path by lazy { requireArguments().getString("path") }
    lateinit var videoView: VideoView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.multi_video_fragment, container, false)

        videoView = view.findViewById<VideoView>(R.id.videoView)
        startVideo(videoView)
        // Inflate the layout for this fragment
        return view
    }

    private fun startVideo(videoView: VideoView) {
        //对于全屏拉伸操作，主要就是如下代码
        val layoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.FILL_PARENT,
            RelativeLayout.LayoutParams.FILL_PARENT
        )
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
        videoView.setLayoutParams(layoutParams)

        //加载指定的视频文件
        videoView.setVideoPath(path)

        //让VideoView获取焦点
        videoView.requestFocus()
        videoView.start()
        Log.i(TAG, "startVideo: " + path)
        videoView.setOnCompletionListener { mp: MediaPlayer? ->
            completeBack()
        }
    }

    override fun onResume() {
        super.onResume()
        videoView.start()
        videoView.seekTo(seekTime)
    }

    override fun onPause() {
        super.onPause()
        videoView.pause()
        seekTime = videoView.currentPosition
    }
}