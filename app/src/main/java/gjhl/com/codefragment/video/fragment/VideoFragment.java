package gjhl.com.codefragment.video.fragment;

import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import gjhl.com.codefragment.R;

public class VideoFragment extends Fragment {

    private View rootView;

    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.video_fragment, container, false);
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        VideoView videoView = (VideoView)rootView.findViewById(R.id.videoView);

        //加载指定的视频文件
        videoView.setVideoPath(getVideoUrl());

        //创建MediaController对象
        MediaController mediaController = new MediaController(getContext());

        //VideoView与MediaController建立关联
        videoView.setMediaController(mediaController);

        //让VideoView获取焦点
        videoView.requestFocus();
        videoView.start();
        videoView.setZOrderOnTop(true);
        // 循环播放
        videoView.setOnCompletionListener(mp -> videoView.start());
    }
    private String getVideoUrl() {
        return Environment.getExternalStorageDirectory().toString() + "/鱼缸/network/湖南农信宣传片6.mp4";
    }
}
