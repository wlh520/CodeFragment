package gjhl.com.codefragment.video;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.video.dialog.VideoDialogFragment;
import gjhl.com.codefragment.video.fragment.VideoFragmentActivity;
import gjhl.com.codefragment.video.multi.MultiActivity;

// https://blog.csdn.net/liuzhi0724/article/details/81318816
public class VideoBaseActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, VideoBaseActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_base_activity);
    }

    public void cActivity(View view) {
        VideoActivity.start(this);
    }

    public void cVideoLoop(View view) {
        VideoLoopActivity.start(this);
    }

    public void cFragment(View view) {
        VideoFragmentActivity.start(this);
    }

    public void cDialog(View view) {
        VideoDialogFragment.showFragment(this);
    }

    public void cMultiActivity(View view) {
        MultiActivity.start(this);
    }

    public static String getVideoUrl() {
        return Environment.getExternalStorageDirectory().toString() + "/视频/home_video.mp4";
    }
}
