package gjhl.com.codefragment.activityManage;

import android.os.Handler;

import androidx.fragment.app.FragmentActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import gjhl.com.codefragment.MainActivity;

// 获取当前的activity
public class ActivityManagerWTop {
    private static ActivityManagerWTop manager = new ActivityManagerWTop();
    private WeakReference<FragmentActivity> current;

    private ActivityManagerWTop() {

    }

    public void setCurrentActivity(FragmentActivity obj) {
        current = new WeakReference<FragmentActivity>(obj);
    }

    public static ActivityManagerWTop getManager() {
        return manager;
    }

    public FragmentActivity getTopActivity() {
        if (current != null)
            return current.get();
        return null;
    }
}