package gjhl.com.codefragment.activityManage;

import android.os.Handler;

import androidx.fragment.app.FragmentActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import gjhl.com.codefragment.MainActivity;

// 管理activity
public class ActivityManagerW {
    private static ActivityManagerW manager = new ActivityManagerW();
    private WeakReference<FragmentActivity> current;
    private Handler handler = new Handler();
    private List<WeakReference<FragmentActivity>> weakReferenceList = new ArrayList<>();

    private ActivityManagerW() {

    }

    public static ActivityManagerW getManager() {
        return manager;
    }

    public List<FragmentActivity> getList() {
        ArrayList<FragmentActivity> list = new ArrayList<>();
        for (WeakReference<FragmentActivity> weakReference : weakReferenceList) {
            if (weakReference.get() != null) {
                list.add(weakReference.get());
            }
        }
        return list;
    }

    public FragmentActivity getTopActivity() {
        if (current != null)
            return current.get();
        return null;
    }

    public void setCurrentActivity(FragmentActivity obj) {
        current = new WeakReference<FragmentActivity>(obj);
    }

    public void addActivity(FragmentActivity obj) {
        weakReferenceList.add(new WeakReference<FragmentActivity>(obj));
    }

    public void removeActivity(FragmentActivity obj) {
        for (WeakReference<FragmentActivity> weakReference : weakReferenceList) {
            if (weakReference.get() == obj) {
                weakReferenceList.remove(weakReference);
                break;
            }

        }
    }

    public MainActivity getMainActivity() {
        for (WeakReference<FragmentActivity> weakReference : weakReferenceList) {
            if (weakReference.get() instanceof MainActivity) {
                return (MainActivity) weakReference.get();
            }
        }
        return null;
    }

    public void deleteToMain() {
        if (!(manager.getTopActivity() instanceof MainActivity)) {
            manager.getTopActivity().finish();
            handler.postDelayed(this::deleteToMain, 100);
        }
    }
}