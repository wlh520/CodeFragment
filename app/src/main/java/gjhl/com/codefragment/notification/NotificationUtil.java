package gjhl.com.codefragment.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import androidx.core.app.NotificationCompat;

/**
 * Created by Administrator on 2018/11/1 0001.
 */

public class NotificationUtil {
    final static String CHANNEL_ID = "channel_id_1";

    public static NotificationManager getNotificationManager(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            //只在Android O之上需要渠道
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,
                    "channel_name_1", NotificationManager.IMPORTANCE_HIGH);
            //如果这里用IMPORTANCE_NOENE就需要在系统的设置里面开启渠道，
            //通知才能正常弹出
            notificationManager.createNotificationChannel(notificationChannel);
        }

        return notificationManager;
    }

    public static NotificationCompat.Builder getNotificationCompat(Context context) {
        return new NotificationCompat.Builder(context, CHANNEL_ID);
    }
}
