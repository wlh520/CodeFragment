package gjhl.com.codefragment.notification

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import gjhl.com.codefragment.R
import gjhl.com.codefragment.util.WUtil
import gjhl.com.codefragment.databinding.BaseNotificationActivityBinding

class CheckNotificationActivity : AppCompatActivity() {
    companion object {
        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, CheckNotificationActivity::class.java)
            activity.startActivity(intent)
        }
    }

    val binding: BaseNotificationActivityBinding by lazy {
        DataBindingUtil.setContentView(this,
                R.layout.base_notification_activity) as BaseNotificationActivityBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.empty_activity)
        WUtil.checkNotification(this)
    }
}