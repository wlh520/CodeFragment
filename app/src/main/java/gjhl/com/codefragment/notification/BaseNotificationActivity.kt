package gjhl.com.codefragment.notification

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import gjhl.com.codefragment.R
import gjhl.com.codefragment.databinding.BaseNotificationActivityBinding
import gjhl.com.codefragment.databinding.GlideActivityBinding
import gjhl.com.codefragment.glide.GlideActivity

class BaseNotificationActivity : AppCompatActivity() {
    companion object {
        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, BaseNotificationActivity::class.java)
            activity.startActivity(intent)
        }
    }

    val binding: BaseNotificationActivityBinding by lazy {
        DataBindingUtil.setContentView(this,
                R.layout.base_notification_activity) as BaseNotificationActivityBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.click = this
    }

    fun cNotification() {
        NotificationActivity.start(this)
    }

    fun cCheck() {
        CheckNotificationActivity.start(this)
    }
}