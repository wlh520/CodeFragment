package gjhl.com.codefragment.notification;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import gjhl.com.codefragment.R;

public class NotificationActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, NotificationActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        final NotificationManager notificationManager = NotificationUtil.getNotificationManager(getApplicationContext());

        final NotificationCompat.Builder mBuilder = NotificationUtil.getNotificationCompat(getApplicationContext());
        mBuilder.setContentTitle("title").setContentText("description").setTicker("您有未读消息")
                // 通知首次出现在通知栏，带上升动画效果的
                .setWhen(System.currentTimeMillis())
                // 通知产生的时间，会在通知信息里显示
                .setPriority(Notification.PRIORITY_DEFAULT)
                // 设置该通知优先级
                .setAutoCancel(true)
                // 设置这个标志当用户单击面板就可以让通知将自动取消
                .setOngoing(false)
                // ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,
                // 主动网络连接)
                .setDefaults(Notification.DEFAULT_ALL)
                // 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：
                .setSmallIcon(R.mipmap.ic_launcher);
        findViewById(R.id.button).setOnClickListener(v -> notificationManager.notify(1, mBuilder.build()));
    }
}
