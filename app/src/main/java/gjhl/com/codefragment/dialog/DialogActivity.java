package gjhl.com.codefragment.dialog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.dialog.alert.AlertActivity;
import gjhl.com.codefragment.dialog.dialogFragment.Dialog;
import gjhl.com.codefragment.dialog.picker.DatePickerFragment;
import gjhl.com.codefragment.dialog.picker.TimePickerFragment;

public class DialogActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, DialogActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_activity);

    }

    public void cTimePickerDialog(View view) {
        TimePickerFragment.Companion.start(this);
    }

    public void cDataPickerDialog(View view) {
        DatePickerFragment.Companion.start(this);
    }

    public void cDialogFragment(View view) {
        Dialog.showFragment(this, "123");
    }

    public void cAlertActivity(View view) {
        AlertActivity.start(this);
    }
}
