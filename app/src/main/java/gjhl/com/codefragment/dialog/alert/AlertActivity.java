package gjhl.com.codefragment.dialog.alert;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class AlertActivity extends AppCompatActivity {
    private static final String TAG = "DialogOriginActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, AlertActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_origin_activity);

    }

    public void cShowAlertDialog(View view) {
        showInput();
    }

    /**
     * 一个输入框的 AlertDialog 弹窗
     */
    private void showInput() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View layout = inflater.inflate(R.layout.dialog_input, null);
        EditText etAddress = layout.findViewById(R.id.etAddress);
        EditText etName = layout.findViewById(R.id.etName);
        etAddress.setText("address");
        etName.setText("name");
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle("网址和端口").setView(layout)
                .setPositiveButton("读取输入框内容", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String data = etAddress.getText().toString();
                        String name = etName.getText().toString();
                        Log.i(TAG, "onClick: " + data + name);
                    }
                });
        builder.create().show();
    }

}
