package gjhl.com.codefragment.dialog.picker

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import java.util.*

// https://developer.android.com/guide/topics/ui/controls/pickers#kotlin
class TimePickerFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {
    private val TAG = "TimePickerFragment"

    companion object {
        fun start(a: FragmentActivity) {
            val newFragment = TimePickerFragment()
            newFragment.show(a.supportFragmentManager, "timePicker")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current time as the default values for the picker
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        // Create a new instance of TimePickerDialog and return it
        return TimePickerDialog(activity, this, hour, minute, DateFormat.is24HourFormat(activity))
    }

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        // Do something with the time chosen by the user
        Log.i(TAG, "onTimeSet: $hourOfDay:$minute")
    }
}