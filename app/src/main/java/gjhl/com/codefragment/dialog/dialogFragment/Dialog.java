package gjhl.com.codefragment.dialog.dialogFragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import gjhl.com.codefragment.R;

public class Dialog extends DialogFragment {
    public static void showFragment(FragmentActivity activity, String statistical_user_label) {
        DialogKt fragment = (DialogKt) activity.getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment != null) {
            activity.getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }

        fragment = new DialogKt();
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        if (!activity.isFinishing()) {
            ft.add(fragment, "dialog").commitAllowingStateLoss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().setCanceledOnTouchOutside(false);
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = 100;
        params.gravity = Gravity.TOP;
        window.setAttributes(params);
        View decorView = window.getDecorView();
        decorView.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        //         val view = inflater.inflate(R.layout.dialog, container, false);
        View view = inflater.inflate(R.layout.dialog, container, false);
        onEventMainThread();
        return view;
    }

    public void onEventMainThread() {
        getInArguments();
    }

    private void getInArguments() {
        if (getArguments() == null) {
            dismissAllowingStateLoss();
            return;
        }
    }
}
