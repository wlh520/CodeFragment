package gjhl.com.codefragment.dialog.dialogFragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import gjhl.com.codefragment.R

class DialogKt : DialogFragment() {
    companion object {
        fun showFragment(activity: FragmentActivity) {
            var fragment = activity.supportFragmentManager.findFragmentByTag("dialog") as DialogKt?
            if (fragment != null) {
                activity.supportFragmentManager.beginTransaction().remove(fragment)
                    .commitAllowingStateLoss()
            }
            fragment = DialogKt()
            val ft = activity.supportFragmentManager.beginTransaction()
            if (!activity.isFinishing) {
                ft.add(fragment, "dialog").commitAllowingStateLoss()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        // 禁止返回
        dialog!!.setCancelable(false);
        // 禁止外部点击
        dialog!!.setCanceledOnTouchOutside(false)
        val window = dialog!!.window
        val decorView = window!!.decorView
        decorView.setBackgroundColor(Color.TRANSPARENT)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = inflater.inflate(R.layout.dialog, container, false);
        return view
    }
}