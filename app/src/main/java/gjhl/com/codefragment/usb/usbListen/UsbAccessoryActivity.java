package gjhl.com.codefragment.usb.usbListen;
 
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.exit.Util;

public class UsbAccessoryActivity extends AppCompatActivity {
    public static void start(Activity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, UsbAccessoryActivity.class);
        appCompatActivity.startActivity(intent);
    }

    private static final String TAG = "UsbAccessoryActivity";
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usb_accessory);

        UsbManager usbManager = (UsbManager)getApplicationContext().getSystemService(USB_SERVICE);
        PendingIntent permissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);


        USBBroadcastReceiver usbBroadcastReceiver = new USBBroadcastReceiver();
        usbBroadcastReceiver.setUsbListener(()->{
            HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
            Collection<UsbDevice> ite = usbDevices.values();
            UsbDevice[] usbs = ite.toArray(new UsbDevice[]{});
            for (UsbDevice usb : usbs) {
                usbManager.requestPermission(usb, permissionIntent);
                Log.d("Connected usb devices","Connected usb devices are "+ usb.getDeviceName());
                String s = Util.getFileContent(new File(usb.getDeviceName()+"/a.txt"));
                Log.d(TAG, "Connected usb: " + s);
            }

        });
        usbBroadcastReceiver.registerUsbReceiver(this);
        
    }
}