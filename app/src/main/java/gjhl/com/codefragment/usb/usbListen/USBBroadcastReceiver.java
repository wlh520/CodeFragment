package gjhl.com.codefragment.usb.usbListen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;

public class USBBroadcastReceiver extends BroadcastReceiver {

    private OnUsbListener usbListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action) ||
                UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
            usbListener.onStateChanged();
        }
    }

    public void registerUsbReceiver(Context context) {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        context.registerReceiver(this, filter);
    }

    public void unregisterUsbReceiver(Context context) {
        context.unregisterReceiver(this);
    }

    public void setUsbListener(OnUsbListener usbListener) {
        this.usbListener = usbListener;
    }

    @FunctionalInterface
    public interface OnUsbListener {
        void onStateChanged();
    }
}