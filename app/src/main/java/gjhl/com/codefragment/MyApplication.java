package gjhl.com.codefragment;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.drawee.backends.pipeline.Fresco;

import gjhl.com.codefragment.util.CrashHandlerUtil;

public class MyApplication extends Application {
    private static MyApplication context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        Fresco.initialize(this);
        CrashHandlerUtil.getInstance().init(this);
        getCurrentActivity();
    }

/*
 registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                ActivityManagerW.getManager().addActivity(activity as FragmentActivity)
            }
            override fun onActivityStarted(activity: Activity) {}
            override fun onActivityResumed(activity: Activity) {
                ActivityManagerW.getManager().setCurrentActivity(activity as FragmentActivity)
            }

            override fun onActivityPaused(activity: Activity) {}
            override fun onActivityStopped(activity: Activity) {}
            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}
            override fun onActivityDestroyed(activity: Activity) {
                ActivityManagerW.getManager().removeActivity(activity as FragmentActivity)
            }
        }) 
* */
    // 获取当前的activity
    private void getCurrentActivity() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(@NonNull Activity activity) {

            }

            @Override
            public void onActivityResumed(@NonNull Activity activity) {
                // get current activity
            }

            @Override
            public void onActivityPaused(@NonNull Activity activity) {

            }

            @Override
            public void onActivityStopped(@NonNull Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(@NonNull Activity activity) {

            }
        });
    }


    public static MyApplication getContext() {
        return context;
    }
}

/**
 * adb
 * 文件查看
 * adb shell -- 进入根目录
 * ls -- 查看子目录
 * cd storage -- 进入子目录
 * exit -- 退出
 *
 * cat system/build.prop 查看系统配置
 *
 * gradle 配置签名：https://blog.csdn.net/shineflowers/article/details/45042485?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.essearch_pc_relevant&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.essearch_pc_relevant
 * 查看签名：https://lbs.amap.com/faq/android/map-sdk/create-project/43112
 */
