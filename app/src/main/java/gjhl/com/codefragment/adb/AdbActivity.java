package gjhl.com.codefragment.adb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.recyclerView.drag.DragRVActivity;

// 命令
public class AdbActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, AdbActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adb_activity);
    }

    public void cShutdown(View v) {
        try {
            Runtime.getRuntime().exec("reboot -p"); //关机 adb shell reboot -p
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cReboot(View v) {
        try {
            Runtime.getRuntime().exec("reboot"); //重启 adb shell reboot -p
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cCuteScreen(View v) {
        try {
            Runtime.getRuntime().exec("/system/bin/screencap -p /sdcard/screenshot.png"); //关机 adb shell reboot -p
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
