package gjhl.com.codefragment.SpecialSign;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.util.SpecialSignFilterUtil;

public class SpecialSignActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SpecialSignActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_sign);
        EditText et = findViewById(R.id.et);
        SpecialSignFilterUtil.setProhibitEmoji(et, this);
    }
}
