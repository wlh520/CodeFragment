package gjhl.com.codefragment.synchronizedTest;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import gjhl.com.codefragment.R;

public class SynchronizedActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, SynchronizedActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synchronized);
        bSynchronized();
        bNoSynchronized();
        bMoreUpdate();
        bSynchronizedThread();
        bNoSynchronizedThread();
    }

    /**
     * main:
     * thread1:
     * processChatmsg: 1579155800822
     * processChatmsg: 1579155800822
     */
    private void bNoSynchronizedThread() {
        findViewById(R.id.bNoSynchronizedThread).setOnClickListener(v -> {
            new Thread("thread1:") {
                @Override
                public void run() {
                    super.run();
                    Log.i("WLH", "thread1: ");
                    processChatmsg3();
                }
            }.start();

            Log.i("WLH", "main: ");
            processChatmsg3();
        });
    }

    /**
     * main:
     * thread1:
     * processChatmsg: 1579155388736
     * processChatmsg: 1579155389738
     */
    private void bSynchronizedThread() {
        findViewById(R.id.bSynchronizedThread).setOnClickListener(v -> {
            new Thread("thread1:") {
                @Override
                public void run() {
                    super.run();
                    Log.i("WLH", "thread1: ");
                    processChatmsg();
                }
            }.start();

            Log.i("WLH", "main: ");
            processChatmsg();
        });
    }

    /**
     * processChatmsg: 1579144734308
     * processChatmsg: 1579144735308
     * processChatmsg: 1579144736309
     * onCreate: 1579144736309
     */
    private void bNoSynchronized() {
        findViewById(R.id.bNoSynchronized).setOnClickListener(v -> {
            processChatmsg3();
            processChatmsg3();
            processChatmsg3();
        });
    }

    /**
     * processChatmsg: 1579153496582 i=1
     * bMoreUpdate: 1579153496582
     * bMoreUpdate: 1579153496583
     * bMoreUpdate: 1579153496583
     * run: 1main
     * processChatmsg: 1579153497600 i=2
     * run: 2main
     * processChatmsg: 1579153498610 i=3
     * run: 3main
     * processChatmsg: 1579153499619 i=4
     */
    private void bMoreUpdate() {
        findViewById(R.id.bMoreUpdate).setOnClickListener(v -> {
            new Handler().post(() -> {
                Log.i("WLH", "run: 1" + Thread.currentThread().getName());
                processChatmsg2();
            });
            processChatmsg2();
            Log.i("WLH", "bMoreUpdate: " + System.currentTimeMillis());

            new Handler().post(() -> {
                Log.i("WLH", "run: 2" + Thread.currentThread().getName());
                processChatmsg2();
            });
            Log.i("WLH", "bMoreUpdate: " + System.currentTimeMillis());

            new Handler().post(() -> {
                Log.i("WLH", "run: 3" + Thread.currentThread().getName());
                processChatmsg2();
            });
            Log.i("WLH", "bMoreUpdate: " + System.currentTimeMillis());
        });
    }

    /**
     * processChatmsg: 1579153798898
     * processChatmsg: 1579153799899
     * processChatmsg: 1579153800899
     */
    private void bSynchronized() {
        findViewById(R.id.bSynchronized).setOnClickListener(v -> {
            processChatmsg();
            processChatmsg();
            processChatmsg();
        });
    }

    private int i;

    public void processChatmsg2() {
        i++;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i("WLH", "processChatmsg: " + System.currentTimeMillis() + " i=" + i);
    }

    public synchronized void processChatmsg() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i("WLH", "processChatmsg: " + System.currentTimeMillis());
    }

    public void processChatmsg3() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i("WLH", "processChatmsg: " + System.currentTimeMillis());
    }
}