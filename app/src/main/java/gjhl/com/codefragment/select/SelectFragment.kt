package gjhl.com.codefragment.select

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import gjhl.com.codefragment.R

class SelectFragment : Fragment() {
    companion object {
        fun newInstance(): SelectFragment {
            return SelectFragment()
        }
    }
    private var preTvSecond: TextView? = null

    lateinit var rootView: View
    lateinit var recyclerview: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.select_fragment, container, false)
        recyclerview = rootView.findViewById(R.id.recyclerview)
        setAdapter()
        initEvent(recyclerview) { scrollToTop() }
        scrollRvToDefault()
        return rootView
    }

    private fun setAdapter() {
        val linearLayoutManager = LinearLayoutManager(activity!!)
        recyclerview.layoutManager = linearLayoutManager
        val data = ArrayList<String>()
        for (i in 150..200) data.add("${i}cm")
        recyclerview.adapter = RecyclerAdapter(data)
    }

    private fun scrollToTop() {
        preTvSecond?.setTextColor(Color.parseColor("#000000"))
        val secondView = recyclerview.getChildAt(2)
        val tv = secondView.findViewById(R.id.tv_titlt) as TextView
        tv.setTextColor(Color.parseColor("#303F9F"))
        val top = recyclerview.getChildAt(0).top
        recyclerview.smoothScrollBy(0, top)
        preTvSecond = tv
    }

    private fun scrollRvToDefault() {
        /*
        * 因为recycle view 加载 adapter 的布局需要时间，所以要给些时间布局。之后在滚动recycle view
        * */
        val handler = Handler(Looper.getMainLooper())
        recyclerview.scrollToPosition(10)
        handler.postDelayed({
            scrollToTop()
        }, 500)
    }

    private fun initEvent(recyclerView: RecyclerView, back: () -> Unit) {
        recyclerView.addOnScrollListener(RecyclerViewListener(back))

        recyclerView.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                recyclerView.stopScroll()
            }

            false
        }
    }
}

class RecyclerViewListener(val back: () -> Unit) : RecyclerView.OnScrollListener() {
    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        // 在空闲的时候继续滚动到顶部
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            back()
        }
    }
}