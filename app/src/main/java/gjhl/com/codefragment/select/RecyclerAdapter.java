package gjhl.com.codefragment.select;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import gjhl.com.codefragment.R;

/**
 * Date: 2018/5/14
 * Author: wlh
 * 一个包含100条数据的 adapter
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private ArrayList<String> mStrings;

    public RecyclerAdapter(ArrayList<String> strings) {
        mStrings = strings;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_main_recycle_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if (position - 2 >= 0 && position - 2 < mStrings.size())
            holder.title.setText(mStrings.get(position - 2));
        else
            holder.title.setText("");
    }

    @Override
    public int getItemCount() {
        return mStrings.size() + 2 + 2;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_titlt);
        }
    }
}
