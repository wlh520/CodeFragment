package gjhl.com.codefragment.select;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.util.WUtil;

public class SelectActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SelectActivity.class);
        activity.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_activity);

        WUtil.fragment(this, SelectFragment.Companion.newInstance(), R.id.fl);
    }

}
