package gjhl.com.codefragment.towScreen;

import android.content.Context;
import android.media.MediaRouter;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class HelpHandPresentation {
    private WindowManager.LayoutParams layoutParams;
    private Display secondDisplay;
    private Context secondDisplayContext;
    private View view;
    private WindowManager windowManager;
    public void addPresentation(Context paramContext) {
        Display display = ((MediaRouter) paramContext.getSystemService(Context.MEDIA_ROUTER_SERVICE)).getSelectedRoute(2).getPresentationDisplay();
        this.secondDisplay = display;
        if (display != null) {
            this.windowManager = (WindowManager) paramContext.createDisplayContext(display).getSystemService(Context.WINDOW_SERVICE);
            this.secondDisplayContext = paramContext.createDisplayContext(this.secondDisplay);
            return;
        }
    }
    public View addView(int paramInt) {
        this.view = View.inflate(this.secondDisplayContext, paramInt, null);
        this.layoutParams = new WindowManager.LayoutParams(2003, 3, 3);
        if (Build.VERSION.SDK_INT >= 26) {
            this.layoutParams.type = 2038;
        } else {
            this.layoutParams.type = 2003;
        }

        this.windowManager.addView(this.view, this.layoutParams);
        return this.view;
    }
    public void presentationAddView() {
        this.windowManager.addView(this.view, this.layoutParams);
    }
    public void removeLayoutView() {
        this.windowManager.removeView(this.view);
    }
}