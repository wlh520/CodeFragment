package gjhl.com.codefragment.towScreen

import android.R.layout
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import gjhl.com.codefragment.R
import gjhl.com.codefragment.fileSelector.FileSelectorUtils.context
import gjhl.com.codefragment.returnW.ReturnActivity


// 双屏异显
class TowScreenActivity: AppCompatActivity() {
    companion object {
        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, TowScreenActivity::class.java)
            activity.startActivity(intent)
        }
    }
    private val helpHandPresentation = HelpHandPresentation()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tow_screen_activity)

        helpHandPresentation.addPresentation(this)
        helpHandPresentation.addView(R.layout.tow_screen_2)
    }

    override fun onDestroy() {
        super.onDestroy()
        helpHandPresentation.removeLayoutView()
    }
}