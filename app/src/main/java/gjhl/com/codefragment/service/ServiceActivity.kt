package gjhl.com.codefragment.service

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import gjhl.com.codefragment.R
import gjhl.com.codefragment.databinding.ServiceActivityBinding

class ServiceActivity : AppCompatActivity() {
    companion object {
        fun start(activity: AppCompatActivity) {
            val intent = Intent(activity, ServiceActivity::class.java)
            activity.startActivity(intent)
        }
    }

    val binding: ServiceActivityBinding by lazy {
        DataBindingUtil.setContentView(this,
                R.layout.service_activity) as ServiceActivityBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.click = this
    }

    fun cStart() {
        val intent = Intent(this, ForegroundService::class.java)
        startService(intent)
    }

    fun cNormal() {
        val intent = Intent(this, NormalService::class.java)
        startService(intent)
    }
}