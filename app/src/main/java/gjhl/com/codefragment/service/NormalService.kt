package gjhl.com.codefragment.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log

// 没关闭，服务就会一直启动着
class NormalService : Service() {
    private val TAG = "NormalService"
    
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        Log.i(TAG, "onCreate: ")
        super.onCreate()
    }

    override fun onDestroy() {
        Log.i(TAG, "onDestroy: ")
        super.onDestroy()
    }

}