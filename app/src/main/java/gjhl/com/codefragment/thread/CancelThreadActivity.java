package gjhl.com.codefragment.thread;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class CancelThreadActivity extends AppCompatActivity {
    private static final String TAG = "CancelThreadActivity";
    private MyThread myThread;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, CancelThreadActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancel_thread_acitvity);
        myThread = new MyThread("aa");
        myThread.start();
        Log.i(TAG, "onCreate: ");
    }
    
    public void cCancel(View v) {
        myThread.cancel();
    }
    public void cCancel2(View v) {
        myThread.cancel2();
    }

    public static class MyThread extends Thread {
        private String s;
        private boolean isRunning = true;
        public final Object mSyncObj = new Object();

        public MyThread(String s) {
            this.s = s;
        }

        public void run() {
            try {
                while (isRunning) {
                    sleep(2000);
                    Log.i(TAG, "run: " + s);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /*
        2022-11-07 11:26:15.668 10799-10854/gjhl.com.codefragment I/CancelThreadActivity: run: aa
        2022-11-07 11:26:17.669 10799-10854/gjhl.com.codefragment I/CancelThreadActivity: run: aa
        2022-11-07 11:26:18.693 10799-10799/gjhl.com.codefragment I/CancelThreadActivity: cancel1: 1
        2022-11-07 11:26:19.670 10799-10854/gjhl.com.codefragment I/CancelThreadActivity: run: aa
        2022-11-07 11:26:19.671 10799-10799/gjhl.com.codefragment I/CancelThreadActivity: cancel1: 2
         */
        public void cancel() {
            isRunning = false;
            Log.i(TAG, "cancel1: 1");
            try {
                synchronized (mSyncObj) {
                    mSyncObj.notifyAll();
                    Log.i(TAG, "cancel1: 2");
                }
                Log.i(TAG, "cancel1: 3");
                this.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.i(TAG, "cancel1: 4"); // 线程结束后运行，如果 注释掉 isRunning = false 会一直运行不到这一步
        }

        /*
        2022-11-07 11:27:18.901 10799-10892/gjhl.com.codefragment I/CancelThreadActivity: run: aa
        2022-11-07 11:27:20.902 10799-10892/gjhl.com.codefragment I/CancelThreadActivity: run: aa
        2022-11-07 11:27:21.626 10799-10799/gjhl.com.codefragment I/CancelThreadActivity: cancel2:
        2022-11-07 11:27:22.902 10799-10892/gjhl.com.codefragment I/CancelThreadActivity: run: aa
         */
        public void cancel2() {
            isRunning = false;
            Log.i(TAG, "cancel2: ");
        }
    }
}
