package gjhl.com.codefragment.thread;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import gjhl.com.codefragment.R;

// 线程
/*
 object : Thread(){
            override fun run() {
                super.run()

            }
        }.start()
 */
/*
new Thread() {
            @Override
            public void run() {
                super.run();
            }
        }.start();
 */
public class ThreadActivity extends AppCompatActivity {
    private static final String TAG = "ThreadActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ThreadActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thread_activity);
        MyThread myThread = new MyThread("dfsfa");
        Log.i(TAG, "onCreate: 1");
        myThread.start();
        Log.i(TAG, "onCreate: 2");
    }

    public class MyThread extends Thread {
        private String s;

        public MyThread(String s) {
            this.s = s;
        }

        public void run() {
            try {
                sleep(100);
                Log.i(TAG, "run: " + s);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
