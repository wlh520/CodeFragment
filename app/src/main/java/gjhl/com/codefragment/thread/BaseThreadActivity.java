package gjhl.com.codefragment.thread;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;

public class BaseThreadActivity extends AppCompatActivity {
    private static final String TAG = "BaseThreadActivity";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, BaseThreadActivity.class);
        activity.startActivity(intent);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_thread_activity);
    }

    /*
    匿名内部类
     new Thread() {
            @Override
            public void run() {
                super.run();
            }
        }.start();
     */
    public void cSimple(View v) {
        ThreadActivity.start(this);
    }

    public void cCancel(View v) {
        CancelThreadActivity.start(this);
    }
}
