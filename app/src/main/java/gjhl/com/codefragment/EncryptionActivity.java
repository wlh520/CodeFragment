package gjhl.com.codefragment;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Calendar;

public class EncryptionActivity extends AppCompatActivity {
    private static final String TAG = "EncryptionActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, EncryptionActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encryption);

        Calendar rightNow = Calendar.getInstance();
        long nowTimeTemp = rightNow.getTimeInMillis();
        rightNow.add(Calendar.MILLISECOND, 120 * 1000);
        long overTimeTemp = rightNow.getTimeInMillis();
        Log.i(TAG, "onCreate: signal- " + Home_Scan_Util.getSignal(nowTimeTemp) + " nowTimeTemp- " + nowTimeTemp + " overTimeIvalStr- " + overTimeTemp);
    }
}
