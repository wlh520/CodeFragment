package gjhl.com.codefragment.score;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import gjhl.com.codefragment.R;

public class ScoreActivity extends AppCompatActivity {
    private static final String TAG = "ScoreActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, ScoreActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri uri = Uri.parse("market://details?id=" + getPackageName());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (Exception e) {
//                    Toast.makeText(this, "您的系统中没有安装应用市场", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.fl, ScoreFragment.newInstance()).commit();
    }
    private void score() {
        try
        {
            Uri uri = Uri.parse("market://details?id=" + getPackageName()); // 要的资源路径
            Intent intent = new Intent(new Intent(Intent.ACTION_VIEW, uri)); // ACTION_VIEW: 展示资源视图，多种展示方式弹选择框
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // FLAG_ACTIVITY_NEW_TASK: 新开个任务
            startActivity(intent);
        }
        catch (Exception ex)
        {
            Log.e(TAG, "onCreate: ", ex);
            Toast.makeText(ScoreActivity.this, "您的手机还没有安装任何安装安装应用市场", Toast.LENGTH_SHORT).show();
        }
    }
}
