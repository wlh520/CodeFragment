package gjhl.com.codefragment.score;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import gjhl.com.codefragment.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ScoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScoreFragment extends Fragment {
    private static final String TAG = "ScoreFragment";
    public ScoreFragment() {
        // Required empty public constructor
    }

    public static ScoreFragment newInstance() {
        ScoreFragment fragment = new ScoreFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_score,container, false);
        view.findViewById(R.id.buttonW).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                score(); // 评分
            }
        });
        return view;
    }

    private void score() {
        try
        {
            Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName()); // 要的资源路径
            Intent intent = new Intent(new Intent(Intent.ACTION_VIEW, uri)); // ACTION_VIEW: 展示资源视图，多种展示方式弹选择框
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // FLAG_ACTIVITY_NEW_TASK: 新开个任务
            startActivity(intent);
        }
        catch (Exception ex)
        {
            Log.e(TAG, "onCreate: ", ex);
            Toast.makeText(getContext(), "您的手机还没有安装任何安装安装应用市场", Toast.LENGTH_SHORT).show();
        }
    }
}
