package gjhl.com.codefragment.memory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivityLeakBinding;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

public class LeakActivity extends AppCompatActivity {
    private static final String TAG = "LeakActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, LeakActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLeakBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_leak);
       /* Handler handler = new Handler();
        handler.postDelayed(() -> {
            Log.i(TAG, "onCreate: ");
        }, 1000 * 60);
*/
        AnimatorSet resultAnimatorSet2 = new AnimatorSet();
        ObjectAnimator leftTranslationX = ObjectAnimator.ofFloat(binding.iv, "translationX", 0, 60);
        ObjectAnimator leftTranslationY = ObjectAnimator.ofFloat(binding.iv, "translationY", 0, 300);
        resultAnimatorSet2.playTogether(leftTranslationX, leftTranslationY);
        resultAnimatorSet2.setDuration(300);
        resultAnimatorSet2.setStartDelay(60 * 1000);
        resultAnimatorSet2.start();
        resultAnimatorSet2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                Log.i(TAG, "onAnimationEnd: ");
            }
        });
    }
}
