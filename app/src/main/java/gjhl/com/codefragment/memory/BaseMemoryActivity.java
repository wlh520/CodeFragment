package gjhl.com.codefragment.memory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivityBaseMemoryBinding;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class BaseMemoryActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, BaseMemoryActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBaseMemoryBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_base_memory);
        binding.setActivity(this);
    }

    public void cShowLeakActivity() {
        LeakActivity.start(this);
    }

}
