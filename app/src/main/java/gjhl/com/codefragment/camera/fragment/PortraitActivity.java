package gjhl.com.codefragment.camera.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.camera.CameraActivity;

public class PortraitActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, PortraitActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.portrait_activity);

        getSupportFragmentManager().beginTransaction().replace(R.id.fl, new PortraitFragment()).commitAllowingStateLoss();
    }
}
