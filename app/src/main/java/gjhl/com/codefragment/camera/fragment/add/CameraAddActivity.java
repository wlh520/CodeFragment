
package gjhl.com.codefragment.camera.fragment.add;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.camera.fragment.PortraitFragment;

public class CameraAddActivity extends AppCompatActivity {
    public static void start(android.app.Activity activity) {
        Intent intent = new Intent(activity, CameraAddActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.portrait_activity);

        getSupportFragmentManager().beginTransaction().replace(R.id.fl, CameraAddFragment.newInstance(bitmap -> {
            ImageView iv = findViewById(R.id.iv);
            iv.setImageBitmap(bitmap);
        })).commitAllowingStateLoss();
    }
}
