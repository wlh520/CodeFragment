package gjhl.com.codefragment.voice;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.recyclerview.widget.RecyclerView;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ItemCpVoiceBinding;

public class CPVoiceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements LifecycleObserver {
    private List<Bean> list;
    private MediaPlayer mMediaPlayer;
    private Handler mHandler = new Handler();
    private Bean mPreBean;

    public CPVoiceAdapter(List<Bean> list, Lifecycle lifecycle) {
        this.list = list;
        lifecycle.addObserver(this);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cp_voice, parent, false);
        ItemCpVoiceBinding binding = ItemCpVoiceBinding.bind(view);
        MyHolder holder = new MyHolder(view);
        holder.setBinding(binding);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ItemCpVoiceBinding binding = ((MyHolder) holder).binding;
        Bean bean = list.get(position);
        if (bean.isSelect()) {
            String url = bean.url; // your URL here
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mMediaPlayer.setDataSource(url);
            } catch (IOException | IllegalArgumentException | SecurityException | IllegalStateException e) {
                e.printStackTrace();
            }
            mMediaPlayer.prepareAsync();
            mMediaPlayer.setOnPreparedListener(MediaPlayer::start);
        } else {
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
            }
        }

        binding.ivStart.setOnClickListener(v->{
            mHandler.postDelayed(()->{
                bean.setSelect(true);
                notifyItemChanged(position);
            }, 100);

            if (mPreBean != null) {
                mPreBean.setSelect(false);
                notifyItemChanged(mPreBean.getPosition());
            }
            mPreBean = bean;
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onStop() {
        if (mMediaPlayer!=null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        if (mHandler!=null) {
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    class MyHolder extends RecyclerView.ViewHolder {
        private ItemCpVoiceBinding binding;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
        }

        public ItemCpVoiceBinding getBinding() {
            return binding;
        }

        public void setBinding(ItemCpVoiceBinding binding) {
            this.binding = binding;
        }
    }

    public static class Bean {
        private String url;
        private boolean select;
        private int position;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public boolean isSelect() {
            return select;
        }

        public void setSelect(boolean select) {
            this.select = select;
        }
    }
}
