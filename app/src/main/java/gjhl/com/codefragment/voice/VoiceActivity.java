package gjhl.com.codefragment.voice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivityVoiceBinding;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

// 音频 https://developer.android.com/guide/topics/media/mediaplayer
public class VoiceActivity extends AppCompatActivity {
    private static final String API_RES = "https://res.appbocai.com/";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, VoiceActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityVoiceBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_voice);

        initRv(binding);
        cRawMp3();
    }

    private void cRawMp3() {
        findViewById(R.id.bMp3).setOnClickListener(v->{
            Mp3Activity.start(this);
        });
    }

    private void initRv(gjhl.com.codefragment.databinding.ActivityVoiceBinding binding) {
        List<String> voiceUrls = new ArrayList<>();
        voiceUrls.add(API_RES + "audio/20200316/d6539c6290bacdf91b625e06f656cd88.amr");
        voiceUrls.add(API_RES + "audio/20200316/d428d300725f6caeba88a0ba6e4ddd2a.amr");
        voiceUrls.add(API_RES + "audio/20200316/c70ac2b72873f27defd42add9b63b8e9.amr");
        voiceUrls.add(API_RES + "audio/20200316/eb2434bde879db7d82fff57bf70d4f7a.amr");
        voiceUrls.add(API_RES + "audio/20200316/b16ab207604eeb8da67802a8bb3e4a5e.amr");
        voiceUrls.add(API_RES + "audio/20200316/76a7460bc8f71ec69aefdac2961804b7.amr");
        voiceUrls.add(API_RES + "audio/20200316/2bd97a9bfdaa98be62f642f9af8f0274.amr");
        voiceUrls.add(API_RES + "audio/20200316/c1f6579d3341f627ec5660b9d3b5312e.amr");

        List<CPVoiceAdapter.Bean> list = new ArrayList<>();
        int i = 0;
        for (String s : voiceUrls) {
            CPVoiceAdapter.Bean bean = new CPVoiceAdapter.Bean();
            bean.setUrl(s);
            bean.setPosition(i++);
            list.add(bean);
        }
        CPVoiceAdapter adapter = new CPVoiceAdapter(list, getLifecycle());

        binding.list.setLayoutManager(new LinearLayoutManager(this));
        binding.list.setAdapter(adapter);
    }

    public void cLocalMp3(View view) {
        LocalMp3Activity.start(this);
    }

    public void cSpeak(View view) {
        SystemTTS systemTTS  = SystemTTS.getInstance(this);
        systemTTS.playText("已开门");
    }
}

/*
code200
time1584323237
message
value
anchorVoiceList
0
voice audio/20200316/d6539c6290bacdf91b625e06f656cd88.amr
voiceTime5
1
voice audio/20200316/d428d300725f6caeba88a0ba6e4ddd2a.amr
voiceTime5
2
voice audio/20200316/c70ac2b72873f27defd42add9b63b8e9.amr
voiceTime6
3
voice audio/20200316/eb2434bde879db7d82fff57bf70d4f7a.amr
voiceTime7
4
voice audio/20200316/b16ab207604eeb8da67802a8bb3e4a5e.amr
voiceTime6
5
voice audio/20200316/76a7460bc8f71ec69aefdac2961804b7.amr
voiceTime8
6
voice audio/20200316/2bd97a9bfdaa98be62f642f9af8f0274.amr
voiceTime20
7
voice audio/20200316/c1f6579d3341f627ec5660b9d3b5312e.amr
voiceTime5
 */