package gjhl.com.codefragment.voice;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import gjhl.com.codefragment.R;

public class Mp3Activity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, Mp3Activity.class);
        activity.startActivity(intent);
    }
    private SoundPool sp;
    private int spId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mp3_activity);

        //    private static final HashMap<Integer, Integer> soundIds = new HashMap<>();
//         soundIds.put(rawId, sp.load(MyApplication.getContext(), rawId, 1));
        SoundPool.Builder soundPoolBuilder = new SoundPool.Builder();
        soundPoolBuilder.setMaxStreams(5);
        AudioAttributes.Builder audioAttributesBuilder = new AudioAttributes.Builder();
        audioAttributesBuilder.setLegacyStreamType(AudioManager.STREAM_SYSTEM);//同下
        soundPoolBuilder.setAudioAttributes(audioAttributesBuilder.build());
        sp = soundPoolBuilder.build();
        spId = sp.load(this, R.raw.give_back_key_success, 1);
        findViewById(R.id.bMp3).setOnClickListener(v -> {
            playMp3();
        });
    }

    private void playMp3() {
        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.mp3);
        mediaPlayer.start(); // no need to call prepare(); create() does that for you
    }

    public void cSoundPool(View view) {
        sp.play(spId, 0.9f,0.9f,1,0,1);
    }

    public void cPlay(View view) {
        //实例化播放内核
        MediaPlayer mediaPlayer = new MediaPlayer();
//获得播放源访问入口
        AssetFileDescriptor afd = getResources().openRawResourceFd(R.raw.give_back_key_success); // 注意这里的区别
//给MediaPlayer设置播放源
        try {
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
        } catch (IOException e) {
            e.printStackTrace();
        }
//设置准备就绪状态监听
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // 开始播放
                mediaPlayer.start();
            }
        });
//准备播放
        mediaPlayer.prepareAsync();
    }
}
