package gjhl.com.codefragment.voice;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

import gjhl.com.codefragment.R;

public class LocalMp3Activity extends AppCompatActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, LocalMp3Activity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.local_mp3_activity);
    }

    public void initMediaPlayer(View view) {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), "music.mp3");
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setLooping(true);
            mediaPlayer.setDataSource(file.getPath());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
