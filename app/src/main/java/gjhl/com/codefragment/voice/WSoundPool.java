package gjhl.com.codefragment.voice;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;

import java.util.HashMap;

import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.R;

// 声音播放
public class WSoundPool {
    private static SoundPool sp;
    private static final HashMap<Integer, Integer> soundIds = new HashMap<>();

    public static void init() {
        if (sp == null) {
            SoundPool.Builder soundPoolBuilder = new SoundPool.Builder();
            soundPoolBuilder.setMaxStreams(5);
            AudioAttributes.Builder audioAttributesBuilder = new AudioAttributes.Builder();
            audioAttributesBuilder.setLegacyStreamType(AudioManager.STREAM_SYSTEM);//同下
            soundPoolBuilder.setAudioAttributes(audioAttributesBuilder.build());
            sp = soundPoolBuilder.build();
            put(R.raw.give_back_key_success);
        }
    }

    private static void put(int rawId) {
        soundIds.put(rawId, sp.load(MyApplication.getContext(), rawId, 1));
    }

    public static void play(int rawId) {
        sp.play(soundIds.get(rawId), 0.9f,0.9f,1,0,1);
    }
}
