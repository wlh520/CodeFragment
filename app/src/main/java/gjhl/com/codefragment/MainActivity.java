package gjhl.com.codefragment;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.google.android.material.snackbar.Snackbar;

import gjhl.com.codefragment.SpecialSign.SpecialSignActivity;
import gjhl.com.codefragment.gpio.GpioActivity;
import gjhl.com.codefragment.thread.BaseThreadActivity;
import gjhl.com.codefragment.util.DataAndStringActivity;
import gjhl.com.codefragment.util.UtilActivity;
import gjhl.com.codefragment.activityPassValue.FatherActivity;
import gjhl.com.codefragment.adBroadcast.AdBroadcastActivity;
import gjhl.com.codefragment.adb.AdbActivity;
import gjhl.com.codefragment.appMarket.AppMarketActivity;
import gjhl.com.codefragment.assetsJson.AssetsJsonActivity;
import gjhl.com.codefragment.camera.CameraActivity;
import gjhl.com.codefragment.cuteScreen.CuteScreenActivity;
import gjhl.com.codefragment.dataSave.DataSaveActivity;
import gjhl.com.codefragment.dialog.DialogActivity;
import gjhl.com.codefragment.editText.EditActivity;
import gjhl.com.codefragment.file.FileActivity;
import gjhl.com.codefragment.fileSelector.FileSelectorActivity;
import gjhl.com.codefragment.fragment.FatherFragment;
import gjhl.com.codefragment.glide.GlideActivity;
import gjhl.com.codefragment.html.HtmlActivity;
import gjhl.com.codefragment.net.http.HttpActivity;
import gjhl.com.codefragment.image.ImageActivity;
import gjhl.com.codefragment.keyboard.HideKeyboardActivity;
import gjhl.com.codefragment.memory.BaseMemoryActivity;
import gjhl.com.codefragment.messageTransfer.LiveDataActivity;
import gjhl.com.codefragment.notification.BaseNotificationActivity;
import gjhl.com.codefragment.openApp.OpenAppActivity;
import gjhl.com.codefragment.permission.PermissionActivity;
import gjhl.com.codefragment.photo.PhotoActivity;
import gjhl.com.codefragment.radioButton.RadioButtonActivity;
import gjhl.com.codefragment.recyclerView.RVActivity;
import gjhl.com.codefragment.returnW.ReturnActivity;
import gjhl.com.codefragment.score.ScoreActivity;
import gjhl.com.codefragment.select.SelectActivity;
import gjhl.com.codefragment.service.ServiceActivity;
import gjhl.com.codefragment.net.tcpIP.TCPipActivity;
import gjhl.com.codefragment.text.ScrollTextActivity;
import gjhl.com.codefragment.slide.QQSwipeActivity;
import gjhl.com.codefragment.rich.BaseSpannableActivity;
import gjhl.com.codefragment.synchronizedTest.SynchronizedActivity;
import gjhl.com.codefragment.usb.usbListen.UsbAccessoryActivity;
import gjhl.com.codefragment.video.VideoBaseActivity;
import gjhl.com.codefragment.view.AddViewActivity;
import gjhl.com.codefragment.view.BaseViewActivity;
import gjhl.com.codefragment.voice.VoiceActivity;

// https://github.com/android/user-interface-samples
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public static Activity activity;


    // 禁用右键退出 鼠标右键
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        setContentView(R.layout.activity_main);

//        TowScreenActivity.Companion.start(this);
//        WebLocalStoreActivity.start(this);
//        WUtil.downloadThread();
        text();
        editText(); // EditText 计数
        btActivity(); // activity 父子传值
        btFullScreen(); // 全屏
        btMeasure(); // 计算屏幕大小尺寸
        btEncryption(); // 加密SHA1
        bHideKeyboard(); // 开始进入隐藏软键盘
        bKeyboard();  // 软键盘高度 是否弹出 是否显示
        bSwipe(); // 侧滑
        bImage(); // adjustViewBounds ImageView 布局
        bAddView(); // 代码添加视图
        bSpecialEmoji(); // 过滤 特殊表情 特殊符号 特色字符
        bAssetsJson(); // json 解析 assets 资源文件获取和解析 城市
        bNotification(); // 通知
        bRadioButton(); // 点选择器
        bDateAndString(); // 字符串分割 时间
        bScore(); // 给app 评分
        bScrollTextView(); // 滚动textview
        bSynchronized(); // Synchronized  Handler thread
        bRv(); // 倒着添加，就跟新倒着的元素

        permission(); // 权限
        statusBar(); // 设置状态栏为白底黑字

        ogjHashCode();
    }

    // 监听点击 监听activity点击
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }
    public static class Bean {
        private boolean select;
    }

    public void cReturnActivity(View view) {
        ReturnActivity.Companion.start(this);
    }

    public void cHint(View view) {
        Snackbar.make(null, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public void cGpio(View view) {
        GpioActivity.start(this);
    }

    public void cMessage(View view) {
        LiveDataActivity.start(this);
    }

    public void cThread(View view) {
        BaseThreadActivity.start(this);
    }

    public void cSelectActivity(View view) {
        SelectActivity.start(this);
    }

    public void cAdbActivity(View view) {
        AdbActivity.start(this);
    }

    public void cOpenAppActivity(View view) {
        OpenAppActivity.start(this);
    }

    public void cFileSelector(View view) {
        FileSelectorActivity.start(this);
    }

    private void ogjHashCode() {
        Bean bean = new Bean();
        Log.i(TAG, "ogjHashCode1: " + System.identityHashCode(bean));
        bean.select = false;
        Log.i(TAG, "ogjHashCode2: " + System.identityHashCode(bean));
        Bean bean2 = new Bean();
        Log.i(TAG, "ogjHashCode3: " + System.identityHashCode(bean2));
    }

    public void cVideoActivity(View view) {
        VideoBaseActivity.start(this);
    }

    public void cTCP_IP(View view) {
        TCPipActivity.start(this);
    }

    public void cHttp(View view) {
        HttpActivity.start(this);
    }

    public void cFile(View view) { // dialog 项目中的弹窗
        FileActivity.Companion.start(this);
    }

    public void cAdBroadcast(View view) {
        AdBroadcastActivity.Companion.start(this);
    }

    public void cDialog(View view) { // dialog 项目中的弹窗
        DialogActivity.start(this);
    }

    public void cShowVoice(View view) { // 音频 声音播放
        VoiceActivity.start(this);
    }

    public void cShowBaseMemoryActivity(View view) { // dialog 项目中的弹窗
        BaseMemoryActivity.start(this);
    }

    public void cUtil(View view) {
        UtilActivity.start(this);
    }

    public void cGlide(View view) {
        GlideActivity.Companion.start(this);
    }

    public void cData(View view) {
        DataSaveActivity.start(this);
    }

    public void cService(View view) {
        ServiceActivity.Companion.start(this);
    }

    public void cAppMarket(View view) {
        AppMarketActivity.start(this);
    }

    public void cView(View view) { // dialog 项目中的弹窗
        BaseViewActivity.start(this);
    }

    public void cSpannable(View view) { // spannable 富文本
        BaseSpannableActivity.start(this);
    }

    public void cHtml(View view) {
        HtmlActivity.start(this);
    }

    public void cUsb(View view) {
        UsbAccessoryActivity.start(this);
    }

    public void cCamera(View view) {
        CameraActivity.start(this);
    }

    private void bRv() {
        findViewById(R.id.bRv).setOnClickListener(v -> RVActivity.start(this));
    }

    private void bSynchronized() {
        findViewById(R.id.bSynchronized).setOnClickListener(v -> SynchronizedActivity.start(this));
    }

    private void bScrollTextView() {
        findViewById(R.id.bScrollTextView).setOnClickListener(v -> ScrollTextActivity.start(this));
    }

    private void bScore() {
        findViewById(R.id.bScore).setOnClickListener(v -> ScoreActivity.start(this));
    }

    // 权限
    public void cPermission(View view) {
        PermissionActivity.start(this);
    }

    private void bDateAndString() {
        findViewById(R.id.bDateAndString).setOnClickListener(v -> DataAndStringActivity.start(this));
    }

    private void bRadioButton() {
        findViewById(R.id.bRadioButton).setOnClickListener(v -> RadioButtonActivity.start(this));
    }

    private void bNotification() {
        findViewById(R.id.bNotification).setOnClickListener(v -> BaseNotificationActivity.Companion.start(this));
    }

    private void bAssetsJson() {
        findViewById(R.id.bAssetsJson).setOnClickListener(v -> AssetsJsonActivity.start(this));
    }

    // 默认拍照 默认图片选择
    public void cPhoto(View view) {
        PhotoActivity.start(this);
    }

    private void bHideKeyboard() {
        findViewById(R.id.bHideKeyboard).setOnClickListener(v -> HideKeyboardActivity.start(this));
    }

    private void bSpecialEmoji() {
        findViewById(R.id.bSpecialSign).setOnClickListener(v -> SpecialSignActivity.start(this));
    }

    /**
     * Android 6.0 以上设置状态栏颜色
     */
    private void statusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // 设置状态栏底色白色
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(Color.WHITE);

            // 设置状态栏字体黑色
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    private void bAddView() {
        findViewById(R.id.bAddView).setOnClickListener(v -> AddViewActivity.start(this));
    }

    public void bCuteScreen(View v) {
        CuteScreenActivity.start(this);
    }

    private void permission() {
        // Android 6.0相机动态权限检查
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS
                    }, 1);
        }
    }

    private void bImage() {
        findViewById(R.id.bImage).setOnClickListener(v -> ImageActivity.start(this));
    }

    private void bSwipe() {
        findViewById(R.id.bSwipe).setOnClickListener(v -> QQSwipeActivity.start(this));
    }

    private void bKeyboard() {
        findViewById(R.id.bKeyboard).setOnClickListener(v -> ListenSoftKeyboardActivity.start(this));
    }


    private void btEncryption() {
        findViewById(R.id.btEncryption).setOnClickListener(v -> EncryptionActivity.start(this));
    }

    private void btMeasure() {
        findViewById(R.id.btMeasure).setOnClickListener(v -> MeasureActivity.start(this));
    }

    private void btFullScreen() {
        findViewById(R.id.btFullScreen).setOnClickListener(v -> FullScreenActivity.start(this));
    }

    private void btActivity() {
        findViewById(R.id.btActivity).setOnClickListener(v -> FatherActivity.start(this));
    }

    public void cKFatherActivity() {

    }

    private void editText() {
        findViewById(R.id.btEditText).setOnClickListener(v -> EditActivity.start(this));
    }

    private void fl() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fl, FatherFragment.newInstance()).commit(); // activity添加fragment
    }

    private void text() {
        findViewById(R.id.btText).setOnClickListener(v -> TextActivity.start(MainActivity.this));
    }
}
