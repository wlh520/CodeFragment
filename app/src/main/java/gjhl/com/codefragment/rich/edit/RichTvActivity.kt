package gjhl.com.codefragment.rich.edit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import gjhl.com.codefragment.R
import gjhl.com.codefragment.databinding.RichTvActivityBinding

/**
 * 主要思路 用 ImageSpan 来实现添加图片
 * 一个图片占一行，不然会有问题（文字输入后）
 * 1.ImageSpan 添加小的图片宽度不是全屏的，自定义CenterImageSpan getSize实现
 * 1.1：宽度是全屏的，不居中自定义CenterImageSpan draw实现
 * 2.光标在图片左侧，自定义 MyEditText 监听光标位置 onSelectionChanged
 * 2.2 删除换行时去掉图片 myEditText.addTextChangedListener
 */
// 富文本编辑
class RichTvActivity : AppCompatActivity() {
    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, RichTvActivity::class.java)
            activity.startActivity(intent)
        }
    }

    private val TAG = "RichTvActivityTAG"
    val binding: RichTvActivityBinding by lazy {
        DataBindingUtil.setContentView(this, R.layout.rich_tv_activity) as RichTvActivityBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.bt.setOnClickListener {
            binding.myEditText.insertDrawable(R.drawable.cp_voice1)
        }
        binding.bt2.setOnClickListener {
            binding.myEditText.insertDrawable(R.drawable.bg_video)
        }
        binding.myEditText.setOnFocusChangeListener { v, hasFocus ->
            Log.i(TAG, "onCreate: $hasFocus")
        }
    }
}