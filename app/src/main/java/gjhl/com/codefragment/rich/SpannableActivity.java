package gjhl.com.codefragment.rich;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivitySpannableBinding;
import gjhl.com.codefragment.exit.CenterImageSpan;
import gjhl.com.codefragment.exit.Util;

public class SpannableActivity extends AppCompatActivity {

    private ActivitySpannableBinding binding;

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, SpannableActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_spannable);
        getBitmap(Uri.parse("https://res.appbocai.com/gift_842e31c38411d214a82724269fcd6bc3.png"));
    }

    private void setText(Bitmap bitmap1) {
        spannableTest1();
        spannableTest2();
        spannableTest3(bitmap1);
    }

    private void spannableTest1() {
        String msg = "123456";
        SpannableString ss = new SpannableString(msg);
        ss.setSpan(new ForegroundColorSpan(Color.parseColor("#fff000")), 2, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.text.setText(ss);
    }

    private void spannableTest2() {
        String msg = "123456$STAR";
        int ddd = Util.dip2px(this, 14);
        Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.money);
        drawable.setBounds(0, 0, ddd, ddd);
        SpannableString ss2 = new SpannableString(msg);
        ss2.setSpan(new CenterImageSpan(drawable), msg.indexOf("$STAR"), msg.indexOf("$STAR")+5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.text2.setText(ss2);
    }

    private void spannableTest3(Bitmap bitmap1) {
        String msg = "123456$STAR789$ICON";
        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap1);
        int ddd = Util.dip2px(this, 14);
        bitmapDrawable.setBounds(0, 0, ddd, ddd);
        SpannableString ss = new SpannableString(msg);
        ss.setSpan(new CenterImageSpan(bitmapDrawable), msg.indexOf("$ICON"), msg.indexOf("$ICON") + 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.text3.setText(ss);
    }

    public void getBitmap(Uri url) {
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(url)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>> dataSource =
                imagePipeline.fetchDecodedImage(imageRequest, MyApplication.getContext());

        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Bitmap bitmap1 = Bitmap.createBitmap(bitmap);
                    setText(bitmap1);
                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {

            }

        }, UiThreadImmediateExecutorService.getInstance());
    }
}
