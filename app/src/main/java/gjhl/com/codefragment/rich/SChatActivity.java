package gjhl.com.codefragment.rich;

import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivitySpannableChatBinding;
import gjhl.com.codefragment.exit.CenterImageSpan;
import gjhl.com.codefragment.exit.Util;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.LeadingMarginSpan;
import android.util.Log;

public class SChatActivity extends AppCompatActivity {
    private static final String TAG = "SChatActivity";

    private ActivitySpannableChatBinding mBinding;

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, SChatActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_spannable_chat);
        getBitmap(Uri.parse("https://res.appbocai.com/gift_842e31c38411d214a82724269fcd6bc3.png"));
    }


    private void spannableTest3(Bitmap bitmap1) {
        // 构造字符串原型
        StringBuilder msg = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            msg.append("$ICON").append(i);
        }

        SpannableString ss = new SpannableString(msg.toString());
        for (int i = 0; i < 2; i++) {
            String iconTemp = "$ICON" + i;

            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap1);
            int ddd = Util.dip2px(this, 14);
            bitmapDrawable.setBounds(0, 0, ddd, ddd);
            ss.setSpan(new CenterImageSpan(bitmapDrawable), msg.indexOf(iconTemp), msg.indexOf(iconTemp) + iconTemp.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        mBinding.t.setText(ss);
        mBinding.t.post(() -> {
            Log.i(TAG, "spannableTest3: post " + mBinding.t.getWidth());
            LeadingMarginSpan.Standard what = new LeadingMarginSpan.Standard((int) mBinding.t.getWidth(), 0);
            SpannableString ssChat = new SpannableString("chat01234567890123456789");
            ssChat.setSpan(what, 0, ssChat.length(), SpannableString.SPAN_INCLUSIVE_INCLUSIVE);
            mBinding.tChat.setText(ssChat);
        });
        /*Log.i(TAG, "spannableTest3: " + mBinding.t.getWidth());
        mRunnable = () -> {
            Log.i(TAG, "spannableTest3: post " + mBinding.t.getWidth());
            LeadingMarginSpan.Standard what = new LeadingMarginSpan.Standard((int) mBinding.t.getWidth(), 0);
            ss.setSpan(what, 0, ss.length(), SpannableString.SPAN_INCLUSIVE_INCLUSIVE);
            mBinding.tChat.setText(ss);
        };
        mBinding.t.postDelayed(mRunnable, 10 * 1000);*/
    }

    Runnable mRunnable;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // view 中 runnable 用postDelayed 会在当前activity 消失后 依旧存在，runnable 运行完后再消失，消失后持有的activity 也会释放
//        mBinding.t.removeCallbacks(mRunnable);
    }

    public void getBitmap(Uri url) {
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(url)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>> dataSource =
                imagePipeline.fetchDecodedImage(imageRequest, MyApplication.getContext());

        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Bitmap bitmap1 = Bitmap.createBitmap(bitmap);
                    spannableTest3(bitmap1);
                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {

            }

        }, UiThreadImmediateExecutorService.getInstance());
    }
}