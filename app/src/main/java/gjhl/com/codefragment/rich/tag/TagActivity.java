package gjhl.com.codefragment.rich.tag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import gjhl.com.codefragment.R;

// https://www.jianshu.com/p/da701b4b8856
// 标签
public class TagActivity extends AppCompatActivity {
    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, TagActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_activity);

        tagTextView();
        tagDrawableTextView();
    }

    private void tagDrawableTextView() {
        TagDrawableTextView tvTagDrawableText = findViewById(R.id.tvTagDrawableText);
        List<TagDrawableTextView.WBean> beans = new ArrayList<>();
        beans.add(new TagDrawableTextView.WBean(R.drawable.round_black, "tag"));
        tvTagDrawableText.setContentAndTag("content", beans);
    }

    private void tagTextView() {
        TagTextView textView = findViewById(R.id.text);
        List<String> tags = new ArrayList<>();
        tags.add("置顶");
        tags.add("置顶1");
        tags.add("置顶2");
        textView.setContentAndTag("发觉朗朗那就拉开怪可怜的口令啦发噶卡罗拉欸绕进去了可能去灵感几块钱并且渴望",
                tags);
    }
}
