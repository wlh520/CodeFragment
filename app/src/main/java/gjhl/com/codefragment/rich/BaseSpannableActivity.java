package gjhl.com.codefragment.rich;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivityBaseSpannableBinding;
import gjhl.com.codefragment.rich.edit.RichTvActivity;
import gjhl.com.codefragment.rich.tag.TagActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

// 基本使用 SpannableActivity (进阶1)
// 花花公屏聊天 SpannableChatActivity (进阶3) 设置margin 值 注意：textview width 要 match_parent 才有效果
// 测试 Spannable 换行 TestActivity (进阶2)
public class BaseSpannableActivity extends AppCompatActivity {

    private ActivityBaseSpannableBinding mBinding;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, BaseSpannableActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_base_spannable);
        mBinding.setActivity(this);
    }

    public void cUseSpannable() {
        SpannableActivity.start(this);
    }

    public void cTSNewline() {
        TSNewlineActivity.start(this);
    }

    public void cSChat() {
        SChatActivity.start(this);
    }

    public void cSChat2() {
        SChat2Activity.start(this);
    }
    
    public void cTab(View view) {
        TagActivity.start(this);
    }

    public void rich(View view) {
        RichTvActivity.Companion.start(this);
    }
}
