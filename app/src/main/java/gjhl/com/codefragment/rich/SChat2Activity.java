package gjhl.com.codefragment.rich;

import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.R;
import gjhl.com.codefragment.databinding.ActivitySpannableChatBinding;
import gjhl.com.codefragment.exit.CenterImageSpan;
import gjhl.com.codefragment.exit.Util;

public class SChat2Activity extends AppCompatActivity {
    private static final String TAG = "SChatActivity";

    private ActivitySpannableChatBinding mBinding;

    public static void start(Activity activity) {
        activity.startActivity(new Intent(activity, SChat2Activity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_spannable_chat);
        getBitmap(Uri.parse("https://res.appbocai.com/gift_842e31c38411d214a82724269fcd6bc3.png"));
    }


    private void spannableTest3(Bitmap bitmap1) {
        // 构造字符串原型
        StringBuilder msg = new StringBuilder();
        for (int i = 0; i < 9; i++) {
            msg.append("$ICON").append(i);
        }
        msg.append("chat01234567890123456789");
        SpannableString ss = new SpannableString(msg.toString());
        for (int i = 0; i < 9; i++) {
            String iconTemp = "$ICON" + i;

            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap1);
            int ddd = Util.dip2px(14);
            bitmapDrawable.setBounds(0, 0, ddd, ddd);
            ss.setSpan(new CenterImageSpan(bitmapDrawable), msg.indexOf(iconTemp), msg.indexOf(iconTemp) + iconTemp.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        mBinding.tChat.setText(ss);

       /* SpannableStringBuilder builder = new SpannableStringBuilder(ss);
//        builder.append("chat01234567890123456789");

        mBinding.tChat.setText(builder);*/
    }

    public void getBitmap(Uri url) {
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(url)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>> dataSource =
                imagePipeline.fetchDecodedImage(imageRequest, MyApplication.getContext());

        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Bitmap bitmap1 = Bitmap.createBitmap(bitmap);
                    spannableTest3(bitmap1);
                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {

            }

        }, UiThreadImmediateExecutorService.getInstance());
    }
}