package gjhl.com.codefragment.rich.edit.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.*
import android.text.style.ImageSpan
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatEditText
import gjhl.com.codefragment.util.ViewUtil
import java.io.File


class ImageEditText(context: Context, attrs: AttributeSet) : AppCompatEditText(context, attrs) {
    companion object {
        val imageWidth = ViewUtil.getWidowWidth()
    }

    private val TAG = "MyEditTextTAG"
    var imageId = 'a'
    val hashMap = HashMap<String, String>()
    var key = "$imageId&"

    init {
        addTextChangedListener(object : TextWatcher {
            var preLength = 0
            var preChar = 'a'
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                preLength = s!!.length
                if (s.isNotEmpty() && start < s.length)
                    preChar = s[start]
            }

            // 在删除换行符时，一并删除换行符前图片
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (start - 1 < 0)
                    return
                Log.i(TAG, "onTextChanged: ${s!!.length} start=${start} ${s[start - 1]}")
                // 不多删除个enter 符号，editText 会显示不完全
                // 删除 "[a..z]&"
                if (preLength > s.length && preChar == '&') {
                    imageId--
                    text?.delete(start - 1, start)
                }

                Log.i(TAG, "onTextChanged: $s")
            }
        })
    }

    /* fun insertDrawable(path: String) {
         key = "$imageId&"
         hashMap[key] = path

         //得到drawable对象，即所有插入的图片
         Glide.with(context).asDrawable().load(path).into(object : CustomTarget<Drawable>() {
             override fun onLoadCleared(placeholder: Drawable?) {

             }

             override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                 insertDrawable(resource)
                 imageId++
             }
         })
     }*/

    fun insertDrawable(path: Int) {
        key = "$imageId&"
        hashMap[key] = "$path"

        insertDrawable(resources.getDrawable(path, null))
        imageId++
    }

    private fun insertDrawable(drawable: Drawable) {
        appendEnterKeyWork()
        val ss = SpannableString("$key") // 不多加个space 符号不能能成功，不知道为何
        var width = drawable.intrinsicWidth
        var height = drawable.intrinsicHeight // 设置图片宽度
        if (drawable.intrinsicWidth > imageWidth) {
            width = imageWidth
            height = imageWidth * drawable.intrinsicHeight / drawable.intrinsicWidth
        }

        drawable.setBounds(0, 0, width, height)
        //用这个drawable对象代替字符串easy
        val span = CenterImageSpan(drawable, ImageSpan.ALIGN_BASELINE)
        //        val span = ImageSpan(d, ImageSpan.ALIGN_BASELINE)
        //包括0但是不包括"easy".length()即：4。[0,4)
        ss.setSpan(span, 0, key.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        val position = Selection.getSelectionStart(this.text)
        text?.insert(position, ss)
        appendEnterKeyWork()
    }

    private fun appendEnterKeyWork() {
        val position2 = Selection.getSelectionStart(this.text)
        text?.insert(position2, "\n")
    }

    var isLeft = true
    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        super.onSelectionChanged(selStart, selEnd)
        text?.let {

            if (selStart - 1 < 0 || selStart + 1 >= it.length)
                return
//            Log.i(TAG, "onSelectionChanged: selStart=${selStart} ${it[selStart]} selEnd=${selEnd} isLeft=${isLeft}")

            if (it[selStart + 1] == '&' && it[selStart] in 'a'..'z' && !isLeft)
                setSelection(selStart + 2)
            else if (it[selStart + 1] == '&' && it[selStart] in 'a'..'z' && isLeft)
                setSelection(selStart - 1)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        isLeft = imageWidth / 2 > event.getRawX()
        return super.onTouchEvent(event)
    }

    /**
     * @param imageUris 服务器返回的图片路径
     */
    fun html(imageUris: List<String>): String {
        var rawString = text.toString()
        var i = 0
        hashMap.forEach {
            rawString = rawString.replace("${it.key}", "<img src=\"${imageUris[i]}\" alt=\"1.0\">")
            i++
        }
        return rawString
    }

    /**
     * 返回 edit text 中的文件
     */
    fun imageFile(): ArrayList<File> {
        val uploadFile = ArrayList<File>()
        hashMap.forEach {
            uploadFile.add(File(it.value))
        }
        return uploadFile
    }
}
