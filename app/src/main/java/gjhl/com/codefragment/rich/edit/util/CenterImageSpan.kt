package gjhl.com.codefragment.rich.edit.util

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.FontMetricsInt
import android.graphics.drawable.Drawable
import android.text.style.DynamicDrawableSpan
import android.text.style.ImageSpan
import androidx.annotation.IntRange

class CenterImageSpan(drawable: Drawable, verticalAlignment: Int) :
    ImageSpan(drawable, verticalAlignment) {

    // 重写宽度为全屏
    override fun getSize(
        paint: Paint, text: CharSequence?,
        start: Int, end: Int,
        fm: Paint.FontMetricsInt?
    ): Int {
        super.getSize(paint, text, start, end, fm)
        return ImageEditText.imageWidth // 设置宽度大于 EditText 会出bug,加一张图片 会出现加两张
    }

    // 重写布局画在中间
    override fun draw(
        canvas: Canvas,
        text: CharSequence?,
        @IntRange(from = 0) start: Int,
        @IntRange(from = 0) end: Int,
        x: Float,
        top: Int,
        y: Int,
        bottom: Int,
        paint: Paint
    ) {
        val b = drawable
        canvas.save()
        var transY = bottom - b.bounds.bottom
        if (mVerticalAlignment == DynamicDrawableSpan.ALIGN_BASELINE) {
            transY -= paint.fontMetricsInt.descent
        } else if (mVerticalAlignment == DynamicDrawableSpan.ALIGN_CENTER) {
            transY = (bottom - top) / 2 - b.bounds.height() / 2
        }
        var transX = ImageEditText.imageWidth - drawable.intrinsicWidth
        if (transX < 0) transX = 0
        canvas.translate(transX / 2.toFloat(), transY + 8.toFloat())
        b.draw(canvas)
        canvas.restore()
    }
}