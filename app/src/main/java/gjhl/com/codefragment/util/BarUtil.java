package gjhl.com.codefragment.util;

import android.app.Activity;
import android.view.View;
import android.view.WindowManager;

/**
 * 作者：Abeing
 * <p>
 *  2019/3/26 17:11
 * ScreenUtil
 */
public class BarUtil {
    public static void hideStateBar(Activity activity) {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static void hideBottomBar(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }
}
