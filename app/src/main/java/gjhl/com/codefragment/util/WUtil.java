package gjhl.com.codefragment.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import gjhl.com.codefragment.MainActivity;
import gjhl.com.codefragment.MyApplication;

public class WUtil {
    private static final String TAG = "WUtil";

    // 串口指令求和
    private String sun(String s, int start, int end) {
        String[] command = s.split(" ");
        int[] commandInt = new int[command.length];
        for (int i = start; i <= end; i++) {
            commandInt[i] = HEX16Util.toByte(command[i].charAt(0)) * 16 + HEX16Util.toByte(command[i].charAt(1));
        }

        int ans = 0;
        for (int i = start; i <= end; i++) {
            ans += commandInt[i];
        }

        String hex16 = HEX16Util.toHexString(ans);
        return hex16.substring(hex16.length() - 2);
    }

    /**
     * 将图片转换成Base64编码
     *
     * @param imgFile 待处理图片
     * @return
     */
    public static String getImgStrBase64(String imgFile) {
        File file = new File(imgFile);
        if (!file.exists())
            return "";
        return Base64.encodeToString(getBytesByFile(imgFile), Base64.DEFAULT);
    }

    // 打开浏览器
    public static void openWeb(Activity activity) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");    //Intent.ACTION_VIEW
        Uri content_url = Uri.parse("http://116.62.146.16/ousouth/#/index");
        intent.setData(content_url);
        activity.startActivity(intent);
    }

    //将文件转换成Byte数组
    public static byte[] getBytesByFile(String pathStr) {
        File file = new File(pathStr);
        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            byte[] data = bos.toByteArray();
            bos.close();
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    // 16进制转 int
    public static int hex(String s) {
        return (int) Long.parseLong("AA", 16);
    }

    // 用16进制的方式打印byte数组
    public static String bytes2hex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        String tmp;
        sb.append("[");
        for (byte b : bytes) {
            // 将每个字节与0xFF进行与运算，然后转化为10进制，然后借助于Integer再转化为16进制
            tmp = Integer.toHexString(0xFF & b);
            if (tmp.length() == 1) {
                tmp = "0" + tmp;//只有一位的前面补个0
            }
            sb.append(tmp).append(" ");//每个字节用空格断开
        }
        sb.delete(sb.length() - 1, sb.length());//删除最后一个字节后面对于的空格
        sb.append("]");
        return sb.toString();
    }

    // 保留一位小数
    public static String retain1(float f) {
        DecimalFormat df = new DecimalFormat("0.0");
        return df.format(f);
    }
    /**
     * ImageView 加载本地图片
     * @param filePath
     * @param imageView
     */
    public static void loadFileImage(String filePath, ImageView imageView) {
       /* 会内存溢出
       Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        //将Bitmap设置到imageview
        imageView.setImageBitmap(bitmap);*/
        Glide.with(MyApplication.getContext()).load(filePath).into(imageView);
    }
    // 图片压缩
    public static Bitmap sizeCompress(String path, int rqsW, int rqsH) {
        // 用option设置返回的bitmap对象的一些属性参数
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;// 设置仅读取Bitmap的宽高而不读取内容
        BitmapFactory.decodeFile(path, options);// 获取到图片的宽高，放在option里边
        final int height = options.outHeight;//图片的高度放在option里的outHeight属性中
        final int width = options.outWidth;
        final BitmapFactory.Options options2 = new BitmapFactory.Options();
        if (rqsW == 0 || rqsH == 0) {
            options2.inSampleSize = 1;
        } else if (height > rqsH || width > rqsW) {
            final int heightRatio = Math.round((float) height / (float) rqsH);
            final int widthRatio = Math.round((float) width / (float) rqsW);
            options2.inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return BitmapFactory.decodeFile(path, options2);// 主要通过option里的inSampleSize对原图片进行按比例压缩
    }

    // 在浏览器中将汉字的字节编码还原成汉字 中文
    public static void netAddressToChinese(String str) {
        try {
            str = URLDecoder.decode(str, "utf-8");
            System.out.println(str);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 将字符串复制到剪切板。
     * setSysClipboardText(mGeneralizeTime.getText().toString());
     */
    public void setSysClipboardText(String copyStr) {
        try {
            //获取剪贴板管理器
            ClipboardManager cm = (ClipboardManager) MyApplication.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
            // 创建普通字符型ClipData
            ClipData mClipData = ClipData.newPlainText("Label", copyStr);
            // 将ClipData内容放到系统剪贴板里。
            cm.setPrimaryClip(mClipData);


            Log.i(TAG, "内容已复制到剪切板");
        } catch (Exception e) {
        }
    }

    // 获取 view 高度
    public static void viewHeight(View view) {
        view.post(() -> {
            view.getHeight();
        });
    }

    /**
     * 写入文件
     *
     * @param sb
     */
    public static void writeLogFile(String sb) {

        String path = Environment.getExternalStorageDirectory() + "error.txt";
        sb = sb + "\n";

        try {
            FileOutputStream fos = new FileOutputStream(path, true);
            fos.write(sb.getBytes());
            fos.flush();
            fos.close();
        } catch (Exception e) {

        }

    }

    // 写入byte
    public static boolean SaveToFile(String sFile, byte[] Buf) {
        Log.i(TAG, "SaveToFile: " + sFile);
        boolean Result = false;
        File file = new File(sFile);

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(file);
            try {
                if (Buf != null && Buf.length > 0)
                    fos.write(Buf);
                fos.close();
                Result = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return Result;
    }

    // 写入文件
    public static String writeStringToFile(String data) {
        String savePath = Environment.getExternalStorageDirectory() + "error.txt";
        try {
            File file = new File(Environment.getExternalStorageDirectory(), "error.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            PrintStream ps = new PrintStream(new FileOutputStream(file));
            ps.println(data);// 往文件里写入字符串
        } catch (Exception e) {
            e.printStackTrace();
        }
        return savePath;
    }

    // recyclerview 点击第一个
    public static void rvClick(RecyclerView rv) {
        rv.postDelayed(()->{
            rv.findViewHolderForAdapterPosition(0).itemView.performClick();
        }, 50);
    }

    // fragment 填充
    public static void fragment(AppCompatActivity activity, Fragment fragment, int resId) {
        activity.getSupportFragmentManager().beginTransaction().replace(resId, fragment).commitAllowingStateLoss();
    }

    /**
     * @param context
     * @return
     * @方法说明:判断是否有网络连接
     * @方法名称:isNetworkConnected
     * @返回值:boolean
     */
    public static boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager
                    .getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    /**
     * 获取版本名字
     *
     * @return versionName
     */
    private String getVersionName(Activity activity) {
        PackageManager pm = activity.getPackageManager();
        String versionName;
        try {
            PackageInfo packageInfo = pm.getPackageInfo(activity.getPackageName(), 0);
            versionName = packageInfo.versionName;
            return versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    // 移除所有 handler                 mHandler.removeCallbacksAndMessages(null)
    // 退出应用 关闭应用             ActivityCompat.finishAffinity(this);

    // 打开应用
    // String type = intent.getStringExtra("type")
    //        if (type=="register") {
    //            ReadDataActivity.start(this)
    //        }
    /*
    private void startOtherApp(boolean isRegister) {
        String applicationId = "com.example.module";
        PackageManager packageManager = getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(applicationId);
        if (isRegister)
            intent.putExtra("type", "register");
        else
            intent.putExtra("type", "bind");
        intent.putExtra("s", new Gson().toJson(MyApplication.userBean));
        startActivity(intent);
    }
    */

    // 获取 mac 地址
    // 获取唯一标识 https://developer.android.com/training/articles/user-data-ids#java
    // 获取后保存在内部存储中
    public static String getMacAddress() {
        return UUID.randomUUID().toString();
    }

    // 反斜杠
    public static String backSplash(String s) {
        return s.replaceAll("\\\\","");
    }

    /*
      放回键
      activity
      //设置监听事件
      public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            }
        return true;
      }


     */
    public static void setBack(AppCompatActivity activity) {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);//左侧添加一个默认的返回图标
        activity.getSupportActionBar().setHomeButtonEnabled(true); //设置返回键可用
    }

    // 文件名
    public static String getFileName(File file) {
        String[] names = file.getName().split("\\.");
        return names[0];
    }

    // 读取TXT文件的文件内容 文本
    public static String getTxtContent(File file) {
        String content = "";
        if (!file.exists())
            return content;
        if (file.isDirectory()) {    //检查此路径名的文件是否是一个目录(文件夹)

        } else {
            if (file.getName().endsWith(".txt")) {//文件格式为txt文件
                try {
                    InputStream inputStream = new FileInputStream(file);
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String line = "";
                    //分行读取
                    while ((line = bufferedReader.readLine()) != null) {
                        content += line;
                    }
                    inputStream.close();        //关闭输入流
                } catch (java.io.FileNotFoundException e) {
                    Log.d("TestFile", "The File doesn't not exist.");
                } catch (IOException e) {
                    Log.d("TestFile", e.getMessage());
                }
            }
        }
        return content;
    }

    // 读取文件 byte 内容
    public static byte[] LoadFromFile(String sFile) {
        byte[] Result = null;
        File file = new File(sFile);

        FileInputStream fis;
        try {
            fis = new FileInputStream(file);
            try {
                Result = new byte[(int) file.length()];
                fis.read(Result);
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Result;
    }


    // 启动app
    /*public void cGoodsRegister(Activity activity) {
        String packageName = "com.example.module";
        PackageManager packageManager = activity.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(packageName);
        activity.startActivity(intent);
    }*/

    public static void isReplace(String s) {
        for (int i = 0; i < s.length()-1; i++) {
            int diff = s.charAt(i) - s.charAt(i+1);
            if (diff==0) {
                System.out.println("不能有重复的数字");
            }
        }
    }

    //重启应用 常驻线程不会销毁（可以把常驻线程设置为单例）
    public static void restartApp() {
        Intent intent = new Intent();
        Context context = MyApplication.getContext();
        intent.setClass(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC,System.currentTimeMillis() + 100,pendingIntent);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    // 获取所有线程
    public static Thread[] findAllThreads() {
        ThreadGroup group = Thread.currentThread().getThreadGroup();
        ThreadGroup topGroup = group;

        /* 遍历线程组树，获取根线程组 */
        while (group != null) {
            topGroup = group;
            group = group.getParent();
        }
        /* 激活的线程数加倍 */
        int estimatedSize = topGroup.activeCount() * 2;
        Thread[] slackList = new Thread[estimatedSize];

        /* 获取根线程组的所有线程 */
        int actualSize = topGroup.enumerate(slackList);
        /* copy into a list that is the exact size */
        Thread[] list = new Thread[actualSize];
        System.arraycopy(slackList, 0, list, 0, actualSize);
        return (list);
    }

    /**
     * [获取应用程序包名]
     */
    public static synchronized String getPackageName() {
        try {
            PackageManager packageManager = MyApplication.getContext().getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    MyApplication.getContext().getPackageName(), 0);
            return packageInfo.packageName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // 升序
    public static void isAscending(String s) {
        for (int i = 0; i < s.length()-1; i++) {
            if (s.charAt(i+1)-s.charAt(i) < 0) {
                System.out.println("不是升序");
                return;
            }
        }
        System.out.println("是升序");
    }

    // 降序
    public static void isDescending(String s) {
        for (int i = 0; i < s.length()-1; i++) {
            if (s.charAt(i+1)-s.charAt(i) > 0) {
                System.out.println("不降降序");
                return;
            }
        }
        System.out.println("是降序");
    }

    // 延迟 延时
    public void initLicenseSuccess() {

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                /**
                 *要执行的操作
                 */
//                startActivity(new Intent(mContext, HomeActivity.class));
//                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 2000);

        /*
        切换到主线程
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                licenseSuccess();
            }
        };
        runOnUiThread(task);*/

        // 定时任务
        timer.schedule(task, 1000, 2000);
    }

    // 轮询
    public void loop() {
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println("hello world");
            }
        }, 100, 1000);
    }

    // json 解析
    public static void parseJson(String response) {
        try {
            JSONObject json = new JSONObject(response);
            int jsonErrorCode = json.optInt("error_code");
            String errorMsg = json.optString("error_msg");
            JSONObject result = json.optJSONObject("result");
            String license = result.optString("license");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取项目根目录
     * public static String s = Environment.getExternalStorageDirectory() + "/Download";
     *
     * @return
     */
    public static String getProjectPath() {
        File file = new File(WUtil.class.getResource("/").getFile());
        String[] paths = file.getAbsolutePath().split("out");
        return paths[0];
    }

    // 查找文件是否存在
    public static boolean isExitInDir(String fileName, String destPath) {
        List<File> files = getSubFiles(destPath);
        for (File file : files) {
            if (file.getName().equals(fileName)) {
                return true;
            }
        }
        return false;
    }

    public static void downloadThread() {
        Thread t = new Thread() {
            @Override
            public void run() {
                super.run();
                downloadNet();
                Log.i(TAG, "run: 下载完成");
            }
        };
        t.start();
    }
    /**
     * 下载网络文件
     *
     * https://www.jianshu.com/p/0562f8385c0f
     * Android 升级
     * 安装失败
     * 在Android手机里不允许有两个应用程序有相同的包名；
     *
     *  假设A应用的包名：com.xxx.xxA;
     *  A应用已经在系统中存在了。
     *
     *  这个时候再去安装一个应用B ，他的包名也叫 con.xxx.xxA
     *  系统就会去检查这两应用的签名是否相同。如果相同，B会把A给覆盖安装掉；
     *  如果不相同 B安装失败；
     *
     *  要想自动安装成功，必须保证应用程序不同版本的签名完成一样。
     *
     *  默认签名
     *  直接在studio里运行项目是, 会采用默认签名debug.keystore. 查找方式: Window->Preference->Android->Build, 可以看到默认签名文件的路径, 默认是: C:\Users\tt\.android\debug.keystore
     *
     * 默认签名的特点:
     * 1. 不同电脑,默认签名文件都不一样
     * 2. 有效期比较短, 默认是1年有效期
     * 3. 有默认密码: android, 别名:androiddebugkey
     *
     * 正式签名
     * 正式签名特点:
     * 1. 发布应用市场时, 统一使用一个签名文件
     * 2. 有效期比较长, 一般25年以上
     * 3. 正式签名文件比较重要,需要开发者妥善保存签名文件和密码
     *
     * @throws IOException
     */
    public static void downloadNet() {
        int byteread;
        try {
            URL url = new URL("http://fb.rzdzkj.com/google.apk");
            String path = Environment.getExternalStorageDirectory() + "/Download";
            new File(path).mkdir();

            URLConnection conn = url.openConnection();
            try (InputStream inStream = conn.getInputStream(); FileOutputStream fs = new FileOutputStream(path + "/abc.apk")) {
                byte[] buffer = new byte[1204];
                while ((byteread = inStream.read(buffer)) != -1) {
                    fs.write(buffer, 0, byteread);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 斜杆
     *
     * @return string.split(" \ \ / ")
     */

    // 获取当前时间 yyyy-MM-dd HH:mm:ss
    public static String getCurTime() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm");
        return sdf.format(date);
    }

    // 目录移动
//    Files.move(Paths.get("start"), Paths.get("end"), StandardCopyOption.REPLACE_EXISTING)

    // 时间戳转字符串
    public static String timeStampToString(long time) {
        Locale locale = Locale.getDefault();//类名.方法名--是静态方法的调用方式
        //获取当前的时区，--也可以使用该类获取当前的语言环境

        String p = "yyyy-cShowInputMM-dd HH:mm:ss";//设置要转换的日期格式
        //也可以是 yyyy-MM-dd HH:mm:ss  yyyy\MM\dd HH:mm:ss 中间的字符可以任意修改
        SimpleDateFormat sformat = new SimpleDateFormat(p, locale);

        return sformat.format(time);
    }

    // 获取当前时间 小时/分钟
    public static String getTime() {
        final Calendar c = Calendar.getInstance();
        String mHour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String mMinute = String.valueOf(c.get(Calendar.MINUTE));
        String mSecond = String.valueOf(c.get(Calendar.SECOND));
        if (Integer.parseInt(mMinute) < 10) {
            mMinute = "0" + mMinute;
        }
        if (Integer.parseInt(mSecond) < 10) {
            mSecond = "0" + mSecond;
        }
        return mHour + "   " + mMinute + "   " + mSecond;
    }

    // 获取当前时间 年
    public static int getTimeYear() {
        final Calendar c = Calendar.getInstance();
        return c.get(Calendar.YEAR);
    }

    // 获取当前时间 月 4月 返回3
    public static int getTimeMonth() {
        final Calendar c = Calendar.getInstance();
        return c.get(Calendar.MONTH);
    }

    // 获取当前时间 日
    public static int getTimeDay() {
        final Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH);
    }

    // 获取当前星期
    public static String getCurTimeWeek() {
        Calendar rightNow = Calendar.getInstance();
        int week = rightNow.get(Calendar.DAY_OF_WEEK);
        switch (week) {
            case 1: return "日";
            case 2: return "一";
            case 3: return "二";
            case 4: return "三";
            case 5: return "四";
            case 6: return "五";
            case 7: return "六";
        }
        return "一";
    }

    // 字符串分割
    // string.split(",")[2];

    // 隐藏软键盘
    public void cCloseSoft(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusView = activity.getCurrentFocus();
        if (focusView != null) {
            imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
        }
    }

    // 判断服务是否开启
    public static boolean isMyServiceRunning(Class<?> serviceClass, Activity activity) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * AlertDialog 弹窗
     * 检测是否开启通知
     *
     * @param context
     */
    public static void checkNotification(final Context context) {
        if (!isNotificationEnabled(context)) {
            new AlertDialog.Builder(context).setTitle("温馨提示")
                    .setMessage("你还未开启系统通知，将影响消息的接收，要去开启吗？")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setNotification(context);
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).show();
        }
    }

    /**
     * 如果没有开启通知，跳转至设置界面
     *
     * @param context
     */
    private static void setNotification(Context context) {
        Intent localIntent = new Intent();
        //直接跳转到应用通知设置的代码：
        localIntent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
        localIntent.putExtra("app_package", context.getPackageName());
        localIntent.putExtra("app_uid", context.getApplicationInfo().uid);
        context.startActivity(localIntent);
    }

    /**
     * 获取通知权限,监测是否开启了系统通知
     *
     * @param context
     */
    private static boolean isNotificationEnabled(Context context) {

        String CHECK_OP_NO_THROW = "checkOpNoThrow";
        String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

        AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        ApplicationInfo appInfo = context.getApplicationInfo();
        String pkg = context.getApplicationContext().getPackageName();
        int uid = appInfo.uid;

        Class appOpsClass = null;
        try {
            appOpsClass = Class.forName(AppOpsManager.class.getName());
            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE,
                    String.class);
            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);

            int value = (Integer) opPostNotificationValue.get(Integer.class);
            return ((Integer) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 获取目录下所有文件
     *
     * @param realpath
     * @param files
     * @return
     */
    public static ArrayList<File> getFiles(String realpath, ArrayList<File> files) {
        File realFile = new File(realpath);
        if (realFile.isDirectory()) {
            File[] subfiles = realFile.listFiles();
            for (File file : subfiles) {
                if (file.isDirectory()) {
                    getFiles(file.getAbsolutePath(), files);
                } else {
                    files.add(file);
                }
            }
        }
        return files;
    }

    /**
     * 获取子目录
     *
     * @param realpath
     * @return
     */
    public static ArrayList<File> getSubDirectory(String realpath) {
        File realFile = new File(realpath);
        ArrayList<File> fileList = new ArrayList<>();
        if (realFile.isDirectory()) {
            File[] subfiles = realFile.listFiles();
            for (File file : subfiles) {
                if (file.isDirectory()) {
                    fileList.add(file);
                }
            }
        }
        return fileList;
    }

    /**
     * 获取子文件
     *
     * @param realpath
     * @return
     */
    public static ArrayList<File> getSubFiles(String realpath) {
        File realFile = new File(realpath);
        ArrayList<File> fileList = new ArrayList<>();
        if (realFile.isDirectory()) {
            File[] subfiles = realFile.listFiles();
            for (File file : subfiles) {
                if (!file.isDirectory()) {
                    fileList.add(file);
                }
            }
        }
        return fileList;
    }

    /**
     * 网络是否连接
     */
    //判断网络状态
    public static boolean isConnectInternet(Activity activity) {

        ConnectivityManager conManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = conManager.getActiveNetworkInfo();

        if (networkInfo != null) { // 注意，这个判断一定要的哦，要不然会出错

            return networkInfo.isAvailable();

        }
        return false;
    }

    /**
     * ping
     *
     * @param url
     * @return
     */
    public static boolean ping(String url) {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("ping -c 3 " + url);
            int res = ipProcess.waitFor();
            return res == 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 获取IP
         <uses-permission android:name="android.permission.INTERNET" />
              <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" /> <!-- 用于访问wifi网络信息，wifi信息会用于进行网络定位 -->
     <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" /> <!-- 这个权限用于获取wifi的获取权限，wifi信息会用来进行网络定位 -->

     */
    public static String getIp(Application application) {
        ConnectivityManager connectivityManager = (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network nw = connectivityManager.getActiveNetwork();
        if (nw == null) return "";
        NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

        if (actNw!=null) {
            if (actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                try {
                    Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
                    while (networkInterfaces.hasMoreElements()) {
                        Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                        while (true) {
                            if (inetAddresses.hasMoreElements()) {
                                InetAddress nextElement = inetAddresses.nextElement();
                                if (!nextElement.isLoopbackAddress() && (nextElement instanceof Inet4Address)) {
                                    return nextElement.getHostAddress();
                                }
                            }
                        }
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                }
            } else if (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return ip(((WifiManager) application.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getIpAddress());
            } else if (actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                return ip2();
            }
        }
        return "";
    }

    private static String ip(int i) {
        return String.valueOf(i & 255) + "." + ((i >> 8) & 255) + "." + ((i >> 16) & 255) + "." + ((i >> 24) & 255);
    }

    private static String ip2() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface nextElement = networkInterfaces.nextElement();
                String displayName = nextElement.getDisplayName();
                Log.i("tag", "网络名字" + displayName);
                if (displayName.equals("eth0")) {
                    Enumeration<InetAddress> inetAddresses = nextElement.getInetAddresses();
                    while (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement2 = inetAddresses.nextElement();
                        if (!nextElement2.isLoopbackAddress() && (nextElement2 instanceof Inet4Address)) {
                            Log.i("tag", String.valueOf(nextElement2.getHostAddress()) + "   ");
                            return nextElement2.getHostAddress();
                        }
                    }
                    continue;
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return "";
    }
}
