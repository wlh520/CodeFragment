package gjhl.com.codefragment.util

import java.io.File
import java.text.Collator
import java.util.*

object KUtil {
    // 启动app
    /* fun cOpenApp() {
         val packname = "com.yitong.mbank"
         val packageManager = packageManager
         if (checkPackInfo(packname)) {
             val intent = packageManager.getLaunchIntentForPackage(packname)

  // 这里跟Activity传递参数一样的嘛，不要担心怎么传递参数，还有接收参数也是跟Activity和Activity传参数一样
             intent.putExtra("value","我是App1里的值");
 // 接收
             intent.getStringExtra("")



             startActivity(intent)
         } else {
             Toast.makeText(this, "没有安装$packname", Toast.LENGTH_LONG).show()
         }
     }



    /**
     * 检查包是否存在
     *
     * @param packname
     * @return
     */
    private fun checkPackInfo(packname: String): Boolean {
        var packageInfo: PackageInfo? = null
        try {
            packageInfo = packageManager.getPackageInfo(packname, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return packageInfo != null
    }*/

    fun isPicture(f1: File): Boolean {
        val names = f1.name.split("\\.".toRegex()).toTypedArray()
        val name = names[names.size - 1]
        return name.equals("jpg", ignoreCase = true) || name.equals(
            "jpeg",
            ignoreCase = true
        ) || name.equals("png", ignoreCase = true) || name.equals("webp", ignoreCase = true)
    }

    // 是否是视频
    fun isVideo(f1: File): Boolean {
        val names = f1.name.split("\\.".toRegex()).toTypedArray()
        val name = names[names.size - 1]
        return name.equals("mp4", ignoreCase = true) || name.equals("mov", ignoreCase = true) || name.equals("ogv", ignoreCase = true)
    }
    /**
     * 获取目录下所有文件(按时间排序)
     *
     * @param path
     * @return
     */
    fun listFileSortByName(path: String): List<File>{
        val list =
            getFiles(path, ArrayList())
        if (list != null && list.size > 0) {
            Collections.sort(list,
                object : Comparator<File?> {

                    override fun compare(o1: File?, o2: File?): Int {
                        if (o1!!.isDirectory && o2!!.isFile) return -1
                        return if (o1.isFile && o2!!.isDirectory) 1 else o1.name.compareTo(o2!!.name)
                    }
                })
        }
        Collections.sort(list,
            object : Comparator<File?> {
                override fun compare(o1: File?, o2: File?): Int {
                    val s1 = o1!!.name
                    val s2 = o2!!.name
                    return Collator.getInstance(Locale.CHINESE).compare(s1, s2)
                }
            })
        return list
    }

    /**
     * 获取目录下所有文件
     *
     * @param realpath
     * @param files
     * @return
     */
    fun getFiles(realpath: String, files: MutableList<File>): List<File> {
        val realFile = File(realpath)
        if (realFile.isDirectory) {
            val subfiles = realFile.listFiles()
            for (file in subfiles) {
                if (file.isDirectory) {
                    //getFiles(file.getAbsolutePath(), files);
                } else {
                    files.add(file)
                }
            }
        }
        return files
    }
}