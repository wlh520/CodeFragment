package gjhl.com.codefragment.util;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * 关闭显示导航条
 */
public class CloseBarUtil {

    /**
     * 关闭底部导航条
     */
    public static void closeBar() {
        execRootCmdSilent("wm overscan 0,0,0,-126");
    }

    /**
     * 运行命令
     * 执行命令但不关注结果输出
     */
    public static int execRootCmdSilent(String cmd) {
        int result = -1;
        DataOutputStream dos = null;

        try {
            Process p = Runtime.getRuntime().exec("su");
            dos = new DataOutputStream(p.getOutputStream());

            dos.writeBytes(cmd + "\n");
            dos.flush();
            dos.writeBytes("exit\n");
            dos.flush();
            p.waitFor();
            result = p.exitValue();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }


    /**
     * 显示底部导航条
     */
    public static void showBar() {
        execRootCmdSilent("wm overscan 0,0,0,-0");
    }
}
