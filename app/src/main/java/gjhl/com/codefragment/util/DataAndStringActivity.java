package gjhl.com.codefragment.util;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import gjhl.com.codefragment.R;

public class DataAndStringActivity extends AppCompatActivity {
    private static final String TAG = "DataAndStringActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, DataAndStringActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_and_string);


        int days = getDays(3);
        Log.i(TAG, "onCreate: days" + days);
    }

    private int getDays(int month) {
        Calendar rightNow = Calendar.getInstance();
        rightNow.add(Calendar.MONTH, month);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date=rightNow.getTime();
        Log.i(TAG, "onCreate: " + sdf.format(date));

        Date dateNow = Calendar.getInstance().getTime();
        return (int) ((dateNow.getTime() - date.getTime()) / (1000*3600*24));
    }

    private void string() {
        String s = "1000~13000元";
        String[] moneys = s.split("~");
        if (moneys[1].endsWith("元"))
            moneys[1] = moneys[1].substring(0, moneys[1].length()-1);
        Log.i(TAG, "onCreate: " + moneys[0] + " : " + moneys[1]);
    }
}
