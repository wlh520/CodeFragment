package gjhl.com.codefragment.util

import android.util.DisplayMetrics
import gjhl.com.codefragment.MainActivity

object ViewUtil {
    // 屏幕宽度（像素）
    fun getWidowWidth(): Int {
        val outMetrics = DisplayMetrics()
        MainActivity.activity.windowManager.defaultDisplay.getRealMetrics(outMetrics)
        return outMetrics.widthPixels
    }

    // 屏幕高度（像素）
    fun getWidowHeight(): Int {
        val outMetrics = DisplayMetrics()
        MainActivity.activity.windowManager.defaultDisplay.getRealMetrics(outMetrics)
        return outMetrics.heightPixels
    }
}