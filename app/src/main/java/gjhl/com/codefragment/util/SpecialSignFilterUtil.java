package gjhl.com.codefragment.util;

import android.content.Context;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Administrator on 2018/12/17 0017.
 */

public class SpecialSignFilterUtil {
    public static void setProhibitEmoji(EditText et, Context context) {
        InputFilter[] filters = { getInputFilterProhibitEmoji(context) ,getInputFilterProhibitSP(context)};
        et.setFilters(filters);
    }

    private static InputFilter getInputFilterProhibitEmoji(final Context context) {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                StringBuffer buffer = new StringBuffer();
                for (int i = start; i < end; i++) {
                    char codePoint = source.charAt(i);
                    if (!getIsEmoji(codePoint)) {
                        buffer.append(codePoint);
                    } else {
                        Toast.makeText(context, "不能含有第三方表情", Toast.LENGTH_SHORT).show();
                        i++;
                        continue;
                    }
                }
                if (source instanceof Spanned) {
                    SpannableString sp = new SpannableString(buffer);
                    TextUtils.copySpansFrom((Spanned) source, start, end, null,
                            sp, 0);
                    return sp;
                } else {
                    return buffer;
                }
            }
        };
        return filter;
    }


    private  static boolean getIsEmoji(char codePoint) {
        if ((codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA)
                || (codePoint == 0xD)
                || ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
                || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
                || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF)))
            return false;
        return true;
    }


    private static InputFilter getInputFilterProhibitSP(final Context context) {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                StringBuffer buffer = new StringBuffer();
                for (int i = start; i < end; i++) {
                    char codePoint = source.charAt(i);
                    if (!getIsSP(codePoint)) {
                        buffer.append(codePoint);
                    } else {
                        Toast.makeText(context, "不能含有特殊字符", Toast.LENGTH_SHORT).show();
                        i++;
                        continue;
                    }
                }
                if (source instanceof Spanned) {
                    SpannableString sp = new SpannableString(buffer);
                    TextUtils.copySpansFrom((Spanned) source, start, end, null, sp, 0);
                    return sp;
                } else {
                    return buffer;
                }
            }
        };
        return filter;
    }

    private static boolean getIsSP(char codePoint){
        if(Character.getType(codePoint)>Character.LETTER_NUMBER){
            return true;
        }

        return false;

    }
}
