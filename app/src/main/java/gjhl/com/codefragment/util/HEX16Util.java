package gjhl.com.codefragment.util;

public class HEX16Util {
    /**
     * int转16进制String
     *
     * @param i
     * @return 8 00000008
     */
    public static String toHexString(int i) {
        return toHexString(toByteArray(i));
    }

    /**
     * 字节数组转16进制String，无分隔，如：FE00120F0E
     *
     * @param array 字节数组
     * @return
     */
    public static String toHexString(byte[] array) {
        return toHexString(array, 0, array.length);
    }

    /**
     * 字节数组转16进制String，无分隔，如：FE00120F0E
     *
     * @param array  字节数组
     * @param offset 起始
     * @param length 长度
     * @return
     */
    public static String toHexString(byte[] array, int offset, int length) {
        char[] buf = new char[length * 2];

        int bufIndex = 0;
        for (int i = offset; i < offset + length; i++) {
            byte b = array[i];
            buf[bufIndex++] = HEX_DIGITS[(b >>> 4) & 0x0F];
            buf[bufIndex++] = HEX_DIGITS[b & 0x0F];
        }

        return new String(buf);
    }
    private final static char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};//十六进制的组成元素
    /**
     * int转字节数组
     *
     * @param i
     * @return
     */
    public static byte[] toByteArray(int i) {
        byte[] array = new byte[4];

        array[3] = (byte) (i & 0xFF);
        array[2] = (byte) ((i >> 8) & 0xFF);
        array[1] = (byte) ((i >> 16) & 0xFF);
        array[0] = (byte) ((i >> 24) & 0xFF);

        return array;
    }

    /**
     * 十六进制转int
     *
     * @param c
     * @return
     */
    public static int toByte(char c) {
        if (c >= '0' && c <= '9') return (c - '0');
        if (c >= 'A' && c <= 'F') return (c - 'A' + 10);
        if (c >= 'a' && c <= 'f') return (c - 'a' + 10);

        throw new RuntimeException("Invalid hex char '" + c + "'");
    }
}
