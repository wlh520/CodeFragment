package gjhl.com.codefragment.util;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import gjhl.com.codefragment.MainActivity;
import gjhl.com.codefragment.MyApplication;
import gjhl.com.codefragment.R;

public class UtilActivity extends AppCompatActivity {
    private static final String TAG = "UtilActivity";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, UtilActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.util_activity);

//        String s1 = WUtil.retain1(0.1f);
//        String s2 = WUtil.retain1(1.1f);
        int month = WUtil.getTimeMonth();
        Log.i(TAG, "onCreate: " + month);
        Button btSend = findViewById(R.id.bSMSS);
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SMSActivity.start(UtilActivity.this);
            }
        });
    }

    //重启应用 常驻线程不会销毁（可以把常驻线程设置为单例）
    public void restartApp(View view) {
        WUtil.restartApp();
    }

    public void macAddress(View view) {
        String macAddress = WUtil.getMacAddress();
        Log.i(TAG, "macAddress: " + macAddress);
    }

    public void netConnect(View view) {
        Button button = (Button) view;
        button.setText("网络" + WUtil.isConnectInternet(this));
    }
}
