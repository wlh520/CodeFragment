package gjhl.com.codefragment.radioButton;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioGroup;

import gjhl.com.codefragment.R;

public class RadioButtonActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private RadioGroup radiogroup;
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, RadioButtonActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_button);
        findView();
        clickTextView();
    }

    private void clickTextView() {
        findViewById(R.id.textView).setOnClickListener(v->{
            switch (radiogroup.getCheckedRadioButtonId()){
                case R.id.radioButton:
                    Log.i(TAG, "onCreate: radioButton");
                    break;
                case R.id.radioButton2:
                    Log.i(TAG, "onCreate: radioButton2");
                    break;
            }
        });
    }

    private void findView() {
        radiogroup = findViewById(R.id.radiogroup);
    }
}
