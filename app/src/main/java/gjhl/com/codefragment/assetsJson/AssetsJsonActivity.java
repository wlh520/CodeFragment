package gjhl.com.codefragment.assetsJson;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import gjhl.com.codefragment.R;

public class AssetsJsonActivity extends AppCompatActivity {
    private static final String TAG = "AssetsJsonActivity";
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, AssetsJsonActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_province);
        getProvinceList();
    }

    private void getProvinceList() {
        List<Province> provinceList = new ArrayList<>(34);
        try {
            JSONArray jsonArray = new JSONArray(getFromAssets("province.sjon"));
            for (int i = 0; i< jsonArray.length(); i++) {
                //循环遍历，依次取出JSONObject对象
                //用getInt和getString方法取出对应键值
                JSONObject jsonProvince = jsonArray.getJSONObject(i);
                List<Province.CityBean> cityList = getCityBeans(jsonProvince);
                Province province = new Province();
                province.setName(jsonProvince.getString("name"));
                province.setCity(cityList);
                provinceList.add(province);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        provinceList.add(0, new Province());
        provinceList.add(provinceList.size(), new Province());
        Log.i(TAG, "getProvinceList: " + provinceList.size());
    }

    @NonNull
    private List<Province.CityBean> getCityBeans(JSONObject jsonProvince) throws JSONException {
        List<Province.CityBean> cityList = new ArrayList<>();
        JSONArray cityArray = jsonProvince.getJSONArray("city");
        for (int j = 0; j < cityArray.length(); j++) {
            JSONObject cityObject = cityArray.getJSONObject(j);
            List<String> areaList = getAreaList(cityObject);
            Province.CityBean city = new Province.CityBean();
            city.setName(cityObject.getString("name"));
            city.setArea(areaList);
            cityList.add(city);
        }
        cityList.add(0, new Province.CityBean());
        cityList.add(cityList.size(), new Province.CityBean());
        return cityList;
    }

    @NonNull
    private List<String> getAreaList(JSONObject cityObject) throws JSONException {
        List<String> areaList = new ArrayList<>();
        JSONArray areaArray = cityObject.getJSONArray("area");
        for (int k = 0; k < areaArray.length(); k++) {
            areaList.add(areaArray.get(k).toString());
        }
        areaList.add(0, "");
        areaList.add(areaList.size(), "");
        return areaList;
    }

    public String getFromAssets(String fileName){
        String Result="";
        try {
            InputStreamReader inputReader = new InputStreamReader( getResources().getAssets().open(fileName) );
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line="";

            while((line = bufReader.readLine()) != null)
                Result += line;
            return Result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result;
    }
}
