package gjhl.com.codefragment.editText;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import gjhl.com.codefragment.R;

/**
 * 作者：wlh
 * <p>
 * 时间： 2019/3/9 09:51
 */
public class CountEditText {
    private String beforeText;

    public static void assemble(Activity activity, int maxInputNum) {
        CountEditText countEditText = new CountEditText();
        countEditText.editText(activity, maxInputNum);
    }
    private EditText editText(Activity activity, int maxInputNum) {
        EditText editText = activity.findViewById(R.id.editText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                beforeText = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                TextView tvNum = activity.findViewById(R.id.tvNum);
                if (s.length() > maxInputNum) {
                    Toast.makeText(activity, "最多输入" + maxInputNum + "字", Toast.LENGTH_SHORT).show();
                    tvNum.setText(beforeText.length() + "/" + maxInputNum);
                    editText.setText(beforeText);
                }
                else
                    tvNum.setText(s.length() + "/" + maxInputNum);
            }
        });
        return editText;
    }
}
