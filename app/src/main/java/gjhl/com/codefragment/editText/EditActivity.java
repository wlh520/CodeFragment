package gjhl.com.codefragment.editText;

import android.app.Activity;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import gjhl.com.codefragment.R;

// 监听 EditText
// edittext 不被遮挡
// 在AndroidManifest.xml相应的activity里面添加
// android:windowSoftInputMode="adjustPan|stateHidden"

/*
输入ip和端口 设置成只能输入 数字和点 android:digits="0123456789."
digits 数字
android:digits="0123456789.:" 输入ip和端口
 */
public class EditActivity extends AppCompatActivity {
    private static final String TAG = "EditActivity";

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, EditActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        CountEditText.assemble(this, 200);
        EditText editText = findViewById(R.id.editText);
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

        EditText et1 = findViewById(R.id.et1);
        // 换行变成搜索
        et1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });

        EditText etFocus = findViewById(R.id.etFocus);
        //判断Edittext是否有焦点
        etFocus.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                Log.i(TAG, "onFocusChange: " + b);
            }
        });
    }
}
