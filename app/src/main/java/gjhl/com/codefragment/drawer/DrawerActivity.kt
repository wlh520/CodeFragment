package gjhl.com.codefragment.drawer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import gjhl.com.codefragment.R

// 抽屉 侧滑布局
class DrawerActivity: AppCompatActivity() {
    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, DrawerActivity::class.java)
            activity.startActivity(intent)
        }
    }
    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.drawer_activity)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)

        findViewById<View>(R.id.bOpenDraw).setOnClickListener {
            drawerLayout.openDrawer(Gravity.LEFT)
        }

        val nav_view = findViewById<NavigationView>(R.id.nav_view)
        val textView = nav_view.getHeaderView(0).findViewById<TextView>(R.id.textView)
        Log.i(TAG, "onCreate: ${textView.text}")
    }
}